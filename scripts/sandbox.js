var LIVE_URL = "//api.azubu.tv/public/modules/channel/live/list?orderBy=channel.viewCount";

var streamsContainer = document.getElementById('streamsContainer');

var handleGetLiveStreamsResponse = function (event) {
  if (this.readyState === 4 && this.status === 200) {
    var response = JSON.parse(event.target.response);

    var fpsVOD = {
      label: 'AZUBU 60fps MP4',
      url: '//azubu.dgnow.net/assets/video/Azubu_Icon_60fps.mp4',
      type: 'video/mp4'
    };

    var bf4DASH = {
      label: 'BF4 60fps DASH',
      url: '//bitdash-a.akamaihd.net/content/battlefield_60fps/battlefield.mpd',
      type: 'application/dash+xml'
    };

    var fourkDASH = {
      label: '4k DASH',
      url: '//bitdash-a.akamaihd.net/content/interstellar/interstellar.mpd',
      type: 'application/dash+xml'
    };

    var fire60fpsVOD = {
      label: 'AZUBU Fire 60fps MP4',
      url: '//amp.azubu.tv/video/azubu_fire_1080_60.mp4',
      type: 'video/mp4'
    };

    createVodButton(fpsVOD);
    createVodButton(bf4DASH);
    createVodButton(fourkDASH);
    createVodButton(fire60fpsVOD);

    console.log(response.data.length, 'LIVE Streams');

    for (var index = 0; index < response.data.length; index++) {
      createAzubuStreamItem(response.data[index]);
    }
  }
};

var loadVodSource = function (data) {
  document.getElementById('playerFrame').src = 'iframe.html?videoUrl=' + data.value;
  document.getElementById('playerFrame').focus();
  window.document.body.scrollTop = 0;
};

var createVodButton = function (data) {
  var itemEl = document.createElement('div');
  itemEl.innerHTML = '<button class="btn btn-warning pull-left" style="width:25%; height: 40px; overflow: hidden;" onclick="loadVodSource(this)" value="' + data.url + '">' + data.label + '</button>'; // "<button onclick='loadSourceByData(this)' value='"+data.stream_video.reference_id+"'>"+ data.user.username +"</button>";

  streamsContainer.appendChild(itemEl);
};

var createAzubuStreamItem = function (data) {
  var itemEl = document.createElement('div');
  var highlight = ['LoLEsports', 'esl_', 'ESL_'];
  var className = 'btn-info';

  highlight.forEach(function (value, index) {
    if (data.user.username.indexOf(value) !== -1) {
      className = 'btn-primary';
      return;
    }
  });

  if (data.stream_video) {
    itemEl.innerHTML = '<button class="btn ' + className + ' pull-left" style="border: 1px solid #4b4a4b; box-sizing: border-box; border-radius: 0; margin: .5%;  width: 24%; height: 160px; text-shadow: 1px 1px 5px rgba(0,0,0,0.8); overflow: hidden; background: url(' + data.url_thumbnail + ') no-repeat; background-size: cover; background-position: center center;" onclick="loadSourceByData(this)" value="' + data.stream_video.reference_id + '">' + data.user.username + '</button>' // "<button onclick='loadSourceByData(this)' value='"+data.stream_video.reference_id+"'>"+ data.user.username +"</button>";
    streamsContainer.appendChild(itemEl);
  } else {
    // console.warn('Cannot Create Stream Item', data);
  }

};

var loadSourceByData = function (data) {
  document.getElementById('playerFrame').src = 'iframe.html?videoId=' + data.value;
  document.getElementById('playerFrame').focus();
  window.document.body.scrollTop = 0;
};

window.onerror = function (message, source, lineno, colno, error) {
  console.warn('ERR', arguments);
};

var xhr = new XMLHttpRequest();
xhr.open('GET', LIVE_URL, true);
xhr.onreadystatechange = handleGetLiveStreamsResponse;
xhr.send();

if (window.location.search.indexOf('videoId') !== -1) {
  loadSourceByData({value: window.location.search.split('=')[1]});
}
