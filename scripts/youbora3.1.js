/**
 * @license
 * Youboralib 2.0-RC <http://youbora.com/>
 * Copyright NicePopleAtWork <http://nicepeopleatwork.com/>
 */
try {
  var $YB = $YB || {
      version: "2.0-RC", errorLevel: 1, report: function (e, t, i) {
        if (console && console.log && ("undefined" == typeof i && (i = 4), $YB.errorLevel >= i))if (document.documentMode)console.log("[Youbora:" + i + "] " + e); else {
          var r = console.log;
          1 == i && console.error ? r = console.error : 2 == i && console.warn ? r = console.warn : 3 == i && console.info ? r = console.info : 5 == i && console.debug && (r = console.debug), "object" == typeof e ? r.call(console, "%c[Youbora] %o", "color: " + t, e) : r.call(console, "%c[Youbora]%c %s", "color: " + t, "color: black", e)
        }
      }, error: function (e) {
        $YB.report(e, "darkred", 1)
      }, warning: function (e) {
        $YB.report(e, "darkorange", 2)
      }, info: function (e) {
        $YB.report(e, "navy", 3)
      }, notice: function (e) {
        $YB.report(e, "darkcyan", 4)
      }, noticeRequest: function (e) {
        $YB.report(e, "darkgreen", 4)
      }, debug: function (e) {
      }, log: function (e) {
        this.errorLevel >= 4 && console.log(e)
      }, plugins: {}, util: {
        listenAllEvents: function () {
        }, serialize: function () {
        }
      }
    };
  !function () {
    var e = /\?.*\&*(youbora-debug=(.+))/i.exec(window.location.search);
    if (null !== e)$YB.errorLevel = e[2]; else {
      var t = document.getElementsByTagName("script");
      for (var i in t)if (t[i].getAttribute) {
        var r = t[i].getAttribute("youbora-debug");
        if (r) {
          $YB.errorLevel = r;
          break
        }
      }
    }
  }(), $YB.notice("YouboraLib " + $YB.version + " is ready.")
} catch (err) {
  var m = "[Youbora] Fatal Error: Unable to start Youboralib.";
  console.error ? console.error(m) : console.log(m), console.log(err)
}
$YB.AjaxRequest = function (e, t, i, r) {
  try {
    this.xmlHttp = this.createXMLHttpRequest(), this.host = e, this.service = t || "", this.params = i || "", this.options = r || {}, this.options.method = this.options.method || "GET", this.options.maxRetries = this.options.maxRetries || 5, this.options.retryAfter || (this.options.retryAfter = 5e3), this.hasError = !1, this.retries = 0
  } catch (n) {
    $YB.error(n)
  }
}, $YB.AjaxRequest.prototype.getUrl = function () {
  try {
    return this.params ? this.host + this.service + "?" + this.params : this.host + this.service
  } catch (e) {
    $YB.error(e)
  }
}, $YB.AjaxRequest.prototype.on = function (e, t) {
  try {
    "error" == e && (this.hasError = !0);
    var i = this;
    "function" == typeof t ? this.xmlHttp.addEventListener(e, function () {
      try {
        t(this)
      } catch (e) {
        $YB.error(e)
      }
    }, !1) : "undefined" != typeof t && $YB.warning("Warning: Request '" + i.getUrl() + "' has a callback that is not a function.")
  } catch (r) {
    $YB.error(r)
  } finally {
    return this
  }
}, $YB.AjaxRequest.prototype.load = function (e) {
  return this.on("load", e)
}, $YB.AjaxRequest.prototype.error = function (e) {
  return this.on("error", e)
}, $YB.AjaxRequest.prototype.append = function (e) {
  return this.params.length > 0 && (e = "&" + e), this.params += e, this
}, $YB.AjaxRequest.prototype.send = function () {
  try {
    if (this.xmlHttp.open(this.options.method, this.getUrl(), !0), this.options.requestHeader)for (var e in this.options.requestHeader)this.options.hasOwnProperty(e) && this.xmlHttp.setRequestHeader(e, this.options.requestHeader[e]);
    if (!this.hasError && this.options.retryAfter > 0) {
      var t = this;
      this.error(function r() {
        t.retries++, t.retries > t.options.maxRetries ? $YB.error("Error: Aborting failed request. Max retries reached.") : ($YB.error("Error: Request failed. Retry " + t.retries + " of " + t.options.maxRetries + " in " + t.options.retryAfter + "ms."), setTimeout(function () {
          t.xmlHttp.removeEventListener("error", r), t.send()
        }, t.options.retryAfter))
      })
    }
    this.xmlHttp.send()
  } catch (i) {
    $YB.error(i)
  }
}, $YB.AjaxRequest.prototype.createXMLHttpRequest = function () {
  try {
    return window.XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject("Microsoft.XMLHTTP")
  } catch (e) {
    return $YB.error(e), {}
  }
}, $YB.Buffer = function (e, t) {
  try {
    this.context = e, this.options = t || {}, this.options.interval = this.options.interval || 800, this.options.threshold = this.options.threshold || 400, this.options.skipMiniBuffer = this.options.skipMiniBuffer || !0, this.timer = null, this.lastPlayhead = 0, this.autostart = !1
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Buffer.prototype.start = function () {
  try {
    if (null === this.timer)if ("function" == typeof this.context.plugin.getPlayhead) {
      var e = this;
      this.lastPlayhead = 0, this.context.chrono.buffer.start(), this.timer = setInterval(function () {
        try {
          e._checkBuffer()
        } catch (t) {
          $YB.error(t)
        }
      }, this.options.interval)
    } else $YB.warning("Warning: Can't start autobuffer because " + this.context.plugin.pluginVersion + " does not implement getPlayhead().")
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Buffer.prototype._checkBuffer = function () {
  try {
    if (this.context.isJoinSent && !this.context.isPaused && !this.context.isSeeking && !this.context.isShowingAds) {
      var e = this.context.plugin.getPlayhead();
      Math.abs(1e3 * this.lastPlayhead - 1e3 * e) > this.options.threshold ? (this.lastPlayhead = e, (!this.options.skipMiniBuffer || this.context.chrono.buffer.stop() > 1.1 * this.options.interval) && this.context.handleBufferEnd()) : this.lastPlayhead && !this.context.isBuffering && Math.abs(1e3 * this.lastPlayhead - 1e3 * e) < this.options.threshold && this.context.handleBufferStart()
    }
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Buffer.prototype.stop = function () {
  try {
    return clearInterval(this.timer), this.timer = null, this.context.chrono.buffer.stop()
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Chrono = function () {
  try {
    this.startTime = 0, this.lastTime = 0
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Chrono.prototype.getDeltaTime = function () {
  try {
    return this.startTime && this.lastTime ? this.lastTime - this.startTime : -1
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Chrono.prototype.start = function () {
  try {
    this.startTime = (new Date).getTime()
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Chrono.prototype.stop = function () {
  try {
    return this.lastTime = (new Date).getTime(), this.getDeltaTime()
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Communication = function (e, t) {
  try {
    this.host = e || "nqs.nice264.com", this.httpSecure = t, this.pingTime = 5, this.lastDurationSent = 0, this._requests = {}, this._preloaders = [], this.code = "", this.view = -1, this.addPreloader("FastData")
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.getViewCode = function () {
  return this.code ? this.code + "_" + this.view : "nocode"
}, $YB.Communication.prototype.sendData = function (e, t) {
  try {
    e = e || {}, delete e.code;
    var i = this, r = new $YB.AjaxRequest(this._parseServiceHost(this.host), "/data", this._parseParams(e));
    r.load(function () {
      i.receiveData(r)
    }), r.load(t), r.send()
  } catch (n) {
    $YB.error(n)
  }
}, $YB.Communication.prototype.receiveData = function (e) {
  try {
    var t = e.xmlHttp.responseXML, i = {
      h: t.getElementsByTagName("h"),
      c: t.getElementsByTagName("c"),
      pt: t.getElementsByTagName("pt"),
      b: t.getElementsByTagName("b")
    };
    i.h.length > 0 && i.c.length > 0 && i.pt.length > 0 && i.b.length > 0 ? (this.code = i.c[0].textContent, this.host = i.h[0].textContent, this.pingTime = i.pt[0].textContent, this.balancerEnabled = i.b[0].textContent, $YB.noticeRequest("FastData '" + this.code + "'is ready."), this._requests.nocode && this._requests.nocode.length > 0 && (this._requests[this.getViewCode()] = [], this._requests[this.getViewCode()] = this._requests.nocode, delete this._requests.nocode), this.removePreloader("FastData")) : $YB.warning("Warning: FastData response is wrong.")
  } catch (r) {
    $YB.error(r)
  }
}, $YB.Communication.prototype.sendStart = function (e, t) {
  try {
    this.view++, e = e || {}, delete e.code, e.user = e.user || "default", e.totalBytes = e.totalBytes || 0, e.pingTime = e.pingTime || this.pingTime, e.referer = e.referer || window.location.href, e.properties = e.properties || {}, e.pingTime = e.pingTime || this.pingTime, e.live = e.live || "false", this.checkMandatoryParams(e, ["system", "pluginVersion", "user", "resource"]), this.lastDurationSent = e.duration, this.sendRequest("/start", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendStop = function (e, t) {
  try {
    e = e || {}, delete e.code, this.sendRequest("/stop", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendJoin = function (e, t) {
  try {
    e = e || {}, delete e.code, this.checkMandatoryParams(e, ["time"]), e.mediaDuration == this.lastDurationSent && delete e.mediaDuration, this.sendRequest("/joinTime", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendBuffer = function (e, t) {
  try {
    e = e || {}, delete e.code, this.checkMandatoryParams(e, ["time", "duration"]), this.sendRequest("/bufferUnderrun", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendSeek = function (e, t) {
  try {
    e = e || {}, delete e.code, this.checkMandatoryParams(e, ["time", "duration"]), this.sendRequest("/seek", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendAds = function (e, t) {
  try {
    e = e || {}, delete e.code, this.checkMandatoryParams(e, ["time", "duration"]), this.sendRequest("/ads", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendPause = function (e, t) {
  try {
    e = e || {}, delete e.code, this.sendRequest("/pause", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendResume = function (e, t) {
  try {
    e = e || {}, delete e.code, this.sendRequest("/resume", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendPing = function (e, t) {
  try {
    e = e || {}, delete e.code, e.totalBytes = e.totalBytes || 0, e.pingTime = e.pingTime || this.pingTime, this.checkMandatoryParams(e, ["time"]), this.sendRequest("/ping", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendError = function (e, t) {
  try {
    e = e || {}, delete e.code, e.msg = e.msg || "Unknown Error", ("undefined" == typeof e.errorCode || parseInt(e.errorCode) < 0) && (e.errorCode = 9e3), this.checkMandatoryParams(e, ["msg", "errorCode", "player"]), this.sendRequest("/error", e, t)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.sendRequest = function (e, t, i) {
  try {
    var r = new $YB.AjaxRequest("", e, this._parseParams(t));
    r.load(i), this._registerRequest(r)
  } catch (n) {
    $YB.error(n)
  }
}, $YB.Communication.prototype.sendService = function (e, t, i) {
  try {
    var r = new $YB.AjaxRequest(this._parseServiceHost(e), "", this._parseParams(t));
    r.load(i), this._registerRequest(r)
  } catch (n) {
    $YB.error(n)
  }
}, $YB.Communication.prototype.addPreloader = function (e) {
  try {
    this._preloaders.push(e)
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Communication.prototype.removePreloader = function (e) {
  try {
    var t = this._preloaders.indexOf(e);
    -1 !== t ? (this._preloaders.splice(t, 1), this._sendRequests()) : $YB.warning("Warning: trying to remove unexisting preloader '" + e + "'.")
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype.checkMandatoryParams = function (e, t) {
  try {
    if ($YB.errorLevel >= 2) {
      for (var i = t.length, r = !0; i--;)("object" == typeof e && !(t[i] in e) || "string" == typeof e && -1 === e.indexOf(t[i] + "=")) && (r = !1, $YB.warning("Warning: Missing mandatory parameter '" + t[i] + "' in the request."));
      return r
    }
    return null
  } catch (n) {
    $YB.error(n)
  }
}, $YB.Communication.prototype._registerRequest = function (e) {
  try {
    return "undefined" == typeof this._requests[this.getViewCode()] && (this._requests[this.getViewCode()] = []), e.append("timemark=" + (new Date).getTime()), this._requests[this.getViewCode()].push(e), this._sendRequests(), e
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Communication.prototype._sendRequests = function () {
  try {
    if (0 === this._preloaders.length)for (var e in this._requests)if (this._requests.hasOwnProperty(e))for (; this._requests[e].length > 0;) {
      var t = this._requests[e].shift();
      t.append("code=" + e), t.host || (t.host = this._parseServiceHost(this.host)), t.send()
    }
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Communication.prototype._parseServiceHost = function (e) {
  try {
    return 0 === e.indexOf("//") ? e = e.slice(2) : 0 === e.indexOf("http://") ? e = e.slice(7) : 0 === e.indexOf("https://") && (e = e.slice(8)), e = this.httpSecure === !0 ? "https://" + e : this.httpSecure === !1 ? "http://" + e : "//" + e
  } catch (t) {
    return $YB.error(t), "http://localhost"
  }
}, $YB.Communication.prototype._parseParams = function (e) {
  try {
    if ("string" == typeof e)return e;
    if ("object" == typeof e) {
      var t = "";
      for (var i in e)"object" == typeof e[i] ? t += encodeURIComponent(i) + "=" + encodeURIComponent(JSON.stringify(e[i])) + "&" : "undefined" != typeof e[i] && "" !== e[i] && null !== e[i] && (t += encodeURIComponent(i) + "=" + encodeURIComponent(e[i]) + "&");
      return t.slice(0, -1)
    }
    return ""
  } catch (r) {
    return $YB.error(r), ""
  }
}, $YB.ConcurrencyService = function (e, t) {
  try {
    this.context = e, this.interval = t || 1e4, this.timer = null, this.sessionId = Math.random(), this.data = this.context.data, this.config = this.context.data.concurrencyConfig, this._init()
  } catch (i) {
    $YB.error(i)
  }
}, $YB.ConcurrencyService.prototype._init = function () {
  try {
    var e = this;
    this.timer = setInterval(function () {
      e._checkConcurrency()
    }, this.interval)
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ConcurrencyService.prototype._checkConcurrency = function () {
  try {
    var e = {};
    e = this.config.ipMode ? {
      accountCode: this.data.accountCode,
      concurrencyCode: this.config.contentId,
      concurrencyMaxCount: this.config.maxConcurrents
    } : {
      accountCode: this.data.accountCode,
      concurrencyCode: this.config.contentId,
      concurrencySessionId: this.sessionId,
      concurrencyMaxCount: this.config.maxConcurrents
    };
    var t = this;
    this.context.comm.sendService(this.config.service, e, function (e) {
      "1" === e.response ? (t.context.handleError({
        errorCode: 14e3,
        msg: "CC_KICK"
      }), "function" == typeof t.config.redirect ? t.config.redirect() : window.location = t.config.redirect) : "0" === e.response || clearInterval(t.timer)
    })
  } catch (i) {
    $YB.error(i)
  }
}, $YB.Data = function (e) {
  try {
    this.setOptions(e)
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Data.prototype = {
  enableAnalytics: !0,
  trackAdvertisements: !0,
  trackSeekEvent: !0,
  parseHLS: !1,
  parseCDNNodeHost: !1,
  hashTitle: !0,
  httpSecure: void 0,
  enableNiceBuffer: !0,
  accountCode: "demosite",
  service: "nqs.nice264.com",
  username: "",
  transactionCode: "",
  isBalanced: 0,
  network: {cdn: "", ip: "", isp: ""},
  media: {isLive: void 0, resource: void 0, title: void 0, duration: void 0},
  properties: {
    filename: "",
    content_id: "",
    content_metadata: {
      title: "",
      genre: "",
      language: "",
      year: "",
      cast: "",
      director: "",
      owner: "",
      duration: "",
      parental: "",
      price: "",
      rating: "",
      audioType: "",
      audioChannels: ""
    },
    transaction_type: "",
    quality: "",
    content_type: "",
    device: {manufacturer: "", type: "", year: "", firmware: ""}
  },
  extraParams: {
    param1: void 0,
    param2: void 0,
    param3: void 0,
    param4: void 0,
    param5: void 0,
    param6: void 0,
    param7: void 0,
    param8: void 0,
    param9: void 0,
    param10: void 0
  },
  concurrencyConfig: {
    enabled: !1,
    contentId: "",
    maxConcurrents: 1,
    service: "pc.youbora.com/cping",
    redirect: "http://www.google.com",
    ipMode: !1
  },
  resumeConfig: {
    enabled: !1,
    contentId: "",
    service: "pc.youbora.com/resume",
    playTimeService: "pc.youbora.com/playTime",
    callback: function (e) {
      console.log("ResumeService Callback: Seek to second " + e)
    }
  },
  smartswitchConfig: {
    enabled: !1,
    type: "balance",
    service: "smartswitch.youbora.com",
    zoneCode: "",
    originCode: "",
    niceNVA: "",
    niceNVB: "",
    token: ""
  },
  setOptions: function (e, t) {
    try {
      if (t = t || this, "undefined" != typeof e)for (var i in e)"object" == typeof t[i] && null !== t[i] ? this.setOptions(e[i], t[i]) : t[i] = e[i]
    } catch (r) {
      $YB.error(r)
    }
  }
}, $YB.DataMap = function () {
}, $YB.DataMap.prototype = {
  _map: {}, getLength: function () {
    return this._map.length
  }, add: function (e, t) {
    return this._map[e] = t
  }, get: function (e) {
    return e = e || "default", this._map.hasOwnProperty(e) === !1 && this.add(e, new $YB.Data), this._map[e]
  }
}, $YB.datamap = new $YB.DataMap, $YB.Pinger = function (e, t, i) {
  try {
    this.time = 0, this.context = e, this.interval = i || 5e3, this.isRunning = !1, this.callback = t, this.timer = null
  } catch (r) {
    $YB.error(r)
  }
}, $YB.Pinger.prototype.getDeltaTime = function () {
  return this.time ? (new Date).getTime() - this.time : -1
}, $YB.Pinger.prototype.start = function () {
  try {
    this.isRunning = !0, this._setPing(), $YB.noticeRequest("Sending pings every " + this.interval + "ms.")
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Pinger.prototype.stop = function () {
  try {
    this.isRunning = !1, clearTimeout(this.timer)
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Pinger.prototype._setPing = function () {
  try {
    if (this.isRunning) {
      var e = this;
      this.time = (new Date).getTime(), this.timer = setTimeout(function () {
        e._ping(e)
      }, this.interval)
    }
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Pinger.prototype._ping = function (e) {
  try {
    e.isRunning && ("function" == typeof e.callback && e.callback(this.getDeltaTime()), e._setPing())
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResourceParser = function (e) {
  try {
    this.context = e, this._init()
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResourceParser.cdnsAvailable = ["Level3", "Akamai"], $YB.ResourceParser.prototype = {
  parseTimeout: null,
  cdns: [],
  realResource: "",
  cdnHost: "",
  cdnTypeString: "",
  cdnType: 0
}, $YB.ResourceParser.prototype._init = function () {
  try {
    this.context.comm.addPreloader("ResourceParser");
    var e = this;
    this.parseTimeout = setTimeout(function () {
      e.realResource = e.context.getResource(), e.context.comm.removePreloader("ResourceParser"), $YB.info("ResourceParser has exceded the maximum execution time (3s) and it will be aborted.")
    }, 3e3), this.context.data.parseHLS ? this.getRealResource(this.context.getResource()) : (this.realResource = this.context.getResource(), this.getNextNode())
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResourceParser.prototype.getRealResource = function (e) {
  try {
    var t = e.slice(e.lastIndexOf("."));
    if (".m3u8" == t) {
      var i = e.slice(0, e.lastIndexOf("/")), r = this;
      new $YB.AjaxRequest(e).load(function (t) {
        var n = /(.*(\.m3u8|\.ts|\.mp4))/i.exec(t.responseText);
        null !== n ? (0 !== n[1].indexOf("http") && (n[1] = i + "/" + n[1]), ".ts" == n[2] || ".mp4" == n[2] ? (r.realResource = n[1], r.getNextNode()) : r.getRealResource(n[1])) : (r.realResource = e, r.getNextNode())
      }).error(function () {
        r.getNextNode()
      }).send()
    } else this.realResource = e, this.getNextNode()
  } catch (n) {
    $YB.error(n)
  }
}, $YB.ResourceParser.prototype.getNextNode = function () {
  try {
    if (this.context.data.parseCDNNodeHost)if (this.cdns = $YB.ResourceParser.cdnsAvailable, this.cdns.length > 0 && !this.cdnHost) {
      var e = this.cdns.shift();
      "function" == typeof this["parseCDN" + e] ? this["parseCDN" + e]() : this.getNextNode()
    } else this.context.comm.removePreloader("ResourceParser"); else this.context.comm.removePreloader("ResourceParser")
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResourceParser.prototype.parseCDNLevel3 = function () {
  try {
    var e = this;
    new $YB.AjaxRequest(this.realResource, "", "", {
      method: "HEAD",
      requestHeader: {"X-WR-DIAG": "host"}
    }).load(function (t) {
      var i = null;
      try {
        i = /Host:(.+)\sType:(.+)/.exec(t.getAllResponseHeaders("X-WR-DIAG"))
      } catch (r) {
        e.getNextNode()
      }
      null !== i && (e.cdnHost = i[1], e.cdnTypeString = i[2], e.parseHeader()), e.getNextNode()
    }).error(function () {
      e.getNextNode()
    }).send()
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResourceParser.prototype.parseCDNAkamai = function () {
  try {
    var e = this;
    new $YB.AjaxRequest(this.realResource, "", "", {method: "HEAD"}).load(function (t) {
      var i = null;
      try {
        i = /(.+)\sfrom\s.+\(.+\/(.+)\).+/.exec(t.getResponseHeader("X-Cache"))
      } catch (r) {
        e.getNextNode()
      }
      null !== i && (e.cdnHost = i[1], e.cdnTypeString = i[2], e.parseHeader()), e.getNextNode()
    }).error(function () {
      e.getNextNode()
    }).send()
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResourceParser.prototype.parseHeader = function () {
  try {
    switch (this.cdnTypeString) {
      case"TCP_HIT":
        this.cdnType = 1;
        break;
      case"TCP_MISS":
        this.cdnType = 2;
        break;
      case"TCP_MEM_HIT":
        this.cdnType = 3;
        break;
      case"TCP_IMS_HIT":
        this.cdnType = 4;
        break;
      default:
        this.cdnType = 0
    }
    $YB.notice("CDN Node Host: " + this.cdnHost + " Type: " + this.cdnTypeString)
  } catch (e) {
    $YB.error(e)
  }
}, $YB.ResumeService = function (e) {
  try {
    this.context = e, this.timer = null, this.isResumed = 0, this.data = this.context.data, this.config = this.context.data.resumeConfig, this._check()
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResumeService.prototype._check = function () {
  try {
    if (this.config.enabled && "undefined" != typeof this.config.contentId && "undefined" != typeof this.data.username) {
      var e = this;
      this.context.comm.sendService(this.config.service, {
        contentId: this.config.contentId,
        userId: this.data.username
      }, function (t) {
        t.response > 0 ? (e.isResumed = 1, "function" == typeof e.config.callback ? e.config.callback(t.response) : $YB.warning("ResumeService callback is not a function")) : "0" === t.response || e.stop()
      }), $YB.noticeRequest("Request: ResumeService check " + this.config.contentId)
    } else this.stop()
  } catch (t) {
    $YB.error(t)
  }
}, $YB.ResumeService.prototype._sendPlayTime = function () {
  try {
    this.config.enabled && "undefined" != typeof this.config.contentId && "undefined" != typeof this.data.username && this.context.comm.sendService(this.config.playTimeService, {
      contentId: this.config.contentId,
      userId: this.data.username,
      playTime: this.context.getPlayhead()
    })
  } catch (e) {
    $YB.error(e)
  }
}, $YB.ResumeService.prototype.start = function (e) {
  try {
    e = e || 6e3, this._sendPlayTime();
    var t = this;
    this.timer = setInterval(function () {
      t._sendPlayTime()
    }, e)
  } catch (i) {
    $YB.error(i)
  }
}, $YB.ResumeService.prototype.stop = function () {
  try {
    clearInterval(this.timer)
  } catch (e) {
    $YB.error(e)
  }
}, $YB.SmartswitchService = function (e) {
  try {
    this.context = e, this.callback = function () {
    }, this.data = this.context.data, this.config = this.context.data.smartswitchConfig
  } catch (t) {
    $YB.error(t)
  }
}, $YB.SmartswitchService.prototype.getBalancedUrls = function (e, t) {
  try {
    if (this.callback = t, this.config.enabled) {
      var i = this;
      this.context.comm.sendService(this.config.service, {
        resource: e,
        systemcode: this.data.accountCode,
        zonecode: this.config.zoneCode,
        session: this.context.comm.getViewCode(),
        origincode: this.config.originCode,
        niceNva: this.config.niceNVA,
        niceNvb: this.config.niceNVB,
        live: this.context.getIsLive(),
        token: this.config.token,
        type: this.config.type
      }, function (e) {
        var t;
        try {
          t = JSON.parse(e.response)
        } catch (r) {
          $YB.warning("Smartswitch said: '" + e.response + "'")
        }
        t ? (i.data.isBalanced = 1, i.callback(t)) : i.callback(!1)
      }), $YB.noticeRequest("Request: Smartswitch " + e)
    } else this.callback(!1)
  } catch (r) {
    $YB.error(r)
  }
}, $YB.Api = function (e, t, i) {
  try {
    if (arguments.length < 2 || void 0 === e || void 0 === t)throw"Fatal Error: $YB.Api constructor needs two arguments at least: plugin and playerId";
    this.plugin = e, this.playerId = t, this.initialOptions = i, this.data = $YB.datamap.get(this.playerId), this.data.setOptions(i);
    var r = this;
    this.pinger = new $YB.Pinger(this, function (e) {
      r.handlePing({diffTime: e})
    }), this.buffer = new $YB.Buffer(this), this.chrono = {
      seek: new $YB.Chrono,
      pause: new $YB.Chrono,
      ads: new $YB.Chrono,
      joinTime: new $YB.Chrono,
      buffer: new $YB.Chrono
    }, this.comm = new $YB.Communication(this.data.service, this.data.httpSecure), this._init()
  } catch (n) {
    $YB.error(n)
  }
}, $YB.Api.prototype = {
  initialOptions: {},
  playerId: "default",
  plugin: null,
  comm: null,
  pinger: null,
  buffer: null,
  resourceParser: null,
  concurrency: null,
  resume: null,
  smartswitch: null,
  chrono: {seek: null, pause: null, ads: null, joinTime: null, buffer: null},
  isStartSent: !1,
  isJoinSent: !1,
  isPaused: !1,
  isSeeking: !1,
  isShowingAds: !1,
  isBuffering: !1,
  lastBitrate: 0
}, $YB.Api.prototype._init = function () {
  try {
    var e = "YAPI Modules Loaded: [Communication] ", t = {
      system: this.data.accountCode,
      pluginVersion: this.plugin.pluginVersion,
      targetDevice: this.plugin.pluginName,
      live: this.data.media.isLive
    }, i = this;
    this.comm.sendData(t, function () {
      i.pinger.interval = 1e3 * i.comm.pingTime
    }), $YB.noticeRequest("Request: NQS /data " + this.data.accountCode), (this.data.parseCDNNodeHost || this.data.parseHLS) && (this.resourceParser = new $YB.ResourceParser(this), e += "[ResourceParser] "), this.data.concurrencyConfig.enabled && (this.concurrency = new $YB.ConcurrencyService(this), e += "[Concurrency] "), this.data.resumeConfig.enabled && (this.resume = new $YB.ResumeService(this), e += "[Resume] "), this.data.smartswitchConfig.enabled && (this.smartswitch = new $YB.SmartswitchService(this), e += "[Smartswitch] "), $YB.notice(e)
  } catch (r) {
    $YB.error(r)
  }
}, $YB.Api.prototype.handleStart = function (e) {
  try {
    this.data.enableAnalytics && (this.isStartSent && this.handleStop(), this.isStartSent = !0, this.chrono.joinTime.start(), this.pinger.start(), this.buffer.autostart && this.buffer.start(), this._consolidateTitle(), e = e || {}, e.system = "undefined" != typeof e.system ? e.system : this.data.accountCode, e.player = "undefined" != typeof e.player ? e.player : this.plugin.pluginName, e.pluginVersion = "undefined" != typeof e.pluginVersion ? e.pluginVersion : this.plugin.pluginVersion, e.playerVersion = "undefined" != typeof e.playerVersion ? e.playerVersion : this.getPlayerVersion(), e.resource = "undefined" != typeof e.resource ? e.resource : this.getResource(), e.duration = "undefined" != typeof e.duration ? e.duration : this.getMediaDuration(), e.live = "undefined" != typeof e.live ? e.live : this.getIsLive(), e.user = "undefined" != typeof e.user ? e.user : this.data.username, e.transcode = "undefined" != typeof e.transcode ? e.transcode : this.data.transactionCode, e.title = "undefined" != typeof e.title ? e.title : this.data.media.title, e.properties = "undefined" != typeof e.properties ? e.properties : this.data.properties, e.hashTitle = "undefined" != typeof e.hashTitle ? e.hashTitle : this.data.hashTitle, e.cdn = "undefined" != typeof e.cdn ? e.cdn : this.data.network.cdn, e.isp = "undefined" != typeof e.isp ? e.isp : this.data.network.isp, e.ip = "undefined" != typeof e.ip ? e.ip : this.data.network.ip, e.param1 = "undefined" != typeof e.param1 ? e.param1 : this.data.extraParams.param1, e.param2 = "undefined" != typeof e.param2 ? e.param2 : this.data.extraParams.param2, e.param3 = "undefined" != typeof e.param3 ? e.param3 : this.data.extraParams.param3, e.param4 = "undefined" != typeof e.param4 ? e.param4 : this.data.extraParams.param4, e.param5 = "undefined" != typeof e.param5 ? e.param5 : this.data.extraParams.param5, e.param6 = "undefined" != typeof e.param6 ? e.param6 : this.data.extraParams.param6, e.param7 = "undefined" != typeof e.param7 ? e.param7 : this.data.extraParams.param7, e.param8 = "undefined" != typeof e.param8 ? e.param8 : this.data.extraParams.param8, e.param9 = "undefined" != typeof e.param9 ? e.param9 : this.data.extraParams.param9, e.param10 = "undefined" != typeof e.param10 ? e.param10 : this.data.extraParams.param10, this.data.resumeConfig.enabled && (this.resume.start(), e.isResumed = this.resume.isResumed), 1 == this.data.isBalanced && (e.isBalanced = 1), this.comm.sendStart(e), $YB.noticeRequest("Request: NQS /start"))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handleJoin = function (e) {
  try {
    this.data.enableAnalytics && !this.isJoinSent && (this.isJoinSent = !0, e = e || {}, e.time = "undefined" != typeof e.time ? e.time : this.chrono.joinTime.stop(), e.eventTime = "undefined" != typeof e.eventTime ? e.eventTime : this.getPlayhead(), e.mediaDuration = "undefined" != typeof e.mediaDuration ? e.mediaDuration : this.getMediaDuration(), this.comm.sendJoin(e), $YB.noticeRequest("Request: NQS /joinTime " + e.time + "ms"))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handleStop = function (e) {
  try {
    this.data.enableAnalytics && this.isStartSent && (this.isStartSent = !1, this.isPaused = !1, this.isJoinSent = !1, this.isSeeking = !1, this.isShowingAds = !1, this.isBuffering = !1, this.pinger.stop(), this.buffer.stop(), e = e || {}, e.diffTime = "undefined" != typeof e.diffTime ? e.diffTime : this.pinger.getDeltaTime(), this.comm.sendStop(e), this.data.resumeConfig.enabled && this.resume.sendPlayTime(), $YB.noticeRequest("Request: NQS /stop"))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handlePause = function () {
  try {
    !this.data.enableAnalytics || !this.isJoinSent || this.isPaused || this.isSeeking || this.isShowingAds || (this.isPaused = !0, this.chrono.pause.start(), this.comm.sendPause(), this.data.resumeConfig.enabled && this.resume.sendPlayTime(), $YB.noticeRequest("Request: NQS /pause"))
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Api.prototype.handleResume = function () {
  try {
    this.data.enableAnalytics && this.isJoinSent && this.isPaused && !this.isSeeking && !this.isShowingAds && (this.isPaused = !1, this.chrono.pause.stop(), this.comm.sendResume(), $YB.noticeRequest("Request: NQS /resume"))
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Api.prototype.startAutobuffer = function () {
  this.data.enableAnalytics && this.data.enableNiceBuffer && (this.buffer.start(), this.buffer.autostart = !0)
}, $YB.Api.prototype.handleBufferStart = function () {
  try {
    this.data.enableAnalytics && this.isJoinSent && !this.isBuffering && (this.isBuffering = !0, this.chrono.buffer.start())
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Api.prototype.handleBufferEnd = function (e) {
  try {
    this.data.enableAnalytics && this.isJoinSent && this.isBuffering && (this.isBuffering = !1, e = e || {}, e.duration = "undefined" != typeof e.duration ? e.duration : this.chrono.buffer.stop(), e.time = "undefined" != typeof e.time ? e.time : this.getPlayhead(), this.getIsLive() && 0 === e.time && (e.time = 1), this.comm.sendBuffer(e), $YB.noticeRequest("Request: NQS /bufferUnderrun " + e.duration + "ms"))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handleError = function (e) {
  try {
    this.data.enableAnalytics && (this._consolidateTitle(), e = e || {}, e.system = "undefined" != typeof e.system ? e.system : this.data.accountCode, e.player = "undefined" != typeof e.player ? e.player : this.plugin.pluginName, e.pluginVersion = "undefined" != typeof e.pluginVersion ? e.pluginVersion : this.plugin.pluginVersion, e.playerVersion = "undefined" != typeof e.playerVersion ? e.playerVersion : this.getPlayerVersion(), e.resource = "undefined" != typeof e.resource ? e.resource : this.getResource(), e.duration = "undefined" != typeof e.duration ? e.duration : this.getMediaDuration(), e.live = "undefined" != typeof e.live ? e.live : this.getIsLive(), e.user = "undefined" != typeof e.user ? e.user : this.data.username, e.transcode = "undefined" != typeof e.transcode ? e.transcode : this.data.transactionCode, e.title = "undefined" != typeof e.title ? e.title : this.data.media.title, e.properties = "undefined" != typeof e.properties ? e.properties : this.data.properties, e.hashTitle = "undefined" != typeof e.hashTitle ? e.hashTitle : this.data.hashTitle, e.cdn = "undefined" != typeof e.cdn ? e.cdn : this.data.network.cdn, e.isp = "undefined" != typeof e.isp ? e.isp : this.data.network.isp, e.ip = "undefined" != typeof e.ip ? e.ip : this.data.network.ip, e.param1 = "undefined" != typeof e.param1 ? e.param1 : this.data.extraParams.param1, e.param2 = "undefined" != typeof e.param2 ? e.param2 : this.data.extraParams.param2, e.param3 = "undefined" != typeof e.param3 ? e.param3 : this.data.extraParams.param3, e.param4 = "undefined" != typeof e.param4 ? e.param4 : this.data.extraParams.param4, e.param5 = "undefined" != typeof e.param5 ? e.param5 : this.data.extraParams.param5, e.param6 = "undefined" != typeof e.param6 ? e.param6 : this.data.extraParams.param6, e.param7 = "undefined" != typeof e.param7 ? e.param7 : this.data.extraParams.param7, e.param8 = "undefined" != typeof e.param8 ? e.param8 : this.data.extraParams.param8, e.param9 = "undefined" != typeof e.param9 ? e.param9 : this.data.extraParams.param9, e.param10 = "undefined" != typeof e.param10 ? e.param10 : this.data.extraParams.param10, this.data.resumeConfig.enabled && (this.resume.start(), e.isResumed = this.resume.isResumed), 1 == this.data.isBalanced && (e.isBalanced = 1), this.comm.sendError(e), $YB.noticeRequest("Request: NQS /error " + e.msg))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handlePing = function (e) {
  try {
    this.data.enableAnalytics && (e = e || {}, e.time = "undefined" != typeof e.time ? e.time : this.getPlayhead(), e.bitrate = "undefined" != typeof e.bitrate ? e.bitrate : this.getBitrate(), this.comm.sendPing(e))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handleSeekStart = function () {
  try {
    this.data.enableAnalytics && this.isJoinSent && (this.isSeeking = !0, this.chrono.seek.start())
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Api.prototype.handleSeekEnd = function (e) {
  try {
    this.data.enableAnalytics && this.isJoinSent && (this.isSeeking = !1, e = e || {}, e.duration = "undefined" != typeof e.duration ? e.duration : this.chrono.seek.stop(), e.time = "undefined" != typeof e.time ? e.time : this.getPlayhead(), this.comm.sendSeek(e), $YB.noticeRequest("Request: NQS /seek " + e.duration + "ms"))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.handleAdStart = function () {
  try {
    this.data.enableAnalytics && this.isJoinSent && !this.isShowingAds && (this.isShowingAds = !0, this.chrono.ads.start())
  } catch (e) {
    $YB.error(e)
  }
}, $YB.Api.prototype.handleAdEnd = function (e) {
  try {
    this.data.enableAnalytics && this.isJoinSent && this.isShowingAds && (this.isShowingAds = !1, e = e || {}, e.duration = "undefined" != typeof e.duration ? e.duration : this.chrono.ads.stop(), e.time = "undefined" != typeof e.time ? e.time : this.getPlayhead(), this.comm.sendAds(e), $YB.noticeRequest("Request: NQS /ads " + e.duration + "ms"))
  } catch (t) {
    $YB.error(t)
  }
}, $YB.Api.prototype.getResource = function () {
  try {
    return this.resourceParser && this.resourceParser.realResource ? this.resourceParser.realResource : "undefined" != typeof this.data.media.resource ? this.data.media.resource : "function" == typeof this.plugin.getResource ? this.plugin.getResource() : ""
  } catch (e) {
    return $YB.error(e), ""
  }
}, $YB.Api.prototype.getPlayhead = function () {
  try {
    return "function" == typeof this.plugin.getPlayhead ? Math.round(this.plugin.getPlayhead()) : 0
  } catch (e) {
    return $YB.error(e), 0
  }
}, $YB.Api.prototype.getMediaDuration = function () {
  try {
    if ("undefined" != typeof this.data.media.duration)return this.data.media.duration;
    if ("function" == typeof this.plugin.getMediaDuration) {
      var e = this.plugin.getMediaDuration();
      return 0 === e || e == 1 / 0 || isNaN(e) ? 0 : Math.round(e)
    }
    return 0
  } catch (t) {
    return $YB.error(t), 0
  }
}, $YB.Api.prototype.getIsLive = function () {
  try {
    return "undefined" != typeof this.data.media.isLive ? this.data.media.isLive : "function" == typeof this.plugin.getIsLive && "boolean" == typeof this.plugin.getIsLive() ? this.plugin.getIsLive() : !1
  } catch (e) {
    return $YB.error(e), !1
  }
}, $YB.Api.prototype.getBitrate = function () {
  try {
    if ("function" == typeof this.plugin.getBitrate && -1 != this.plugin.getBitrate())return Math.round(this.plugin.getBitrate());
    if ("undefined" != typeof this.plugin.video && "undefined" != typeof this.plugin.video.webkitVideoDecodedByteCount) {
      var e = this.plugin.video.webkitVideoDecodedByteCount;
      return this.lastBitrate && (e = Math.round((this.plugin.video.webkitVideoDecodedByteCount - this.lastBitrate) / 5 * 8)), this.lastBitrate = this.plugin.video.webkitVideoDecodedByteCount, e
    }
    return -1
  } catch (t) {
    return $YB.error(t), -1
  }
}, $YB.Api.prototype.getPlayerVersion = function () {
  try {
    return "function" == typeof this.plugin.getPlayerVersion && this.plugin.getPlayerVersion() ? this.plugin.getPlayerVersion() : ""
  } catch (e) {
    return $YB.error(e), ""
  }
}, $YB.Api.prototype._consolidateTitle = function () {
  try {
    this.data && this.data.media.title && (this.data.properties.content_metadata ? this.data.properties.content_metadata.title = this.data.media.title : this.data.properties.content_metadata = {
      title: this.data.media.title
    })
  } catch (e) {
    $YB.error(e)
  }
};
/**
 * @license
 * Plugin 3.1.0-videojs5 <http://youbora.com/>
 * Copyright NicePopleAtWork <http://nicepeopleatwork.com/>
 */
if ("undefined" != typeof $YB) {
  $YB.plugins.Videojs5 = function (t) {
    try {
      this.pluginName = "pc-videojs5", this.pluginVersion = "3.1.0-videojs5", this.getPlayhead = function () {
        return this.currentTime()
      }, this.getMediaDuration = function () {
        return Math.round(this.duration())
      }, this.getIsLive = function () {
        var t = this.duration();
        return isNaN(t) || t === 1 / 0 || 0 >= t
      }, this.getBitrate = function () {
        return -1
      }, this.getResource = function () {
        return this.currentSrc()
      }, this.getPlayerVersion = function () {
        return videojs.CDN_VERSION
      }, this.yapi = new $YB.Api(this, this.id(), t), this.ready(function () {
        try {
          $YB.notice("Plugin " + this.pluginVersion + " is ready."), this.yapi.startAutobuffer(), $YB.util.listenAllEvents(this)
        } catch (t) {
          $YB.error(t)
        }
      }), this.on("play", function (t) {
        try {
          this.yapi.isStartSent || this.yapi.handleStart({duration: 0})
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("playing", function (t) {
        try {
          this.yapi.isStartSent && this.yapi.isPaused && this.yapi.handleResume()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("timeupdate", function (t) {
        try {
          this.yapi.isStartSent && !this.yapi.isJoinSent && this.currentTime() > .1 && this.yapi.handleJoin()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("pause", function (t) {
        try {
          this.yapi.handlePause()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("abort", function (t) {
        try {
          this.yapi.handleStop()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("ended", function (t) {
        try {
          this.yapi.handleStop()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("error", function (t) {
        try {
          this.yapi.handleError({msg: this.error().message, errorCode: this.error().code}), this.yapi.handleStop()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("seeking", function (t) {
        try {
          this.yapi.handleSeekStart()
        } catch (i) {
          $YB.error(i)
        }
      }), this.on("seeked", function (t) {
        try {
          this.yapi.handleSeekEnd()
        } catch (i) {
          $YB.error(i)
        }
      })
    } catch (i) {
      $YB.error(i)
    }
  };
  try {
    "undefined" != typeof videojs && videojs.plugin("youbora", $YB.plugins.Videojs5)
  } catch (err) {
    $YB.error(err)
  }
}
