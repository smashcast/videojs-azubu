import * as EVENTS from '../util/event';

let allowedHosts = '*';
let events = [
  EVENTS.SUSPEND, EVENTS.PAUSE, EVENTS.PLAY, EVENTS.PLAYING, EVENTS.SEEKING, EVENTS.SEEKED,
  EVENTS.VOLUME_CHANGE, EVENTS.WAITING, EVENTS.FIRST_PLAY, EVENTS.INACTIVE, EVENTS.ACTIVE,
  EVENTS.READY, EVENTS.RECONNECTING, EVENTS.RECONNECTED, EVENTS.AD_START, EVENTS.AD_END, EVENTS.TIME_UPDATE
];

export const init = (player, settings) => {

  if (settings.allowedHosts) {
    allowedHosts = settings.allowedHosts;
  }

  // Notify Host of Player Events
  player.on(events, (event) => {
    window.top.postMessage(JSON.stringify({
      type: event.type,
      seekable: player.seekable(),
      buffered: player.buffered(),
      duration: player.duration(),
      currentTime: player.currentTime()
    }), allowedHosts);
  });

  // In theory this is where the Host could query the player
  window.addEventListener('message', (event) => {
    //console.warn('AMP', event.origin);

    if (event.data.type) {

      console.warn('AMP Received', event.data);

      switch (event.data.type) {
        case EVENTS.PLAY:
          player.play();
          break;

        case EVENTS.PAUSE:
          player.pause();
          break;

        case 'currentTime':
          window.top.postMessage(JSON.stringify(player.currentTime()), allowedHosts);
          break;

        case 'buffered':
          window.top.postMessage(JSON.stringify(player.buffered()), allowedHosts);
          break;

        case 'status':
          window.top.postMessage(JSON.stringify(player), allowedHosts);

        default:
          break;
      }
    }
  });
};
