const HTTP_VERBS = {
  GET: 'GET'
};

const REGEX = {
  CHANNEL: '[CHANNEL]',
  USER: '[USER]'
};

const HOSTS = {
  PUBLIC: 'api.azubu.tv',
  ADS: 'ads.azubu.tv',
  // Overlays API
  OVERLAYS_AZUBU: 'www.azubu.tv',
  OVERLAYS_S3: 's3-us-west-1.amazonaws.com'
};

const ENDPOINTS = {
  // Channel APIs
  GET_CHANNEL_ADS: '/public/ads/source/' + REGEX.CHANNEL,
  GET_CHANNEL_INFO: '/public/channel/' + REGEX.CHANNEL,
  GET_CHANNEL_RESTRICTIONS: '/public/modules/player/restrictions',
  // User APIs
  GET_USER_DATA: '/auth/decrypt/user-cookie?callback=userData',
  GET_USER_SUBSCRIPTIONS: '/public/modules/subscriptions/' + REGEX.USER,
  // Overlays APIs
  GET_OVERLAY_CHANNELS: '/azububcc/overlay_channels.json',
  GET_OVERLAY_BROADCASTER_INFO: '/urest/user/broadcaster/' + REGEX.CHANNEL
};

const getURLFromParts = (host, endpoints) => {
  return 'http://' + host + endpoints ;
};

export const getChannelInfoByChannelName = (channelName, callback) => {
  let xhr = new XMLHttpRequest();
  let handleReadyStateChange = (event) => {
    if(event.target.readyState === 4) {
      callback(JSON.parse(event.target.response).data, event.target.status);
    }
  };

  xhr.open(HTTP_VERBS.GET, getURLFromParts(HOSTS.PUBLIC, ENDPOINTS.GET_CHANNEL_INFO.replace(REGEX.CHANNEL, channelName)), true);
  xhr.onreadystatechange = handleReadyStateChange;
  xhr.send();
};

export const getChannelRestrictions = (callback) => {
  let xhr = new XMLHttpRequest();
  let handleReadyStateChange = (event) => {
    if(event.target.readyState === 4) {
      callback(JSON.parse(event.target.response), event.target.status);
    }
  };

  xhr.open(HTTP_VERBS.GET, getURLFromParts(HOSTS.PUBLIC, ENDPOINTS.GET_CHANNEL_RESTRICTIONS, true));
  xhr.onreadystatechange = handleReadyStateChange;
  xhr.send();
};

// USER API CALLS
export const getUserName = (callback) => {
  let script = document.createElement('script');
  script.src = getURLFromParts(HOSTS.PUBLIC, ENDPOINTS.GET_USER_DATA, true);
  window.userData = (data) => {
    if(callback) {
      if(data.username === null) data.username = 'Guest';
      callback(data, (data.username) ? 200 : 404);
    }
  };
  window.document.getElementsByTagName('head')[0].appendChild(script);
};

export const getUserSubscriptions = (userName, callback) => {
  let xhr = new XMLHttpRequest();
  let handleReadyStateChange = (event) => {
    if(event.target.readyState === 4) {
      callback(JSON.parse(event.target.response), event.target.status);
    }
  };

  xhr.open(HTTP_VERBS.GET, getURLFromParts(HOSTS.PUBLIC, ENDPOINTS.GET_USER_SUBSCRIPTIONS.replace(REGEX.USER, userName)), true);
  xhr.onreadystatechange = handleReadyStateChange;
  xhr.send();
};

// ADVERTISEMENT API CALLS
export const getAdvertisementsByChannelName = (channelName, callback) => {
  let xhr = new XMLHttpRequest();
  let handleReadyStateChange = (event) => {
    if(event.target.readyState === 4) {
      if(event.target.status === 0) {
        console.log('Ad Blocked');
        callback(null, event.target.status);
      } else {
        callback(JSON.parse(event.target.response).data, event.target.status);
      }

    }
  };

  xhr.open(HTTP_VERBS.GET, getURLFromParts(HOSTS.PUBLIC, ENDPOINTS.GET_CHANNEL_ADS.replace(REGEX.CHANNEL, channelName)), true);
  xhr.onreadystatechange = handleReadyStateChange;
  xhr.send();
};

// OVERLAY API CALLS
export const getOverlayChannels = (callback, player, options) => {
  try{
    let xhr = new XMLHttpRequest();
    let handleReadyStateChange = (event) => {
      if(event.target.readyState === 4) {
        callback(JSON.parse(event.target.response), event.target.status, player, options);
      }
    };

    xhr.open(HTTP_VERBS.GET, getURLFromParts(HOSTS.OVERLAYS_S3, ENDPOINTS.GET_OVERLAY_CHANNELS, true));
    xhr.onreadystatechange = handleReadyStateChange;
    xhr.send();
  }catch(ex){
    // console.log('VJS - OVERLAYS:  getOverlayChannels Error', ex);
  }
};

export const getBroadcasterInfoByChannelName = (channelName, callback) => {
  try{
    let xhr = new XMLHttpRequest();
    let handleReadyStateChange = (event) => {
      if(event.target.readyState === 4) {
        callback(JSON.parse(event.target.responseText), event.target.status);
      }
    };

    xhr.open(HTTP_VERBS.GET, getURLFromParts(HOSTS.OVERLAYS_AZUBU, ENDPOINTS.GET_OVERLAY_BROADCASTER_INFO.replace(REGEX.CHANNEL, channelName)), true);
    xhr.onreadystatechange = handleReadyStateChange;
    xhr.send();
  }catch(ex){
    // console.log('VJS - OVERLAYS:  getBroadcasterInfoByChannelName Error', ex);
  }
};
