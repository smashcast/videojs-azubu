import * as AKAMAI from './vendors/akamai/akamai';
import * as YOUBORA from './vendors/npaw/youbora';
// import * as MUX from './vendors/mux/mux';

export const init = function(player, options) {

  // Init Akamai
  AKAMAI.init(player);

  // Init Youbora
  YOUBORA.init(player, options);

  // Init Mux
  // MUX.init(player);

};
