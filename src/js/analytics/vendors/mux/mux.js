import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';

const PROPERTY_KEYS = {
  LOCALHOST: 'AZUBU-LOCALHOST:JWLNXQW1IY40VCU',
  AMP_DEMO: 'AZUBU-AMP-DEMO:VHZKMCLL6EXH9W4',
  PROD: 'afyYaU0ttFPu07pxlcHFpN-iO'
};

const ENV = {
  local: {
    domains: ['localhost', 'tjohnson.azubu.tv', 'tdinsmore.azubu.tv'],
    key: PROPERTY_KEYS.LOCALHOST
  },
  demo: {
    domains: ['amp.azubu.tv', 'everest.azubu.tv', 'qa.azubu.it', 'qa2.azubu.it', 'stg.azubu.t'],
    key: PROPERTY_KEYS.AMP_DEMO
  },
  production: {
    domains: ['www.azubu.tv', 'uol.com.br'],
    key: PROPERTY_KEYS.PROD
  }
};

const handleLoadStart = () => {

  let info = player.mediainfo;

  if (info) {
    let config = {
      video_id: info.reference_id,
      video_title: info.name,
      video_series: UTIL.getGameNameFromTags(info.tags),
      video_variant_name: "",
      video_variant_id: info.id,
      video_language: "",
      video_content_type: (info.duration && info.duration < 0) ? "broadcast" : "replay",
      video_duration: (info.duration && info.duration < 0) ? Infinity : info.duration,
      video_stream_type: (info.duration && info.duration < 0) ? "live" : "on-demand",
      video_producer: UTIL.getChannelNameFromTags(info.tags),
      video_encoding_variant: ""
    };

    player.mux.changeVideo(config);

    player.off([EVENTS.LOAD_START, EVENTS.LOADED_METADATA, EVENTS.AD_START], handleLoadStart);

  } else {
    console.warn('no MediaInfo', player);
  }
};

const getInitialPropertyKey = () => {
  let returnValue = PROPERTY_KEYS.PROD;

  for(var i in ENV) {
    ENV[i].domains.forEach(function(value) {
      if(document.referrer.indexOf(value) !== -1 || window.location.host.indexOf(value) !== -1) {
        returnValue = ENV[i].key;
        return;
      }
    })
  }

  return returnValue;
};

export const init = (player, options) => {

  player.mux({
    debug: false,
    data: {
      property_key: getInitialPropertyKey(),
      player_version: player.options_["data-player"],
      player_name: 'Everest',
      viewer_user_id: UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.USER_NAME) || 'Unknown'
    }
  });

  player.one([EVENTS.LOAD_START, EVENTS.LOADED_METADATA, EVENTS.AD_START], handleLoadStart);
};
