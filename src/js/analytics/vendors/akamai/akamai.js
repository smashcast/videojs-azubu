import * as UTIL from '../../../util/util';
import * as EVENTS from '../../../util/event';

const URLS = {
  BEACON: '//ma330-r.analytics.edgekey.net/config/beacon-15459.xml',
  SDK: '//79423.analytics.edgekey.net/js/brightcove-csma.js'
};

const DATA_NAMES = {
  PLAYER_ID: 'playerId',
  REFERENCE_ID: 'referenceId',
  STREAM_NAME: 'std:streamName',
  TITLE: 'title'
};

let akamaiAnalyticsPlugin = null;
let akamaiAvailable = false;

const handleSdkLoaded = () => {
  akamaiAvailable = true;
  akamaiAnalyticsPlugin = player.akamaiAnalytics({"configPath":URLS.BEACON});

  reportData(DATA_NAMES.PLAYER_ID, player.options_["data-player"]);
};

var handleLoadedMetadata = function() {
  // set the custom params
  try{
    var parser = document.createElement('a');
    parser.href = player.currentSrc();

    if(player.mediainfo.title) {
      reportData(DATA_NAMES.TITLE, player.mediainfo.title);
    } else if (player.mediainfo.name) {
      reportData(DATA_NAMES.TITLE, player.mediainfo.name);
    }

    if(player.mediainfo.reference_id) {
      reportData(DATA_NAMES.REFERENCE_ID, player.mediainfo.reference_id);
    }

    if(parser.pathname) {
      reportData(DATA_NAMES.STREAM_NAME, parser.pathname);
    }

    player.off(EVENTS.LOADED_METADATA, handleLoadedMetadata);

  } catch(err) {
    console.warn('[Analytics]', 'Error firing analytics custom params');
  }
};

const reportData = function(name, value) {
  if(!akamaiAvailable) {
    console.warn('Akamai Not Available');
  }

  if(akamaiAvailable && akamaiAnalyticsPlugin && value) {
    //console.log('Report Data', name, value);
    akamaiAnalyticsPlugin.setData(name, value);
  } else {
    console.warn('Unable to Report Data', name);
    console.warn(name, value);
  }
};

export const init = (player) => {
  if(!player.akamaiAnalytics) {
    UTIL.loadScriptWithCallback(URLS.SDK, handleSdkLoaded);
  } else {
    handleSdkLoaded();
  }

  player.one(EVENTS.LOADED_METADATA, handleLoadedMetadata);

};
