import videojs from 'video.js';

import $YB from './youboralib';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';

let VERSION = 'undefined';

const defaults = {
  accountCode: "azubu",
  username: "Unknown User",
  transactionCode: "Everest_" + VERSION,
  media: {
    title: '',
    isLive: true
  },
  properties: {
    content_metadata: {
      genre: "",
      language: "",
      year: "",
      cast: "",
      director: "",
      owner: "",
      parental: "",
      price: "",
      rating: "",
      audioType: "",
      audioChannels: ""
    },
    transaction_type: "Free",
    quality: "HD",
    content_type: "",
    device: {
      manufacturer: "",
      type: "",
      year: "",
      firmware: ""
    }
  }
};

const getConfigByPlayer = (player) => {

  let returnConfig, info = player.mediainfo;

  if(info) {
    returnConfig = videojs.mergeOptions(defaults, {
      username: UTIL.getUserID(),
      transactionCode: "Everest_" + VERSION,
      media: {
        title: (info.reference_id) ? info.reference_id : "Unknown",
        duration: (info.duration && info.duration < 1) ? Infinity : info.duration,
        isLive: (info.duration && info.duration < 1),
        resource: player.currentSrc()
      },
      parseHLS: false,
      properties: {
        content_id: info.reference_id || "",
        filename: player.currentSrc()
      },
      extraParams: {
        user: UTIL.getUserID(),
        channel: (info && info.tags) ? UTIL.getChannelNameFromTags(info.tags) : "",
        game: (info && info.tags) ? UTIL.getGameNameFromTags(info.tags): "",
        tags: (info && info.tags) ? info.tags: "",
        brightcove_id: (info && info.id) ? info.id : ""
      }
    })
  } else {
    returnConfig = videojs.mergeOptions(defaults, {
      username: UTIL.getUserID(),
      transactionCode: "Everest_" + VERSION,
      media: {
        title: player.currentSrc().substr(player.currentSrc().lastIndexOf('/'), player.currentSrc().length),
        duration: player.duration(),
        isLive: (player.duration() === Infinity),
        resource: player.currentSrc()
      },
      properties: {
        content_id: player.currentSrc().substr(player.currentSrc().lastIndexOf('/'), player.currentSrc().length),
        filename: player.currentSrc()
      }
    });
  }

  return returnConfig;

};

export const init = (player, settings) => {
  VERSION = settings.VERSION;

  window.$YB = $YB;

  $YB.plugins.Html5 = function(playerId, options) {
    this.init(playerId, options);
    this.registerListeners();
  };

  $YB.plugins.Html5.prototype = new $YB.plugins.Generic();

  $YB.plugins.Html5.prototype.registerListeners = function() {
    var context = this;

    this.player = player;

    this.player.on(EVENTS.ERROR, function(){
      let errCode = this.error().code || -1;
      let errMessage = this.error().message || 'Unknown Error';
      context.errorHandler(errCode, errMessage);
    });

    this.player.on(EVENTS.LOAD_START, function(){
      context.endedHandler();
      context.playHandler(getConfigByPlayer(player));
    });

    this.player.on(EVENTS.PLAYING, function(e) {
      context.playingHandler();
    });

    this.player.on(EVENTS.PAUSE, function(e) {
      context.pauseHandler();
    });

    this.player.on(EVENTS.ENDED, function(e) {
      context.endedHandler();
    });

    this.player.on(EVENTS.SEEKING, function(e) {
      context.seekingHandler();
    });

    this.player.on(EVENTS.SEEKED, function(e) {
      context.seekedHandler();
    });

    this.player.on(EVENTS.AD_START, function(e) {
      context.genericAdStartHandler();
    });

    this.player.on(EVENTS.AD_END, function(e) {
      context.genericAdEndHandler();
    });

    this.player.on("buffering", function(e) {
      context.bufferingHandler();
    });

    this.player.on("buffered", function(e) {
      context.bufferedHandler();
    });

    window.onunload = function() {
      context.endedHandler();
    };

    context.startAutobuffer();
  };

  $YB.plugins.Html5.prototype.getMediaDuration = () => {
    return player.duration();
  };

  $YB.plugins.Html5.prototype.getPlayhead = () => {
    return player.currentTime();
  };

  $YB.plugins.Html5.prototype.getResource = () => {
    return player.currentSrc();
  };

  $YB.plugins.Html5.prototype.getIsLive = () => {
    return (player.duration() === Infinity);
  };

  $YB.plugins.Html5.prototype.getBitrate = () => {
    let returnValue = -1;
    try{
      if(player.tech_ && player.tech_.hls && player.tech_.hls.playlists.media().attributes.BANDWIDTH
        && typeof player.tech_.hls.playlists.media().attributes.BANDWIDTH === 'number') {
        returnValue = player.tech_.hls.playlists.media().attributes.BANDWIDTH;
      }
    } catch (err){

    }
    return returnValue;
  };

  player.youboraPlugin = new $YB.plugins.Html5(player.id(), {accountCode: defaults.accountCode});
};
