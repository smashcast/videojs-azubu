import videojs from 'video.js';

import 'videojs-contrib-ads/dist/videojs.ads.global';
import 'videojs-ima/src/videojs.ima';

import * as ENV from '../util/environment';
import * as EVENTS from '../util/event';
import * as UTIL from '../util/util';
import * as API from '../communication/api';

const IMA_SDK_URL = '//imasdk.googleapis.com/js/sdkloader/ima3.js';
const LOCAL_DOMAIN = 'localhost';

const REGEX = {
  BROWSER: {
    PRODUCTION: 'AzubuProdBrowser',
    DEVELOPMENT: 'AzubuDevBrowser'
  },
  EMBED: {
    EXTERNAL: 'EmbeddedLive',
    INTERNAL: 'Live'
  }
};

let subs, override_, player, channelname, adTagURLs = null;

const getAdTagByData = (data) => {
  if(ENV.isAzubuEmbed()) {
    if(player.duration() === Infinity && data['embed_ad_source_live']) {
      console.warn('Live Embed Ad');
      return data['embed_ad_source_live'];
    } else if (player.duration() !== Infinity && data['embed_ad_source_vod']) {
      console.warn('VOD Embed Ad');
      return data['embed_ad_source_vod'];
    } else {
      console.warn ('Unknown EMBED ad');
      return '';
    }
  } else {
    if(player.duration() === Infinity && data['direct_ad_source_live']) {
      console.warn('Live Direct Ad');
      return data['direct_ad_source_live'];
    } else if (player.duration() !== Infinity && data['direct_ad_source_vod']) {
      console.warn('VOD Direct Ad');
      return data['direct_ad_source_vod'];
    } else {
      console.warn ('Unknown DIRECT ad');
      return '';
    }
  }
};

const getFormattedAdTagUrl = (url, breaklength = -1) => {
  if(!url || typeof url === 'undefined') {
    console.warn('Undefined Ad Tag');
    return null;
  }

  if (window.location.hostname === LOCAL_DOMAIN) {
    url = url.replace(REGEX.BROWSER.PRODUCTION, REGEX.BROWSER.DEVELOPMENT);
  }

  if (breaklength !== -1) {
    url += '&breaklength=' + breaklength
  }

  return url;
};

/**
 * Azubu Event Handlers (Broadcaster Data / Subscriber)
 * =====================================================================
 */

const handleUserDataChange = (event, data) => {
  console.warn(event.type, data);
};

const handleSubscriptionDataChange = (event, data) => {
  console.warn(event.type, data);
};

const handleLoadStart = () => {
  if (player.mediainfo) {
    console.warn('Getting Ads', UTIL.getChannelNameFromTags(player.mediainfo.tags));
    API.getAdvertisementsByChannelName(UTIL.getChannelNameFromTags(player.mediainfo.tags), getAdTagUrlCallback);
  }
};


/**
 * SDK Events Handlers
 * =====================================================================
 */

const handleSDKLoaded = () => {
  console.warn('SDK Loaded');

  setupLiveMidroll();

  player.on(EVENTS.AD_START, handleAdStart);
  player.on(EVENTS.AD_END, handleAdEnd);
  player.on(EVENTS.AD_ERROR, handleAdError);
  player.on(EVENTS.ADS_READY, handleAdReady);
  player.on(EVENTS.LOAD_START, handleLoadStart);
  player.on(EVENTS.SUBSCRIPTION_DATA_CHANGE, handleSubscriptionDataChange);
  player.on(EVENTS.USER_DATA_CHANGE, handleUserDataChange);

};

const handleSDKError = () => {
  console.warn('Ad Block : SDK Error');
  overridden(true);
};

/**
 * Ad Events Handlers
 * =====================================================================
 */

const handleAdStart = () => {
  player.snapshot = {
    paused: player.paused(),
    currentTime: player.currentTime(),
    livePoint: player.seekable().end(0),
    date: new Date().valueOf()
  };
  document.getElementById('ima-ad-container').style.display = 'block';

  console.warn('Ad Start');
  console.log(player.snapshot);
};

const handleAdEnd = () => {
  let timePassedWatchingAd = parseInt((new Date().valueOf() - player.snapshot.date) / 1000, 10);
  document.getElementById('ima-ad-container').style.display = 'none';

  console.warn('AD END, Watched', timePassedWatchingAd, 'Seconds');
  console.warn(player.seekable().start(0), player.seekable().end(0), player.currentTime());
};

const handleAdError = (e) => {
  console.warn('Ad Error', e.type);
  document.getElementById('ima-ad-container').style.display = 'none';
};

const handleAdReady = (e) => {
  console.warn('Ad Ready', e.type);
};

/**
 * =====================================================================
 */



// API
const getAdTagUrlCallback = (data, status) => {
  console.warn('getAdTagUrlCallback', status);
  if(status !== 0) {
    adTagURLs = data;
    initIMA();
  }
};

// IMA
const overridden = (value) => {
  if (value !== null && typeof value === 'boolean') override_ = value;

  return override_;
};

const subscribed = () => {
  if(player && player.mediainfo && player.mediainfo.tags && subs) {
    let channel = UTIL.getChannelNameFromTags(player.mediainfo.tags);
    return subs.indexOf(channel) !== -1;
  }

  return false;
};

const initIMA = () => {
  console.warn('Init IMA');
  if (overridden()) {
    console.warn('Ad Skipped: Override');
  } else if (subscribed()) {
    console.warn('Ad Skipped: Subscribed');
  } else if (ENV.isAdvertisementWhitelistedUrl()) {
    console.warn('Ad Skipped: Whitelisted');
  } else {
    let defaults = {
      id: player.id(),
      adTagUrl: getFormattedAdTagUrl(getAdTagByData(adTagURLs)),
      showCountdown: true,
      debug: true,
      vpaidMode: 2,
      numRedirects: 4,
      contribAdsSettings: {
        prerollTimeout: 10000
      }
    };

    console.log('Fire Ad', defaults.adTagUrl);

    try{
      player.ima(defaults);

      player.ima.seekContentToZero_ = function() {
        console.warn('IMA SCTZ');
        player.off(EVENTS.LOADED_METADATA, player.ima.seekContentToZero_);
        //player.currentTime(player.seekable().end(0));
      };

      player.ima.playContentFromZero_ = function() {
        console.warn('IMA PCTZ');
        player.off(EVENTS.LOADED_METADATA, player.ima.playContentFromZero_);
        //player.currentTime(player.seekable().end(0));
      };

      player.ima.initializeAdDisplayContainer();
      player.ima.requestAds();
    } catch(err) {
      console.log('err');
    }

  }
};

const handleTrackAdded = function() {
  if(player.mediainfo && player.mediainfo.reference_id && player.duration() === Infinity) {
    player.textTracks()[player.textTracks().length-1].ref = player.mediainfo.reference_id;
    if(typeof player.textTracks()[player.textTracks().length-1].on === 'function') {
      player.textTracks()[player.textTracks().length-1].on(EVENTS.CUE_CHANGE, function(){
        triggerMidroll(getMidrollLengthByTrack(player.textTracks()[player.textTracks().length-1]));
      })
    } else if(typeof player.textTracks()[player.textTracks().length-1].oncuechange === 'object') {
      player.textTracks()[player.textTracks().length-1].oncuechange = function() {
        triggerMidroll(getMidrollLengthByTrack(player.textTracks()[player.textTracks().length-1]));
      }
    }
  }
};

const getMidrollLengthByTrack = (track) => {
  var requestedAdDuration, cue, cueString, currentCueIndex = (track.activeCues !== null && track.activeCues.length) ? track.activeCues.length-1 : 0;

  if (track.activeCues && track.activeCues.length) {
    cueString = track.activeCues[currentCueIndex].text;

    //When reading the cues, there was a null char at the end of the text
    //that was making the JSON.parse below throw.  Filtering out any terminating
    //null chars.  I think this is a bug in the ID3 parser though.
    if (cueString.charCodeAt(cueString.length-1) === 0) {
      cueString = cueString.slice(0,-1);
    }

    try {
      cue = JSON.parse(cueString);
    }catch(e) {
      // log out invalid JSON
      videojs.log("ERROR: Parsing JSON. Please confirm that JSON is valid.");
    }

    if (cue.name === 'adCue') {
      var parameters = cue.parameters, duration = parameters.duration;

      requestedAdDuration = isNaN(Number(duration)) ? 0 : Number(duration);

      return requestedAdDuration;
    }
  }

  return -1;
};

const buildMidrollRequestByTrack = (midrollUrl, breaklength = -1) => {
  if (overridden()) {
    console.warn('Ad Skipped: Override');
  } else if (subscribed()) {
    console.warn('Ad Skipped: Subscribed');
  } else if (ENV.isAdvertisementWhitelistedUrl()) {
    console.warn('Ad Skipped: Whitelisted');
  } else {
    player.ima.setContent(null, getFormattedAdTagUrl(midrollUrl, breaklength), true);
    player.ima.requestAds();
  }
};

const triggerMidroll = (length=-1) => {
  if(length !== null && length !== -1) {
    console.warn('Trigger Midroll', length);
    buildMidrollRequestByTrack(getAdTagByData(adTagURLs), length);
  }
};

const setupLiveMidroll = () => {
  player.triggerMidroll = triggerMidroll;

  if(typeof player.textTracks().on === 'function') {
    player.textTracks().on(EVENTS.TRACK_ADDED, videojs.bind(this, handleTrackAdded));
  } else if(typeof player.textTracks().onaddtrack === 'object') {
    player.textTracks().onaddtrack = handleTrackAdded;
  }
};

export const init = (playerRef) => {

  if(videojs.IS_IPHONE) {
   return;
  }

  player = playerRef;

  UTIL.loadScriptWithCallback(IMA_SDK_URL, handleSDKLoaded, handleSDKError);

};
