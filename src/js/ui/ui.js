import * as CONTROLS from './controls/controls';
import * as STAGE from './stage/stage';
import * as OVERLAYS from './overlays/overlays';

export const init = (player, options) => {
  // Init Controls
  CONTROLS.init(player, options);
  // Init Stage
  STAGE.init(player, options);
  // Init Overlays
  OVERLAYS.init(player, options);
};
