import videojs from 'video.js';

let Component = videojs.getComponent('Component');

export default class AzubuImageOverlay extends Component {
  constructor(player,options) {

    try{

      // Base constructor
      super(player,options);

      // Capture the videojs player
      this.vjs_player = player;

      // Add Click Handlers
      this.on(this.imageButton, 'click', this.handleImageButtonClick);
      this.on(this.closeButton, 'click', this.handleCloseButtonClick);

      // console.log('EVEREST - OVERLAYS:  AzubuImageOverlay: constructor');
    }catch(ex){
      // console.log('EVEREST - OVERLAYS:  AzubuImageOverlay Constructor error', ex);
      return null;
    }
  }

  createEl(){
    try{

      // Create the container DOM Element
      let el_ = super.createEl('div', {
        className: this.buildCSSClass() + 'vjs-azubu-image-overlay-container'
      });

      // console.log('EVEREST - OVERLAYS:  Overlay Image createEl ');

      // Create the image element
      this.imageButton = super.createEl('div', {className: 'vjs-azubu-image-overlay'});

      el_.appendChild(this.imageButton);

      // Create the close button container element
      this.closeButtonContainer = super.createEl('div', {className: 'vjs-azubu-image-overlay-close-button-container'});

      // Create the close button element
      this.closeButton = super.createEl('div', {className: 'vjs-azubu-image-overlay-close-button'});

      // Add close button to close button container
      this.closeButtonContainer.appendChild(this.closeButton);

      // Add close button container to image element
      this.imageButton.appendChild(this.closeButtonContainer);

      return el_;

    }catch(ex){
      // console.log('EVEREST - OVERLAYS:  handleImageButtonClick error', ex);
      return null;
    }

  }

  handleImageButtonClick(event){

    try{
      // If we have a target URL open target and remove image overlay
      if(this.targetUrl) {
        // Open Window to target
        window.open(this.targetUrl,'_blank');

        // Remove image overlay
        this.vjs_player.removeChild(this);

        // Update metrics
        this.updateViewOverlayTargetMetrics();
      }
      else // No Target, just remove image overlay
      {
        this.vjs_player.removeChild(this);
      }
    }catch(ex){
      console.log('EVEREST - OVERLAYS:  handleImageButtonClick error', ex);
    }
  }

  handleCloseButtonClick(event){

    try{
      // Close image overlay
      // console.log('EVEREST - OVERLAYS:  Close Button Clicked');
      this.vjs_player.removeChild(this);
      event.stopImmediatePropagation();
    }catch(ex){
      // console.log('EVEREST - OVERLAYS:  handleCloseButtonClick error', ex);
    }

  }

  updateViewOverlayTargetMetrics(){

    // console.log('EVEREST - OVERLAYS:  updateViewOverlayTargetMetrics',this);

    try{

      // Assuming we have an overlay object, update metrics
      if(this.overlay) {

        if(!this.vjs_player.myDataRef)
        {
          this.vjs_player.myDataRef = new Firebase('https://torrid-inferno-1419.firebaseio.com/channels/' + this.overlay.channelName.toLowerCase());
        }

        //console.log('EVEREST - OVERLAYS:  updateViewOverlayTargetMetrics');

        var filename = this.overlay.imageUrl.substring(this.overlay.imageUrl.lastIndexOf('/') + 1).toLowerCase().replace('.png', '').replace('.jpg', '').replace('.gif', '');

        this.vjs_player.myDataRef.child(this.overlay.clickMetricsKey).child(filename).push({
          url: this.overlay.imageUrl,
          alertText: this.overlay.alertText,
          targetUrl: this.overlay.targetUrl,
          uuid: this.overlay.uuid,
          time_stamp: Firebase.ServerValue.TIMESTAMP
        });

      }
    }catch(ex){
      //console.log('EVEREST - OVERLAYS:  Firebase razer view error', ex);
    }
  }


  initImage(overlay){

    try{
      // console.log('EVEREST - OVERLAYS:  initImage',overlay, overlay.imageUrl, overlay.targetUrl, this.imageButton);

      // Capture the image and target URLs
      this.overlay = overlay;
      this.targetUrl = overlay.targetUrl;
      this.imageUrl = overlay.imageUrl;

      // Set the image URL
      this.imageButton.style.backgroundImage = "url('" + this.imageUrl + "') ";

      if(overlay.scaleBy){
        // console.log('EVEREST - OVERLAYS:  Overlay Scale By ' + overlay.scaleBy );

        if(overlay.scaleBy === 'Height'){
          this.imageButton.style.backgroundSize = "auto " + overlay.overlay_height  + "%";
        }else if(overlay.scaleBy === 'Width')
        {
          this.imageButton.style.backgroundSize = overlay.overlay_width  + "% auto";
        }
        else{
          this.imageButton.style.backgroundSize = "75% auto;";
        }

      }

      // If target url update close button, if not remove.
      if(!this.targetUrl){
        // console.log('EVEREST - OVERLAYS:  No Target URL, remove close button');
        this.imageButton.removeChild(this.closeButtonContainer);
      }
      else{
        // console.log('EVEREST - OVERLAYS:  Target URL, update close button');
        this.imageButton.title = this.targetUrl;
        this.closeButton.innerHTML = "Click Here to Close";
      }
    }catch(ex){
      // console.log('EVEREST - OVERLAYS:  Init Image Error', ex);
    }

  }

}

videojs.registerComponent('AzubuImageOverlay', AzubuImageOverlay);
