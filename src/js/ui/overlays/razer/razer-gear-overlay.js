import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';
import * as API from '../../../communication/api';

import AzubuImageOverlay from '../overlay-components';

let Component = videojs.getComponent('Component');

export default class AzubuRazerGearOverlay extends Component {
  constructor(player,options) {

    try{

      // Parent Constructor
      super(player,options);

      // grab the player for easy access
      this.vjs_player = player;

      // Create a unique id for this user session
      this.uuid = this.guid();

      this.messageVisible = false;

      // Set up event handlers

      this.on(player, EVENTS.BROADCASTER_DATA_CHANGE, this.handleBroadcasterDataChange);
      this.on(this.razerGearOverlayButton, 'click', this.handleRazerGearOverlayButtonClick);
      this.on(this.razerGearOverlayMessage, 'click', this.handleRazerGearOverlayMessageClick);

      console.log('EVEREST - OVERLAYS (RazerGear):  constructor');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay error', ex);
    }
  }

  handleBroadcasterDataChange(event, data) {

    try{

      // clear any previously connected channel.
      this.clearFBConnection();

      // capture the channel name.
      this.channelName = data.data.user.username;

      this.razerGearOverlayButton.channelName = this.channelName;

      console.log('EVEREST - OVERLAYS (RazerGear):  handleBroadcasterDataChange', this.channelName);

      let overlay_channels = JSON.parse(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.OVERLAY_CHANNELS));

      for(var i =0; i < overlay_channels.length; i++){
        if (overlay_channels[i].name === 'Razer Gear') {
          if(overlay_channels[i].channels.indexOf('*') >=0 || overlay_channels[i].channels.indexOf(this.channelName) >= 0) {

            console.log('EVEREST - OVERLAYS (RazerGear):  Razer Gear Overlay: Channel White listed, Init Channel');

            // Capture this as the overlay object, for callbacks.
            var overlay = this;

            // Check to see if they have any gear.
            API.getBroadcasterInfoByChannelName(this.channelName,function(response, status){

              console.log('EVEREST - OVERLAYS (RazerGear):  getBroadcasterInfoByChannelName response',response,status);

              try{

                if(response && response.success && status === 200) {

                  overlay.broadcaster_info = response.user;

                  overlay.bricks = overlay.broadcaster_info.bricks;

                  overlay.gear = [];

                  for(var i = 0; i < overlay.bricks.length; i++){
                    if(overlay.bricks[i].type === "gear"){
                      for(var j = 0; j < overlay.bricks[i].items.length; j++) {

                        if(overlay.bricks[i].items[j].name.toLowerCase().indexOf("razer") != -1){

                          overlay.gear.push(overlay.bricks[i].items[j]);

                          //console.log('Razer Gear Found', overlay.bricks[i].items[j].name);
                        }
                      }
                    }
                  }

                  if(overlay.gear.length){

                    // Gear Found. Init Channel
                    console.log('EVEREST - OVERLAYS (Razer Gear):  ' + overlay.channelName + ' Channel White listed and has Gear, Init Channel');
                    overlay.initChannel();
                  }
                  else{
                    console.log('EVEREST - OVERLAYS (RazerGear):  No Gear Found. No point loading Gear Overlay');
                  }
                }else{
                  console.log('EVEREST - OVERLAYS (RazerGear):  No Gear Response Received. No point loading Gear Overlay');
                }
              }catch(ex){
                console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay handleLoadedMetadata error', ex);
              }
            });
            break;
          }
          else{
            console.log('EVEREST - OVERLAYS (RazerGear):  Channel Not White Listed For Razer Gear Overlay');
          }
        }
      }

    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuAzubuOverlay handleLoadedMetadata error', ex);
    }
  }

  createEl(){

    try{
      // Create the container element. Initially Hide the element and slide it in.
      this.razerGearOverlayContainer = super.createEl('div', {
        className: this.buildCSSClass() + '  vjs-overlays-razergear-button-panel-container  vjs-overlays-razergear-button-panel-offscreen vjs-overlays-hide-container'
      });

      // Create the button element
      this.razerGearOverlayButton = super.createEl('button', {className: 'vjs-overlays-icon-button vjs-overlays-icon-button-hidden vjs-azubu-razer-gear-overlay-button'});

      this.razerGearOverlayContainer.appendChild(this.razerGearOverlayButton);

      // Create the message element
      this.razerGearOverlayMessage = super.createEl('button', {className: 'vjs-azubu-razer-gear-overlay-message'});

      this.razerGearOverlayContainer.appendChild(this.razerGearOverlayMessage);

      return this.razerGearOverlayContainer;
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay createEl error', ex);
      return null;
    }
  }

  hideContainer(){
    try{
      console.log('EVEREST - OVERLAYS (RazerGear):  Hide Azubu Overlay Container');
      this.razerGearOverlayContainer.classList.add('vjs-overlays-hide-container');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuAzubuOverlay hideContainer error', ex);
    }
  }

  animateContainer()
  {
    try{
      // Remove Hide and Animate Container
      this.razerGearOverlayContainer.classList.remove('vjs-overlays-hide-container');
      this.razerGearOverlayContainer.classList.remove('vjs-overlays-razergear-button-panel-container-toggle');
      this.razerGearOverlayContainer.style.offsetWidth = this.razerGearOverlayContainer.offsetWidth;
      this.razerGearOverlayContainer.classList.add('vjs-overlays-razergear-button-panel-container-toggle');

      // Remove Button Hide that prevents events
      this.razerGearOverlayButton.classList.remove('vjs-overlays-icon-button-hidden');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  animateContainer error', ex);
    }
  }

  toggleContainerVisible() {
    try{
      this.razerGearOverlayContainer.classList.toggle('vjs-overlays-razergear-button-panel-offscreen');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuAzubuOverlay error', ex);
    }
  }

  guid() {
    try{

      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();

    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay guid error', ex);
      return null;
    }
  }

  clearFBConnection(){
    try{
      // Check to see if we had a previously connected channel.
      if(this.vjs_player.myDataRef){
        console.log('EVEREST - OVERLAYS (RazerGear):  Previous Channel... Disconnecting.')
        this.vjs_player.myDataRef.child('razer-gear-message').off("value",this.fbHandler);
      }

    }catch(ex){
       console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay clearFBConnection error', ex);
    }
  }

  initChannel(){

    try{
      let overlay = this;

      console.log('EVEREST - OVERLAYS (RazerGear):  Connecting Channel to FB API',this.channelName);

      if(!this.vjs_player.myDataRef)
      {
        this.vjs_player.myDataRef = new Firebase('https://torrid-inferno-1419.firebaseio.com/channels/' + overlay.channelName.toLowerCase());
      }

      // Get the data on a post that has changed
      this.fbHandler = this.vjs_player.myDataRef.child('razergear-message').on("value",function(snapshot){

        var message = snapshot.val();

        console.log('EVEREST - OVERLAYS (RazerGear):  razergear-message', message);

        if(message)
        {

          // Capture the overlay message content
          overlay.razer_gear_overlay_message = message;

          overlay.razerGearOverlayButton.style.backgroundImage = "url('" + overlay.razer_gear_overlay_message.imageUrl + "') ";

          let gear = overlay.gear;

          var gearHtml = '<h4 style="margin-bottom:10px; margin-right:5px;">' + overlay.razer_gear_overlay_message.alertText + '</h4>';
          gearHtml += '<div class="vjs-razorgear-content" ><div class="vjs-razorgear-gear-content ng-scope"><table><tbody>';


          for(var i = 0; i < gear.length; i ++){
            var url = (typeof gear[i].url != 'undefined') ? gear[i].url : 'http://www.razerzone.com';
            var image = (typeof gear[i].image != 'undefined') ? gear[i].image : 'http://assets.razerzone.com/images/common/razer-homepage-logo.png';
            var name = (typeof gear[i].name != 'undefined') ? gear[i].name : 'Razer Product';
            var category = (typeof gear[i].category != 'undefined') ? gear[i].category : '';

            gearHtml += '<tr class="vjs-razorgear-gear-item ng-scope"><td class="gear-content-left">';
            gearHtml += '<a href="' + url + '" target="_blank">';
            gearHtml += '<img src="' + image + '" class="vjs-razorgear-img-border" >';
            gearHtml += '</a></td><td class="gear-content-right"><span class="vjs-razorgear-title gear-name vjs-razorgear-title5 light">';
            gearHtml += '<a href="' + url + '" target="_blank" class="ng-binding">';
            gearHtml += name + ' ...';

            gearHtml += '</a></span><p class="vjs-razorgear-title gear-category vjs-razorgear-title7 muted-gray ng-binding">' + category  + '</p></td></tr>';
          }


          gearHtml += '</tbody></table></div></div><div class="clearfix"></div>'

          // Add Message to Div
          overlay.razerGearOverlayMessage.innerHTML = gearHtml;

          // Show the container
          overlay.animateContainer();

        }
        else
        {
          overlay.hideContainer();

          overlay.razerGearOverlayMessage.innerHTML = '';

        }
      });

      console.log('EVEREST - OVERLAYS (RazerGear):  Firebase Handler',this.fbHandler);
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay initChannel error', ex);
    }
  }

  handleRazerGearOverlayButtonClick(event){

    try{
      this.toggleContainerVisible();
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay handleRazerGearOverlayButtonClick error', ex);
    }

  }

  updateViewMessageMetrics(){
    try{

      if(this.vjs_player.myDataRef && this.razer_gear_overlay_message) {

        console.log('EVEREST - OVERLAYS (RazerGear):  updateViewMessageMetrics');

        var filename = this.razer_gear_overlay_message.imageUrl.substring(this.razer_gear_overlay_message.imageUrl.lastIndexOf('/') + 1).toLowerCase().replace('.png', '').replace('.jpg', '').replace('.gif', '');

        this.vjs_player.myDataRef.child('razergear-message-views').child(filename).push({
          url: this.razer_gear_overlay_message.imageUrl,
          alertText: this.razer_gear_overlay_message.alertText,
          targetUrl: this.razer_gear_overlay_message.targetUrl,
          uuid: this.uuid,
          time_stamp: Firebase.ServerValue.TIMESTAMP
        });
      }

    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay updateViewMessageMetrics error', ex);
    }
  }

  handleRazerGearOverlayMessageClick(event){

    try{
      this.updateClickOverlayMetrics(event.target.href);

    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  AzubuRazerGearOverlay handleRazerGearOverlayMessageClick error', ex);
    }

  }

  updateClickOverlayMetrics(clickTarget){

    try{

      // Assuming we have an overlay object, update metrics
      if(this.razer_gear_overlay_message) {

        console.log('EVEREST - OVERLAYS (RazerGear):  updateClickOverlayMetrics');

        var filename = this.razer_gear_overlay_message.imageUrl.substring(this.razer_gear_overlay_message.imageUrl.lastIndexOf('/') + 1).toLowerCase().replace('.png', '').replace('.jpg', '').replace('.gif', '');

        let metric = {
          url: this.razer_gear_overlay_message.imageUrl,
          alertText: this.razer_gear_overlay_message.alertText,
          targetUrl: this.razer_gear_overlay_message.targetUrl,
          clickTargetUrl: clickTarget,
          uuid: this.uuid,
          time_stamp: Firebase.ServerValue.TIMESTAMP
        };

        this.vjs_player.myDataRef.child('razergear-message-clicks').child(filename).push(metric);

      }
    }catch(ex){
      console.log('EVEREST - OVERLAYS (RazerGear):  Firebase razer view error', ex);
    }
  }
}

videojs.registerComponent('AzubuRazerGearOverlay', AzubuRazerGearOverlay);


