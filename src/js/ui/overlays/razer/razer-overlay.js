import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';

import AzubuImageOverlay from '../overlay-components';

let Component = videojs.getComponent('Component');

export default class AzubuRazerOverlay extends Component {
  constructor(player,options) {

    try{

      // Parent Constructor
      super(player,options);

      // grab the player for easy access
      this.vjs_player = player;

      // Create a unique id for this user session
      this.uuid = this.guid();

      this.messageVisible = false;

      // Set up event handlers
      this.on(player, EVENTS.BROADCASTER_DATA_CHANGE, this.handleBroadcasterDataChange);
      this.on(this.razerOverlayButton, 'click', this.handleRazerOverlayButtonClick);
      this.on(this.razerOverlayMessage, 'click', this.handleRazerOverlayMessageClick);

      console.log('EVEREST - OVERLAYS (Razer):  constructor');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay error', ex);
    }
  }

  handleBroadcasterDataChange(event, data) {

    try{

      // clear any previously connected channel.
      this.clearFBConnection();

      // capture the channel name.
      this.channelName = data.data.user.username;

      this.razerOverlayButton.channelName = this.channelName;

      console.log('EVEREST - OVERLAYS (Razer):  handleBroadcasterDataChange', this.channelName);

      let overlay_channels = JSON.parse(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.OVERLAY_CHANNELS));

      let whitelisted = false;

      for(var i =0; i < overlay_channels.length; i++){
        if (overlay_channels[i].name === 'Razer') {
          if(overlay_channels[i].channels.indexOf('*') >=0 || overlay_channels[i].channels.indexOf(this.channelName) >= 0) {
            console.log('EVEREST - OVERLAYS (Razer):  ' + this.channelName + ' Channel White listed, Init Channel');
            this.initChannel();
            whitelisted = true;
            break;
          }
        }
      }

      if(!whitelisted){
        console.log('EVEREST - OVERLAYS (Razer):  ' + this.channelName + ' Channel NOT White listed.');
      }


    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuAzubuOverlay handleLoadedMetadata error', ex);
    }
  }

  createEl(){

    try{
      // Create the container element. Initially Hide the element and slide it in.
      this.razerOverlayContainer = super.createEl('div', {
        className: this.buildCSSClass() + '  vjs-overlays-button-panel-container vjs-overlays-button-panel-offscreen vjs-overlays-hide-container'
      });

      // Create the button element
      this.razerOverlayButton = super.createEl('button', {className: 'vjs-overlays-icon-button vjs-overlays-icon-button-hidden vjs-azubu-razer-overlay-button'});

      this.razerOverlayContainer.appendChild(this.razerOverlayButton);

      // Create the message element
      this.razerOverlayMessage = super.createEl('button', {className: 'vjs-azubu-razer-overlay-message'});

      this.razerOverlayContainer.appendChild(this.razerOverlayMessage);

      return this.razerOverlayContainer;
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay createEl error', ex);
      return null;
    }
  }

  hideContainer(){
    try{
      console.log('EVEREST - OVERLAYS (Razer):  Hide Azubu Overlay Container');
      this.razerOverlayContainer.classList.add('vjs-overlays-hide-container');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuAzubuOverlay hideContainer error', ex);
    }
  }

  animateContainer()
  {
    try{
      // Remove Hide and Animate Container
      this.razerOverlayContainer.classList.remove('vjs-overlays-hide-container');
      this.razerOverlayContainer.classList.remove('vjs-overlays-button-panel-container-toggle');
      this.razerOverlayContainer.style.offsetWidth = this.razerOverlayContainer.offsetWidth;
      this.razerOverlayContainer.classList.add('vjs-overlays-button-panel-container-toggle');

      // Remove Button Hide that prevents events
      this.razerOverlayButton.classList.remove('vjs-overlays-icon-button-hidden');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  animateContainer error', ex);
    }
  }

  toggleContainerVisible() {
    try{
      this.razerOverlayContainer.classList.toggle('vjs-overlays-button-panel-offscreen');
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  toggleContainerVisible error', ex);
    }
  }

  guid() {
    try{

      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();

    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay guid error', ex);
      return null;
    }
  }

  clearFBConnection(){
    try{
      // Check to see if we had a previously connected channel.
      if(this.vjs_player.myDataRef){
        console.log('EVEREST - OVERLAYS (Razer):  Previous Channel... Disconnecting.')
        this.vjs_player.myDataRef.child('razer-message').off("value",this.fbHandler);

      }

    }catch(ex){
       console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay clearFBConnection error', ex);
    }
  }

  initChannel(){

    try{
      let overlay = this;

      console.log('EVEREST - OVERLAYS (Razer):  Connecting Channel to FB API',this.channelName);

      if(!this.vjs_player.myDataRef)
      {
        this.vjs_player.myDataRef = new Firebase('https://torrid-inferno-1419.firebaseio.com/channels/' + overlay.channelName.toLowerCase());
      }

      // Get the data on a post that has changed
      this.fbHandler = this.vjs_player.myDataRef.child('razer-message').on("value",function(snapshot){

        var message = snapshot.val();

        console.log('EVEREST - OVERLAYS (Razer):  razer-message', message);

        if(message)
        {

          // Capture the overlay message content
          overlay.razer_overlay_message = message;

          // Add Message to Div
          overlay.razerOverlayMessage.innerHTML = overlay.razer_overlay_message.alertText;

          // Show the container
          overlay.animateContainer();

        }
        else
        {
          overlay.hideContainer();

          overlay.razerOverlayMessage.innerHTML = '';

        }
      });

      console.log('EVEREST - OVERLAYS (Razer):  Firebase Handler',this.fbHandler);
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay initChannel error', ex);
    }
  }

  handleRazerOverlayButtonClick(event){

    try{
      console.log('EVEREST - OVERLAYS (Razer):  handleRazerOverlayButtonClick');

      this.toggleContainerVisible();

    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay handleRazerOverlayButtonClick error', ex);
    }

  }

  updateViewMessageMetrics(){
    try{

      if(this.vjs_player.myDataRef && this.razer_overlay_message) {

        console.log('EVEREST - OVERLAYS (Razer):  updateViewMessageMetrics');

        var filename = this.razer_overlay_message.imageUrl.substring(this.razer_overlay_message.imageUrl.lastIndexOf('/') + 1).toLowerCase().replace('.png', '').replace('.jpg', '').replace('.gif', '');

        this.vjs_player.myDataRef.child('razer-message-views').child(filename).push({
          url: this.razer_overlay_message.imageUrl,
          alertText: this.razer_overlay_message.alertText,
          targetUrl: this.razer_overlay_message.targetUrl,
          uuid: this.uuid,
          time_stamp: Firebase.ServerValue.TIMESTAMP
        });
      }

    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay updateViewMessageMetrics error', ex);
    }
  }

  handleRazerOverlayMessageClick(event){

    try{
      // Create an image overlay object
      this.imageOverlay = new AzubuImageOverlay(this.vjs_player, this.options);

      // Add image overlay to player
      this.vjs_player.addChild(this.imageOverlay);

      // Update overlay with additional info
      this.razer_overlay_message.channelName = this.channelName;

      this.razer_overlay_message.clickMetricsKey = 'razer-message-clicks';

      this.razer_overlay_message.uuid = this.uuid;

      // Init the image
      this.imageOverlay.initImage(this.razer_overlay_message);

      // Update the view metrics
      this.updateViewMessageMetrics();
    }catch(ex){
      console.log('EVEREST - OVERLAYS (Razer):  AzubuRazerOverlay handleRazerOverlayMessageClick error', ex);
    }

  }
}

videojs.registerComponent('AzubuRazerOverlay', AzubuRazerOverlay);


