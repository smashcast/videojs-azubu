import videojs from 'video.js';

let Component = videojs.getComponent('Component');

export default class AzubuPlayerOverlayHotspot extends Component {
  constructor(player,options) {

    try{

      // Base constructor
      super(player,options);

      // Capture the videojs player
      this.vjs_player = player;

      // Add Click Handlers
      this.on(this.hotspotButton, 'click', this.handleHotSpotButtonClick);

       console.log('EVEREST - OVERLAYS (HotSpot):  AzubuPlayerOverlayHotspot: constructor');
    }catch(ex){
        console.log('EVEREST - OVERLAYS (HotSpot):  AzubuPlayerOverlayHotspot Constructor error', ex);
      return null;
    }
  }

  createEl(){
    try{

      // Create the container DOM Element
      this.hotspotButton = super.createEl('div', {
        className: this.buildCSSClass() + 'vjs-azubu-player-overlay-hotspot-button'
      });

      return this.hotspotButton;

    }catch(ex){
        console.log('EVEREST - OVERLAYS (HotSpot):  handleImageButtonClick error', ex);
      return null;
    }

  }

  handleHotSpotButtonClick(event){

    try{
      // If we have a target URL open target and remove image hotspot
      if(this.hotspot.images[this.hotspotImageIndex].href) {
        // Open Window to target
        window.open(this.hotspot.images[this.hotspotImageIndex].href,'_blank');

        // Update metrics
        this.updateViewOverlayTargetMetrics();
      }

    }catch(ex){
      console.log('EVEREST - OVERLAYS (HotSpot):  handleImageButtonClick error', ex);
    }
  }

  updateViewOverlayTargetMetrics(){

      console.log('EVEREST - OVERLAYS (HotSpot):  updateViewOverlayTargetMetrics',this);

    try{

      // Assuming we have an hotspot object, update metrics
      if(this.hotspot) {

        if(!this.vjs_player.myDataRef)
        {
          this.vjs_player.myDataRef = new Firebase('https://torrid-inferno-1419.firebaseio.com/channels/' + this.channelName.toLowerCase());
        }

        this.vjs_player.myDataRef.child('azubu-player-overlays-clicks').child(this.overlayId).push({
          targetUrl: this.hotspot.images[this.hotspotImageIndex].href,
          hotspot: this.hotspot,
          uuid: this.uuid,
          time_stamp: Firebase.ServerValue.TIMESTAMP
        });

      }
    }catch(ex){
      console.log('EVEREST - OVERLAYS (HotSpot):  Firebase razer view error', ex);
    }
  }

  initHotSpot(hotspot,container){

    try{
       console.log('EVEREST - OVERLAYS (HotSpot):  initHotSpot',hotspot,container,this.hotspotButton);

      this.hotspot = hotspot;
      this.container = container;
      this.hotspotImageIndex = 0;

      this.top = (( hotspot.top / this.container.height) * 100).toFixed(0) + '%';
      this.left =  (( hotspot.left / this.container.width) * 100).toFixed(0) + '%';
      this.width = (( hotspot.width / this.container.width) * 100).toFixed(0) + '%';
      this.height = (( hotspot.height / this.container.height) * 100).toFixed(0) + '%';

      // Set the image button styles

      this.hotspotButton.style.top = this.top;
      this.hotspotButton.style.left = this.left;
      this.hotspotButton.style.width = this.width;
      this.hotspotButton.style.height = this.height;

      // Single Image, don't bother with rotation.
      if(this.hotspot.images.length === 1){
        // Capture the image and target URLs
        this.hotspotButton.title = this.hotspot.images[this.hotspotImageIndex].href;
        this.hotspotButton.style.backgroundImage = "url('" + this.hotspot.images[this.hotspotImageIndex].url + "') ";
      }
      else // Multiple Images, set up rotator
      {
        console.log('Multiple Images, Set Rotation',this.hotspotImageIndex,this.hotspot.images.length  );
        // Set the first Image in rotation

        if(this.hotspot.images[this.hotspotImageIndex].href) {
          this.hotspotButton.title = this.hotspot.images[this.hotspotImageIndex].href;
          this.hotspotButton.style.cursor = 'pointer';
        }
        else{
          this.hotspotButton.title = '';
          this.hotspotButton.style.cursor = 'auto';
        }
        this.hotspotButton.style.backgroundImage = "url('" + this.hotspot.images[this.hotspotImageIndex].url + "') ";

        // Start a timer to rotate images
        this.setInterval(function(){
          // do your thing

          this.hotspotImageIndex ++;

          console.log('Image Rotation',this.hotspotImageIndex,this.hotspot.images.length  );

          if(this.hotspotImageIndex >= this.hotspot.images.length ){
            this.hotspotImageIndex = 0;
          }

          if(this.hotspot.images[this.hotspotImageIndex].href) {
            this.hotspotButton.title = this.hotspot.images[this.hotspotImageIndex].href;
            this.hotspotButton.style.cursor = 'pointer';
          }
          else{
            this.hotspotButton.title = '';
            this.hotspotButton.style.cursor = 'auto';
          }
          this.hotspotButton.style.backgroundImage = "url('" + this.hotspot.images[this.hotspotImageIndex].url + "') ";

        }, this.hotspot.image_rotation * 1000);
      }




    }catch(ex){
        console.log('EVEREST - OVERLAYS (HotSpot):  Init Image Error', ex);
    }

  }

}

videojs.registerComponent('AzubuPlayerOverlayHotspot', AzubuPlayerOverlayHotspot);
