import * as UTIL from '../../util/util';
import * as API from '../../communication/api';

import AzubuOverlay from './azubu/azubu-overlay';
import AzubuPlayerOverlays from './azubu/azubu-playeroverlays';
import AzubuRazerOverlay from './razer/razer-overlay';
import AzubuRazerGearOverlay from './razer/razer-gear-overlay';


export const init = (player, options) => {
  //console.log('VJS - OVERLAYS:  INIT OVERLAYS');
  try{

    // Load the Overlay White List
    API.getOverlayChannels(getOverlayChannelsCallback, player, options);




  }catch(ex){
    console.log('VJS - OVERLAYS:  Init Error', ex);
  }
};

const getOverlayChannelsCallback = (overlays, status, player, options) => {
  try{

    //console.log('VJS - OVERLAYS:  OVERLAY CHANNELS CALLBACK');

    // Save white list in local storage
    UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.OVERLAY_CHANNELS, JSON.stringify(overlays));

    // Add Overlays
    player.addChild(new AzubuOverlay(player, options));
    player.addChild(new AzubuPlayerOverlays(player, options));
    player.addChild(new AzubuRazerOverlay(player, options));
    player.addChild(new AzubuRazerGearOverlay(player, options));


  }catch(ex){
    console.log('VJS - OVERLAYS:  getOverlayChannelsCallback Error', ex);
  }
};
