import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';

import AzubuImageOverlay from '../overlay-components';

let Component = videojs.getComponent('Component');

export default class AzubuOverlay extends Component {
  constructor(player,options) {

    try{

      // Parent Constructor
      super(player,options);

      // grab the player for easy access
      this.vjs_player = player;

      // Create a unique id for this user session
      this.uuid = this.guid();

      this.messageVisible = false;

      // Set up event handlers
      this.on(player, EVENTS.BROADCASTER_DATA_CHANGE, this.handleBroadcasterDataChange);
      this.on(this.azubuOverlayButton, 'click', this.handleAzubuOverlayButtonClick);
      this.on(this.azubuOverlayMessage, 'click', this.handleAzubuOverlayMessageClick);

       console.log('EVEREST - OVERLAYS (AzubuImage):  constructor');
    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay error', ex);
    }
  }

  handleBroadcasterDataChange(event, data) {

    try{

      // clear any previously connected channel.
      this.clearFBConnection();

      // capture the channel name.
      this.channelName = data.data.user.username;

      this.azubuOverlayButton.channelName = this.channelName;

      console.log('EVEREST - OVERLAYS (AzubuImage):  handleBroadcasterDataChange', this.channelName);

      let overlay_channels = JSON.parse(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.OVERLAY_CHANNELS));

      for(var i =0; i < overlay_channels.length; i++){
        if (overlay_channels[i].name === 'Azubu') {
          if(overlay_channels[i].channels.indexOf('*') >=0 || overlay_channels[i].channels.indexOf(this.channelName) >= 0) {
            console.log('EVEREST - OVERLAYS (Azubu Image):  ' + this.channelName + ' Channel White listed, Init Channel');
            this.initChannel();

            break;
          }
        }
      }

    }catch(ex){
      console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay handleLoadedMetadata error', ex);
    }
  }

  createEl(){

    try{

      // Create the container element. Initially Hide the element off screen
      this.azubuOverlayContainer = super.createEl('div', {
        className: this.buildCSSClass() + ' vjs-overlays-button-panel-container vjs-overlays-button-panel-offscreen vjs-overlays-hide-container'
      });

      // Create the button element
      this.azubuOverlayButton = super.createEl('button', {className: 'vjs-overlays-icon-button vjs-overlays-icon-button-hidden vjs-azubu-overlay-button'});

      // Add Button To Container
      this.azubuOverlayContainer.appendChild(this.azubuOverlayButton);

      // Create the message element
      this.azubuOverlayMessage = super.createEl('button', {className: 'vjs-azubu-overlay-message'});

      // Add message element to Container
      this.azubuOverlayContainer.appendChild(this.azubuOverlayMessage);

      // Return the container
      return this.azubuOverlayContainer;


    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay createEl error', ex);
      return null;
    }
  }

  hideContainer(){
    try{
       console.log('EVEREST - OVERLAYS (AzubuImage):  Hide Azubu Overlay Container');
      this.azubuOverlayContainer.classList.add('vjs-overlays-hide-container');
    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay hideContainer error', ex);
    }
  }

  animateContainer()
  {
    try{
      // Remove Hide and Animate Container
      this.azubuOverlayContainer.classList.remove('vjs-overlays-hide-container');
      this.azubuOverlayContainer.classList.remove('vjs-overlays-button-panel-container-toggle');
      this.azubuOverlayContainer.style.offsetWidth = this.azubuOverlayContainer.offsetWidth;
      this.azubuOverlayContainer.classList.add('vjs-overlays-button-panel-container-toggle');

      // Remove Button Hide that prevents events
      this.azubuOverlayButton.classList.remove('vjs-overlays-icon-button-hidden');
    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  animateContainer error', ex);
    }
  }

  toggleContainerVisible() {
    try{
      this.azubuOverlayContainer.classList.toggle('vjs-overlays-button-panel-offscreen');
    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  toggleContainerVisible error', ex);
    }
  }

  guid() {
    try{

      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();

    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay guid error', ex);
      return null;
    }
  }

  clearFBConnection(){
    try{

      //console.log('EVEREST - OVERLAYS (AzubuImage):  Previous Channel... Disconnecting.');
      // Check to see if we had a previously connected channel.
      if(this.vjs_player.myDataRef){

        this.vjs_player.myDataRef.child('azubu-overlay-message').off("value",this.fbHandler);

      }


    }catch(ex){
      console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay clearFBConnection error', ex);
    }
  }

  initChannel(){

    try{
      let overlay = this;

       console.log('EVEREST - OVERLAYS (AzubuImage):  Connecting Channel to FB API',this.channelName);

      // Connect to Firebase.io
      if(!this.vjs_player.myDataRef)
      {
        this.vjs_player.myDataRef = new Firebase('https://torrid-inferno-1419.firebaseio.com/channels/' + overlay.channelName.toLowerCase());
      }

      // Get the data on a post that has changed
      overlay.fbHandler = this.vjs_player.myDataRef.child('azubu-overlay-message').on("value",function(snapshot){

        var message = snapshot.val();

         console.log('EVEREST - OVERLAYS (AzubuImage):  azubu-overlay-message', message);

        if(message)
        {

          // Capture the overlay message content
          overlay.azubu_overlay_message = message;

          overlay.azubuOverlayButton.style.backgroundImage = "url('" + overlay.azubu_overlay_message.icon + "') ";

          // Add Message to Div
          overlay.azubuOverlayMessage.innerHTML = overlay.azubu_overlay_message.alertText;

          overlay.azubuOverlayMessage.style.color = overlay.azubu_overlay_message.text_color;

          // Show the container
          overlay.animateContainer();

        }
        else
        {
          overlay.hideContainer();

          overlay.azubuOverlayMessage.innerHTML = '';

        }
      });

    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay initChannel error', ex);
    }
  }

  handleAzubuOverlayButtonClick(event){

    try{

       console.log('EVEREST - OVERLAYS (AzubuImage):  handleAzubuOverlayButtonClick');

      this.toggleContainerVisible();

    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay handleAzubuOverlayButtonClick error', ex);
    }

  }

  updateViewMessageMetrics(){
    try{

      if(this.vjs_player.myDataRef && this.azubu_overlay_message) {

         console.log('EVEREST - OVERLAYS (AzubuImage):  updateViewMessageMetrics');

        var filename = this.azubu_overlay_message.imageUrl.substring(this.azubu_overlay_message.imageUrl.lastIndexOf('/') + 1).toLowerCase().replace('.png', '').replace('.jpg', '').replace('.gif', '');

        this.vjs_player.myDataRef.child('azubu-overlay-message-views').child(filename).push({
          url: this.azubu_overlay_message.imageUrl,
          alertText: this.azubu_overlay_message.alertText,
          targetUrl: this.azubu_overlay_message.targetUrl,
          uuid: this.uuid,
          time_stamp: Firebase.ServerValue.TIMESTAMP
        });
      }

    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay updateViewMessageMetrics error', ex);
    }
  }

  handleAzubuOverlayMessageClick(event){

    try{
      // Create an image overlay object
      this.imageOverlay = new AzubuImageOverlay(this.vjs_player, this.options);

      // Add image overlay to player
      this.vjs_player.addChild(this.imageOverlay);

      // Update overlay with additional info
      this.azubu_overlay_message.channelName = this.channelName;

      this.azubu_overlay_message.clickMetricsKey = 'azubu-overlay-message-clicks';

      this.azubu_overlay_message.uuid = this.uuid;

      // Init the image
      this.imageOverlay.initImage(this.azubu_overlay_message);

      // Update the view metrics
      this.updateViewMessageMetrics();
    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuImage):  AzubuOverlay handleAzubuOverlayMessageClick error', ex);
    }

  }
}

videojs.registerComponent('AzubuOverlay', AzubuOverlay);
