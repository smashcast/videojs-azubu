import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';

import AzubuPlayerOverlayHotspot from '../hotspot-components';

let Component = videojs.getComponent('Component');

export default class AzubuPlayerOverlays extends Component {
  constructor(player,options) {

    try{

      // Parent Constructor
      super(player,options);

      // grab the player for easy access
      this.vjs_player = player;

      // Create a unique id for this user session
      this.uuid = this.guid();

      // Set up event handlers
      this.on(player, EVENTS.BROADCASTER_DATA_CHANGE, this.handleBroadcasterDataChange);

      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):  : constructor');
    }catch(ex){
       console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):   error', ex);
    }
  }

  handleBroadcasterDataChange(event, data) {

    try{

      // clear any previously connected channel.
      this.clearFBConnection();

      // capture the channel name.
      this.channelName = data.data.user.username;

      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays): handleBroadcasterDataChange', this.channelName);

      let overlay_channels = JSON.parse(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.OVERLAY_CHANNELS));

      for(var i =0; i < overlay_channels.length; i++){
        if (overlay_channels[i].name === 'Player Overlays') {
          if(overlay_channels[i].channels.indexOf('*') >=0 || overlay_channels[i].channels.indexOf(this.channelName) >= 0) {
            console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):  ' + this.channelName + ' Channel White listed, Init Channel');
            this.initChannel();

            break;
          }
        }
      }


    }catch(ex){
      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays): handleLoadedMetadata error', ex);
    }
  }

  guid() {
    try{

      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();

    }catch(ex){
      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):   guid error', ex);
      return null;
    }
  }

  clearFBConnection(){
    try{

      //console.log('EVEREST - OVERLAYS:  Previous Channel... Disconnecting.');
      // Check to see if we had a previously connected channel.
      if(this.vjs_player.myDataRef){

        this.vjs_player.myDataRef.child('azubu-player-overlays').off("value",this.fbHandler);

      }

    }catch(ex){
      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):   clearFBConnection error', ex);
    }
  }

  initChannel(){

    try{
      let overlay = this;

      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):   Connecting Channel to FB API',overlay.channelName);

      if(!this.vjs_player.myDataRef)
      {
        this.vjs_player.myDataRef = new Firebase('https://torrid-inferno-1419.firebaseio.com/channels/' + overlay.channelName.toLowerCase());
      }

      // Get the data on a post that has changed
      this.fbHandler = this.vjs_player.myDataRef.child('azubu-player-overlays').on("value",function(snapshot){

        var message = snapshot.val();

        console.log('Got azubu-player-overlays Event',message);

        if(message)
        {

          overlay.overlays = JSON.parse(message);

          overlay.clearHotspots();

          for(var i = 0; i < overlay.overlays.length; i++) {

            if (overlay.overlays[i].enabled) {
              for (var j = 0; j < overlay.overlays[i].hotspots.length; j++) {

                if(overlay.overlays[i].hotspots[j].enabled) {
                  // Create an image overlay object
                  let hotspot = new AzubuPlayerOverlayHotspot(overlay.vjs_player, overlay.options);
                  hotspot.uuid = overlay.uuid;
                  hotspot.channelName = overlay.channelName;
                  hotspot.overlayId = overlay.overlays[i]._id;

                  overlay.overlayHotspots.push(hotspot);

                  // Add image overlay to player
                  overlay.vjs_player.addChild(hotspot);

                  hotspot.initHotSpot(overlay.overlays[i].hotspots[j], overlay.overlays[i].container);
                }
              }
            }
          }
        }
        else
        {
          overlay.clearHotspots();
        }
      });

      //console.log('EVEREST - OVERLAYS:  Firebase Handler',this.fbHandler);
    }catch(ex){
      console.log('EVEREST - OVERLAYS (AzubuPlayerOverlays):   initChannel error', ex);
    }
  }

  clearHotspots()
  {
    if(this.overlayHotspots) {
      for (var i = 0; i < this.overlayHotspots.length; i++) {

        // Remove image overlay
        this.vjs_player.removeChild(this.overlayHotspots[i]);
      }
    }

    this.overlayHotspots = [];
  }
}

videojs.registerComponent('AzubuPlayerOverlays', AzubuPlayerOverlays);
