import videojs from 'video.js';
import * as EVENTS from '../../../util/event';

let Button = videojs.getComponent('Button');

const defaults = {
  className: 'vjs-big-play-button'
};

export default class AzubuBigPlayButton extends Button {
  constructor(player,options) {
    super(player, options);

    player.bigPlayButton.dispose();

    this.on(player, EVENTS.AD_START, this.hide);
    this.on(player, EVENTS.AD_END, this.show);
    this.on(player, EVENTS.SHARE_OPEN, this.hide);
    this.on(player, EVENTS.SHARE_CLOSE, this.show);

  }

  createEl(){
    return super.createEl('button', {
      className: this.buildCSSClass() + defaults.className
    });
  }
  handleClick() {
    this.player_.play();
  }
}

videojs.registerComponent('AzubuBigPlayButton', AzubuBigPlayButton);
