import * as ENV from '../../util/environment';
import * as EVENTS from '../../util/event';
import * as UTIL from '../../util/util';
import AzubuStatusTagContainer from './status-tags/status-tag';
import AzubuWatermark from './watermark/watermark';
import AzubuLoadingSpinner from './loading-spinner/loading-spinner';
import AzubuBigPlayButton from './big-play-button/big-play-button';

export const init = (player, options) => {
  player.addChild(new AzubuStatusTagContainer(player, options));
  player.addChild(new AzubuWatermark(player, options));
  player.addChild(new AzubuLoadingSpinner(player, options));
  player.addChild(new AzubuBigPlayButton(player, options));

  player.one(EVENTS.LOADED_METADATA, () => {

    if(player.duration() === Infinity) {
      player.off(player.tech_, 'mousedown', player.handleTechClick_);
    }

    player.on(player.tech_, 'mousedown', () => {
      if(!ENV.isAzubuChannelpage() && player.mediainfo && player.mediainfo.tags){
        window.open(UTIL.getChannelPageUrlByTags(player.mediainfo.tags), '_top');
      }
    });
  });
};
