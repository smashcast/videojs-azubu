import videojs from 'video.js';
import * as EVENTS from '../../../util/event';

let Component = videojs.getComponent('Component');

const defaults = {
  classNames: {
    defaults: {
      container: 'vjs-status-tags',
      tag: 'status-tag-video'
    },
    live: 'status-tag-live',
    vod: 'status-tag-video',
    reconnecting: 'status-tag-reconnecting',
    reconnected: 'status-tag-reconnected',
    offline: 'status-tag-offline',
    rebroadcast: 'status-tag-rebroadcast',
    disconnected: 'status-tag-disconnected'
  },
  labels: {
    live: 'live',
    vod: 'video',
    reconnecting: 'reconnecting',
    reconnected: 'reconnected',
    offline: 'offline',
    rebroadcast: 'rebroadcast',
    disconnected: 'disconnected',
    loading: 'loading...'
  }
};

export default class AzubuStatusTag extends Component {
  constructor(player, options) {
    super(player, options);
  }

  createEl(){
    return super.createEl('div', {
      className: this.buildCSSClass() + defaults.classNames.defaults.tag,
      innerHTML: defaults.labels.loading
    });
  }

  update(label){
    videojs.emptyEl(this.el());
    this.el().innerHTML = label;
  }
}

videojs.registerComponent('AzubuStatusTag', AzubuStatusTag);

export default class AzubuStatusTagContainer extends Component {
  constructor(player, options) {
    super(player,options);

    player.statusTag = new AzubuStatusTag(player, options);

    this.addChild(player.statusTag);

    player.on([EVENTS.LOADED_METADATA, EVENTS.AD_START], this.handleLoadedMetadata);
    player.on(EVENTS.RECONNECTING, this.handleReconnecting);
    player.on(EVENTS.RECONNECTED, this.handleReconnected);
  }

  createEl(){
    return super.createEl('div', {
      className: this.buildCSSClass() + defaults.classNames.defaults.container
    });
  }

  handleLoadedMetadata(){
    if(player.duration() === Infinity) {
      this.statusTag.update(defaults.labels.live);
      this.statusTag.addClass(defaults.classNames.live);
      this.statusTag.removeClass(defaults.classNames.vod);
    } else {
      this.statusTag.update(defaults.labels.vod);
      this.statusTag.addClass(defaults.classNames.vod);
      this.statusTag.removeClass(defaults.classNames.live);
    }
  }

  handleReconnecting(){
    this.statusTag.update(defaults.labels.reconnecting);
    this.statusTag.addClass(defaults.classNames.reconnecting);
    this.statusTag.removeClass(defaults.classNames.reconnected);
  }

  handleReconnected(){
    this.statusTag.update(defaults.labels.reconnected);
    this.statusTag.addClass(defaults.classNames.reconnected);
    this.statusTag.removeClass(defaults.classNames.reconnecting);
  }
}

videojs.registerComponent('AzubuStatusTagContainer', AzubuStatusTagContainer);
