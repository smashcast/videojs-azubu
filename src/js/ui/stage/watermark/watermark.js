import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as util from '../../../util/util';

let Component = videojs.getComponent('Component');

const defaults = {
  className: 'vjs-azubu-logo-watermark-container',
  channelName: 'Azubu',
  url: 'http://www.azubu.tv/'
};

export default class AzubuWatermark extends Component {
  constructor(player,options) {
    super(player,options);

    this.on(player, EVENTS.LOADED_METADATA, this.handleLoadedMetadata);
    this.on(player, EVENTS.BROADCASTER_DATA_CHANGE, this.handleBroadcasterDataChange);
    this.on(this.watermarkButton, 'click', this.handleChildButtonClick);
    this.on(player, EVENTS.AD_START, this.hide);
    this.on(player, EVENTS.AD_END, this.show);
  }

  createEl(){
    let el_ = super.createEl('div', {
      className: this.buildCSSClass() + defaults.className
    });

    this.channelName = defaults.channelName;
    this.watermarkButton = super.createEl('button', {className: 'vjs-azubu-logo-watermark'});
    this.watermarkButton.value = 'http://www.azubu.tv';

    el_.appendChild(this.watermarkButton);

    return el_;
  }

  handleBroadcasterDataChange(event, data) {
    if(data.data && data.data.url_channel){
      this.watermarkButton.value = data.data.url_channel;
    } else {
      this.watermarkButton.value = 'http://www.azubu.tv';
    }
  }

  handleChildButtonClick(event){
    window.open(event.target.value, '_top');
  }

  handleLoadedMetadata(){
    this.channelName = (player.mediainfo && player.mediainfo.tags) ? util.getChannelNameFromTags(player.mediainfo.tags) : '';
    this.watermarkButton.channelName = this.channelName;
  }

}

videojs.registerComponent('AzubuWatermark', AzubuWatermark);
