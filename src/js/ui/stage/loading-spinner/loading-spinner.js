import videojs from 'video.js';
import * as EVENTS from '../../../util/event';

let Component = videojs.getComponent('Component');

const defaults = {
  className: 'vjs-loading-spinner',
  message: 'Buffering...'
};

export default class AzubuLoadingSpinner extends Component {
  constructor(player,options) {
    super(player, options);

    player.loadingSpinner.dispose();

    this.on(player, EVENTS.AD_START, this.hide);
    this.on(player, EVENTS.AD_END, this.show);
    this.on(player, EVENTS.SHARE_OPEN, this.hide);
    this.on(player, EVENTS.SHARE_CLOSE, this.show);

  }

  createEl(){
    let el_ =  super.createEl('div', {
      className: this.buildCSSClass() + defaults.className,
      id: 'loading-spinner'
    });

    this.loadingSpinnerIcon = super.createEl('div', {className: 'vjs-loading-spinner-icon'});

    el_.appendChild(this.loadingSpinnerIcon);

    return el_;
  }

}

videojs.registerComponent('AzubuLoadingSpinner', AzubuLoadingSpinner);
