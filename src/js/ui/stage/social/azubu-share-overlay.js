import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';
import * as ENV from '../../../util/environment';


let Component = videojs.getComponent('Component');

const DEFAULTS = {
  CLASSNAME: 'vjs-share-overlay',
  URL: {
    WEB: 'www.azubu.tv',
    EMBED: 'embed.azubu.tv',
    UOL: 'azubu.uol.com.br',
    LOCAL: 'localhost'
  }
};

export default class AzubuShareOverlay extends Component {
  constructor(player, options) {
    super(player, options);

    this.setEmbedCodeUrl(DEFAULTS.URL.WEB);
    this.on(this.shareOverlayExit, 'click', this.hide);
    this.on(this.shareIconFacebook, 'click', this.handleFacebookClick);
    this.on(this.shareIconTwitter, 'click', this.handleTwitterClick);
    this.on(this.shareUrlCheckbox, 'change', this.handleShareCheckboxChange);
    this.on(this.shareTime, 'change', this.handleSetShareTimeUpdate);
    this.on(player, EVENTS.LOADED_METADATA, this.handleLoadedMetadata);
    this.on(player, EVENTS.SHARE_OPEN, this.show);
  }

  createEl() {

    let el_ = super.createEl('div', {
      className: this.buildCSSClass() + DEFAULTS.CLASSNAME
    });

    // Share Overlay Elements
    this.shareOverlayExit = super.createEl('Button', {className: 'vjs-share-overlay-exit', innerHTML: ''});
    this.shareOverlayElementsContainer = super.createEl('div', {className: 'vjs-share-elements-container', innerHTML: ''});
    this.shareLabel = super.createEl('div', {className: 'vjs-share-label', innerHTML: 'Share'});
    this.shareInputsContainer = super.createEl('div', {className: 'vjs-share-inputs-container'});
    this.shareUrlContainer = super.createEl('div', {className: 'vjs-share-url-container', innerHTML: ''});

    // Share URL input and background
    this.shareUrl = super.createEl('input', {className: 'vjs-share-url', id: 'share-url', readOnly: 'readonly'}, {type: 'text'});
    this.shareUrl.setAttribute('onclick', 'this.focus();this.select()');
    this.shareUrlBack = super.createEl('span', {className: 'vjs-share-url-back', id: 'share-url-back', readOnly: 'readonly'});

    // Option to start video share at a specific point in the VOD.
    // shareTimeContainer is for VOD share & should be hidden on channel share
    this.shareTimeContainer = super.createEl('div', {className: 'vjs-share-time-container', id: 'vod-share-time'});
    this.shareUrlCheckbox = super.createEl('input', {className: 'vjs-share-checkbox', name: 'checkboxShare', id: 'checkboxShare'}, {type: 'checkbox'});
    this.shareUrlCheckboxLabel = super.createEl('label', {className: 'vjs-share-checkbox-label', innerHTML: 'Start at'});
    this.shareUrlCheckboxLabel.setAttribute('for', 'checkboxShare');
    this.shareTime = super.createEl('input', {className: 'vjs-share-time', name: 'shareTime', id: 'shareTime', value: '00:42:00'}, {type: 'text'});
    this.shareTimeWarning = super.createEl('div', {className: 'vjs-share-time-warning vjs-hidden'});

    // Social Icons
    this.shareIconsContainer = super.createEl('div', {className: 'vjs-share-icons-container'});
    this.shareIconFacebook = super.createEl('Button', {className: 'vjs-share-icons-facebook'});
    this.shareIconTwitter = super.createEl('Button', {className: 'vjs-share-icons-twitter'});

    // Overlay Assembly
    this.shareOverlayElementsContainer.appendChild(this.shareLabel);
    this.shareOverlayElementsContainer.appendChild(this.shareInputsContainer);
    this.shareInputsContainer.appendChild(this.shareUrlContainer);
    this.shareUrlContainer.appendChild(this.shareUrl);
    this.shareUrlContainer.appendChild(this.shareUrlBack);
    this.shareInputsContainer.appendChild(this.shareTimeContainer);
    this.shareTimeContainer.appendChild(this.shareUrlCheckbox);
    this.shareTimeContainer.appendChild(this.shareUrlCheckboxLabel);
    this.shareTimeContainer.appendChild(this.shareTime);
    this.shareTimeContainer.appendChild(this.shareTimeWarning);
    this.shareOverlayElementsContainer.appendChild(this.shareIconsContainer);
    this.shareIconsContainer.appendChild(this.shareIconTwitter);
    this.shareIconsContainer.appendChild(this.shareIconFacebook);

    el_.appendChild(this.shareOverlayExit);
    el_.appendChild(this.shareOverlayElementsContainer);

    return el_;
  }

  handleSetShareTimeUpdate() {
    let shareCheckbox = player.el_.querySelector('.vjs-share-checkbox');
    let inputTime = player.el_.querySelector('.vjs-share-time').value;

    let inputTimeSeconds = this.convertHmsToSec(inputTime);
    let durationTimesSec = player.duration();

    // Error messages to display
    let timeError = 'Please use a time less than ';
    let formatError = 'Please use valid format less than ';

    // Compare and validate manual share time change

    if (this.shareTime.value.match(/^(([1-9]?[1-9]:[1-5]\d|[1-9])|([1-5]?\d|[1-9]?)):[0-5]\d$/)) {
      if (inputTimeSeconds <= durationTimesSec) {
        if (this.shareTime.value !== player.currentTime()) {
          let offset = this.convertOffset(this.shareTime.value);
          this.shareUrl.value = this.currentURL + '?t=' + offset;
          this.shareUrlBack.innerHTML = this.shareUrl.value;
          this.removeShareTimeError();
        }
        shareCheckbox.checked = true;
      } else {
        this.displayShareTimeError(timeError);
      }
    } else {
      this.displayShareTimeError(formatError);
    }
  }


  handleShareCheckboxChange(event) {

    if(event.target.checked) {
      if(this.shareTime.value === player.currentTime()) {
        this.setEmbedCodeUrl(this.getCurrentURL(), player.currentTime());
      } else {
        this.handleSetShareTimeUpdate();
      }
    } else {
      this.setEmbedCodeUrl(this.getCurrentURL(), null);
    }
  }

  removeShareTimeError() {
    if(this.shareTime.classList.contains('error') === true) {
      this.shareTime.classList.remove('error');
    }
    if(this.shareTimeWarning.classList.contains('vjs-hidden') !== true) {
      this.shareTimeWarning.classList.add('vjs-hidden');
    }
  }


  displayShareTimeError(errorCode) {
    let shareCheckbox = player.el_.querySelector('.vjs-share-checkbox');
    let durationTime = videojs.formatTime(player.duration());

    this.shareTime.classList.add('error');
    shareCheckbox.checked = false;
    this.setEmbedCodeUrl(this.getCurrentURL(), null);
    this.shareTimeWarning.classList.remove('vjs-hidden');
    this.shareTimeWarning.innerHTML = errorCode + durationTime;
  }

  convertHmsToSec(timeToConvert) {
    var times = timeToConvert.split(":");
    let seconds;

    if(times.length === 2) {  //  If Share Time = m:ss || mm:ss
      seconds = parseInt(times[1], 10) + (parseInt(times[0], 10) * 60);
      return seconds;
    } else {  //  If Share Time = h:mm:ss || hh:mm:ss
      seconds = parseInt(times[2], 10) + (parseInt(times[1], 10) * 60) + (parseInt(times[0], 10) * 3600);
      return seconds;
    }
  }


  convertOffset(offset) {

    let segments,
      seconds = 0,
      multiples = [1, 60, 3600],
      ret = '',
      i,
      s;

    if (offset) {
      segments = offset.split(':');
      if (segments.length >= 1 && segments.length <= 3) {
        // Parse each segment into an integer to remove leading zeros and other dentritis
        for(i = 0; i < segments.length; ++i) {
          s = parseInt(segments[i], 10) * multiples[segments.length - 1 - i];
          if (isNaN(s)) {
            return '';
          }
          seconds += s;
        }
        ret = '';
        if (seconds >= 3600 && Math.floor(seconds / 3600) !== 0) {
          ret = Math.floor(seconds / 3600) + 'h';
          seconds = seconds % 3600;
        }

        if (seconds >= 60 && Math.floor(seconds / 60) !== 0) {
          ret += Math.floor(seconds / 60) + 'm';
          seconds = seconds % 60;
        }

        if (seconds > 0) {
          ret += seconds + 's';
        }

        return ret;

      }
    }

    return '';
  }

  // Make overlay visible
  show() {
    if(player.duration() === Infinity) {
      this.shareTimeContainer.classList.add('vjs-hidden');
    } else {
      this.setShareTime(player.currentTime());
      this.shareTimeContainer.classList.remove('vjs-hidden');
      player.pause();
    }

    this.el().classList.add('vjs-share-overlay-on');
  }

  // Make overlay hidden
  hide() {
    this.el().classList.remove('vjs-share-overlay-on');
    player.play();
    this.resetShareInputs();
    this.removeShareTimeError();
    player.trigger(EVENTS.SHARE_CLOSE);
  }

  resetShareInputs() {
    let checkBox = player.el_.getElementsByTagName("input");
    for (var i = 0; i < checkBox.length; i++) {
      if (checkBox[i].type === 'checkbox')
        checkBox[i].checked = false;
    }
    this.setEmbedCodeUrl(this.getCurrentURL(), null);
    this.setShareTime(player.currentTime());
  }

  setEmbedCodeUrl(value, offset) {
    this.currentURL = value;

    this.shareUrl.value = (offset) ? value + '?t=' + this.convertOffset(videojs.formatTime(offset)) : value;
    this.shareUrlBack.innerHTML = this.shareUrl.value;
  }

  getCurrentURL() {
    return this.currentURL || null;
  }

  setShareTime(value) {
    this.shareTime.value = videojs.formatTime(value);
  }

  handleFacebookClick() {
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u={URL}&title={TITLE}'.replace('{URL}', encodeURIComponent(this.shareUrl.value)).replace('{TITLE}', encodeURIComponent((player.mediainfo) ? player.mediainfo.name : '')),
      '_blank',
      'width=600, height=400, top=100, left=100, titlebar=yes, modal=yes, resizable=yes, toolbar=no, status=1, location=no, menubar=no, centerscreen=yes'
    );
  }

  handleTwitterClick() {
    window.open(
      'https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fabout.twitter.com%2Fresources%2Fbuttons&text={TITLE}&tw_p=tweetbutton&url={URL}'.replace('{URL}', encodeURIComponent(this.shareUrl.value)).replace('{TITLE}', encodeURIComponent((player.mediainfo) ? player.mediainfo.name : '')),
      '_blank',
      'width=600, height=400, top=100, left=100, titlebar=yes, modal=yes, resizable=yes, toolbar=no, status=1, location=no, menubar=no, centerscreen=yes'
    );
  }

  // Gets user / stream details and creates url to for shareUrl input
  handleLoadedMetadata() {

    let responseValue = '';

    if (player.mediainfo) {
      if(player.duration() === Infinity) {
        if(location.hostname !== DEFAULTS.URL.UOL) {
          responseValue = DEFAULTS.URL.WEB + '/' + UTIL.getChannelNameFromTags(player.mediainfo.tags);
        } else {
          responseValue = DEFAULTS.URL.UOL + '/' + UTIL.getChannelNameFromTags(player.mediainfo.tags);
        }
      } else {
        responseValue = DEFAULTS.URL.EMBED + '/' + UTIL.getChannelNameFromTags(player.mediainfo.tags) + '/' + player.mediainfo.reference_id;
      }
    } else {
      responseValue = player.currentSrc();
    }

    this.setEmbedCodeUrl('http://' + responseValue);
  }

}

videojs.registerComponent('AzubuShareOverlay', AzubuShareOverlay);
