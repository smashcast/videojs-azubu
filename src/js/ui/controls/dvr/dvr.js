import videojs from 'video.js';
import AzubuLiveIndicator from './azubu-live-indicator';


videojs.MouseTimeDisplay.prototype.update = function(newTime, position) {
  let time;
  let tooltip = this.el().querySelector('.vjs-time-tooltip');

  if(newTime === Infinity) {

    let windowDuration = this.player_.seekable().end(0) - this.player_.seekable().start(0);
    let perc = (position/this.player_.width());
    let percTime = windowDuration * perc;
    let deltaTime = this.player_.seekable().end(0) - percTime;

    time = '<< ' + videojs.formatTime(deltaTime);
  } else {
    time = videojs.formatTime(newTime, (this.player_.duration() === Infinity) ? this.player_.seekable().end(0) : this.player_.duration());
  }

  this.el().style.left = position + 'px';
  this.el().setAttribute('data-current-time', time);

  // Prevents progress bar timeTooltip from leaving the viewable player surface
  let clampedPosition = this.clampPosition_(position);
  let difference = position - clampedPosition + 1;
  let tooltipWidth = parseFloat(window.getComputedStyle(this.tooltip).width);
  let tooltipWidthHalf = tooltipWidth / 2;

  this.tooltip.innerHTML = time;
  this.tooltip.style.right = `-${tooltipWidthHalf - difference}px`;
};


videojs.SeekBar.prototype.getPercent = function() {
  let percent = 0;

  try{
    if(this.player_.duration() === Infinity) {
      percent = this.player_.currentTime() / (this.player_.seekable().end(0) - this.player_.seekable().start(0));
    } else {
      percent = this.player_.currentTime() / this.player_.duration();
    }
  } catch(err) {

  }

  return percent >= 1 ? 1 : percent;
};

videojs.SeekBar.prototype.handleMouseMove = function(event) {
  let newTime = 0;

  if(this.player_.duration() === Infinity) {
    newTime = this.calculateDistance(event) * this.player_.seekable().end(0);
  } else {
    newTime = this.calculateDistance(event) * this.player_.duration();

    // Don't let video end while scrubbing.
    if (newTime === this.player_.duration()) { newTime = newTime - 0.1; }
  }

  // Set new time (tell player to seek to new time)
  this.player_.currentTime(newTime);
};

export const init = (player, options) => {

  player.controlBar.addChild(new AzubuLiveIndicator(player, options));

};
