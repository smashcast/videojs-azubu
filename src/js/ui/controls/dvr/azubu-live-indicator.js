import videojs from 'video.js';
import * as EVENTS from '../../../util/event';

let Component = videojs.getComponent('Component');

const defaults = {
  className: 'vjs-live-status-control',
  classes: {
    live: 'vjs-live-status-live',
    dvr: 'vjs-live-status-dvr',
    display: 'vjs-live-status',
    hidden: 'vjs-hidden'
  }

};

export default class AzubuLiveIndicator extends Component {
  constructor(player, options) {
    super(player, options);

    this.on(player, EVENTS.TIME_UPDATE, this.handleTimeUpdate);
    this.on(player, EVENTS.LOADED_METADATA, this.handleLoadedMetadata);
    this.on(this.liveDisplay, 'click', this.handleClick);
  }

  createEl(){
    let el_ = super.createEl('div', {
      className: defaults.className + ' ' + this.buildCSSClass()
    });

    this.liveDisplay = super.createEl('button', {className: 'vjs-live-status vjs-live-status-dvr', innerHTML: 'live'});

    el_.appendChild(this.liveDisplay);

    return el_;
  }

  handleTimeUpdate(){
    try {
      if(player.duration() === Infinity && player.seekable() && player.seekable().end(0) && (player.seekable().end(0)-player.currentTime()) < 20 ) {
        // LIVE - RED
        this.liveDisplay.classList.remove(defaults.classes.dvr);
        this.liveDisplay.classList.add(defaults.classes.live);
      } else {
        // Click to go LIVE - GREY
        this.liveDisplay.classList.add(defaults.classes.dvr);
        this.liveDisplay.classList.remove(defaults.classes.live);
      }
    } catch(err) {
      console.log(err);
    }
  }

  handleClick(){
    player.currentTime(player.seekable().end(0)-10);
  }

  handleLoadedMetadata(){
    if(player.duration() !== Infinity) {
      this.addClass('vjs-hidden');
    } else {
      this.removeClass('vjs-hidden');
      this.addClass('no-select');
    }
  }
}

videojs.registerComponent('AzubuLiveIndicator', AzubuLiveIndicator);
