import * as EVENTS from '../../util/event';
import * as UTIL from '../../util/util';
import * as DVR from './dvr/dvr';

import AzubuMenuButton from './menu/azubu-menu-button';
import AzubuInfoButton from './info/info-menu-button';

const initialVolume = UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.VOLUME) || .5;

const handleVolumeChange = (player) => {
  UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.VOLUME, player.volume());
};

// Hotkey Spacebar for play/pause toggle
const handleKeyPress = (event) => {
  if (event.which === 32) {
    document.activeElement.blur();
    event.preventDefault();
    (player.paused()) ? player.play() : player.pause();
  }
};

export const init = (player, options) => {
  console.warn(initialVolume);

  player.volume(initialVolume);

  DVR.init(player, options);

  player.controlBar.addChild(new AzubuMenuButton(player, options));
  player.controlBar.addChild(new AzubuInfoButton(player, options));

  player.on(EVENTS.VOLUME_CHANGE, () => {
    handleVolumeChange(player);
  });

  player.one(EVENTS.LOAD_START, () => {
    player.on(document, 'keypress', handleKeyPress);
  });

};
