import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';
import AzubuShareOverlay from '../../stage/social/azubu-share-overlay';

let Component = videojs.getComponent('Component');

const defaults = {
  fpsBandwidthLimit: 3300000,
  classNames: {
    menuPanels: {
      container: 'vjs-menu-container',
      settings: {
        container: 'vjs-settings-options-container',
        ul: 'vjs-settings-menu vjs-settings-menu-toggle',
        top: 'vjs-settings-menu-top'
      },
      renditions: {
        container: 'vjs-rendition-options-container',
        ul: 'vjs-renditions-menu vjs-renditions-menu-toggle',
        top: 'vjs-renditions-menu-top'
      },
      item: 'vjs-settings-menu-item',
      label: 'vjs-settings-menu-item-label',
      source: {
        item: 'vjs-settings-menu-item-source',
        tag: 'vjs-settings-video-source-tag vjs-menu-icon vjs-hidden',
        label: 'vjs-settings-menu-item-label-source',
        enterIcon: 'vjs-source-options-enter vjs-menu-icon',
        exitIcon: 'vjs-source-options-exit vjs-menu-icon'
      }
    }
  },
  renditions: [
    {
      resolution: 1080,
      high: 6600000,
      low: 3300000
    },
    {
      resolution: 720,
      high: 5000000,
      low: 2400000
    },
    {
      resolution: 480,
      high: 2000000,
      low: 1000000
    },
    {
      resolution: 360,
      high: 1000000,
      low: 500000
    },
    {
      resolution: 270,
      high: 500000,
      low: 250000
    }
  ],
  labels: {
    fps60: '60fps',
    auto: 'Auto',
    options: 'Player Options',
    source: 'Source',
    share: 'Share',
    info: 'Info',
    popout: 'Popout',
    quality: 'Video Quality'
  }
};

export default class AzubuSettingsMenu extends Component {
  constructor(player, options) {
    super(player, options);

    this.shareOverlay = new AzubuShareOverlay(player, options);
    player.addChild(this.shareOverlay);

    this.on(this.settingsMenuItemSource, 'click', this.enterSourceOptions);
    this.on(this.settingsMenuItemShare, 'click', this.handleSettingsMenuItemShareClick);
    this.on(this.settingsMenuItemInfo, 'click', this.toggleInfoDockPanel);

    this.on(player, EVENTS.RENDITION_SELECTED, this.handleRenditionSelected);
    this.on(player, EVENTS.AUTO_MODE_SELECTED, this.handleAutoModeSelected);
    this.on(player, EVENTS.LOADED_METADATA, this.handleLoadedMetadata);
  }

  createEl() {
    let el_ = super.createEl('ul', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.settings.ul
    });

    this.settingsMenuTop = super.createEl('li', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.settings.top,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.options + '</label>'
    });
    this.settingsOptionsContainer = super.createEl('div', {className: this.buildCSSClass() + defaults.classNames.menuPanels.settings.container});
    this.settingsMenuItemSource = super.createEl('li', {className: this.buildCSSClass() + defaults.classNames.menuPanels.source.item});
    this.settingsMenuItemSourceTag = super.createEl('div', {className: this.buildCSSClass() + defaults.classNames.menuPanels.source.tag});
    this.settingsMenuItemSourceLabel = super.createEl('div', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.source.label,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + this.setDefaultLabel() + '</label>'
    });
    this.sourceOptionsEnter = super.createEl('div', {className: this.buildCSSClass() + defaults.classNames.menuPanels.source.enterIcon});
    this.settingsMenuItemShare = super.createEl('li', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.item,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.share + '</label>'
    });
    this.settingsMenuItemInfo = super.createEl('li', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.item,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.info + '</label>'
    });
    this.settingsMenuItemInfo.classList.add('vjs-settings-menu-item-infodock');
    this.settingsMenuItemPopout = super.createEl('li', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.item,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.popout + '</label>'
    });
    this.settingsMenuItemPopout.classList.add('vjs-settings-menu-item-inactive');

    this.settingsMenuItemSource.appendChild(this.settingsMenuItemSourceTag);
    this.settingsMenuItemSource.appendChild(this.settingsMenuItemSourceLabel);
    this.settingsMenuItemSource.appendChild(this.sourceOptionsEnter);

    this.settingsOptionsContainer.appendChild(this.settingsMenuItemSource);
    this.settingsOptionsContainer.appendChild(this.settingsMenuItemShare);
    this.settingsOptionsContainer.appendChild(this.settingsMenuItemInfo);
    this.settingsOptionsContainer.appendChild(this.settingsMenuItemPopout);

    el_.appendChild(this.settingsMenuTop);
    el_.appendChild(this.settingsOptionsContainer);

    return el_;
  }

  enterSourceOptions() {
    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    settingsMenu.classList.add('vjs-panel-fadeout');
    settingsMenu.classList.remove('vjs-panel-fadein');
    renditionsMenu.classList.toggle('vjs-renditions-menu-toggle');
    renditionsMenu.classList.toggle('vjs-menu-upto-renditions');
    this.toggleMenuIcons();
    this.toggleMenuLabels();
  }

  toggleMenuIcons() {
    let settingsMenuIcons = this.el().parentNode.getElementsByClassName('vjs-menu-icon');
    let i;
    for (i = 0; i < settingsMenuIcons.length; i++) {
      settingsMenuIcons[i].classList.remove('vjs-toggle-menu-labels');
      settingsMenuIcons[i].style.offsetWidth = settingsMenuIcons[i].offsetWidth;
      settingsMenuIcons[i].classList.add('vjs-toggle-menu-labels');
    }
    return false;
  }

  toggleMenuLabels() {
    let menuLabels = this.el().parentNode.getElementsByTagName('label');
    let k;
    for (k = 0; k < menuLabels.length; k++) {
      menuLabels[k].classList.remove('vjs-toggle-menu-labels');
      menuLabels[k].style.offsetWidth = menuLabels[k].offsetWidth;
      menuLabels[k].classList.add('vjs-toggle-menu-labels');
    }
    return false;
  }

  toggleInfoDockPanel() {
    let infoInfoPanelContent = player.el_.querySelector('.vjs-infodock-content');
    infoInfoPanelContent.classList.toggle('vjs-panel-fadeout');
  }

  handleSettingsMenuItemShareClick() {
    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    settingsMenu.classList.toggle('vjs-settings-menu-toggle');
    settingsMenu.classList.remove('vjs-panel-fadein');
    renditionsMenu.classList.toggle('vjs-menu-upto-renditions');
    renditionsMenu.classList.add('vjs-no-delay');
    player.trigger(EVENTS.SHARE_OPEN);
  }

  handleLoadedMetadata() {
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    if(player.duration() === Infinity) {
      this.settingsMenuItemSource.classList.remove('vjs-hidden');
      renditionsMenu.classList.remove('vjs-hidden');
    } else {
      this.settingsMenuItemSource.classList.add('vjs-hidden');
      renditionsMenu.classList.add('vjs-hidden');
    }
  }

  handleAutoModeSelected(event, data) {
    if (data === true) {
      this.updateSelectedRenditionLabel(defaults.labels.auto);
    }
  }

  handleRenditionSelected(event, data) {
    this.updateSelectedRenditionLabel(this.formatLabelFromRendition(data));
  }

  updateSelectedRenditionLabel(label) {
    this.settingsMenuItemSourceLabel.querySelector('.vjs-toggle-menu-labels').innerHTML = label;
  }

  formatLabelFromRendition(rendition) {
    let label = 'AUTO';

    if(rendition && typeof rendition !== 'undefined' && rendition.resolution) {
      label = rendition.resolution + 'p';

      if(rendition.bandwidth && rendition.bandwidth > defaults.fpsBandwidthLimit) {
        label += ' ' + defaults.labels.fps60;
      }
    }

    return label;
  };

  setDefaultLabel() {
    if(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE) === 'true') {
      return defaults.labels.auto;
    } else {
      return this.formatLabelFromRendition(JSON.parse(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.SELECTED_RENDITION)));
    }
  }
}

export default class AzubuRenditionsMenu extends Component {
  constructor(player, options) {
    super(player, options);

    this.on(this.renditionsMenuTop, 'click', this.exitSourceOptions);
    this.on(player, EVENTS.LOADED_METADATA, this.handleLoadedMetadata);
    this.on(player, EVENTS.RENDITION_SELECTED, this.exitSourceOptions);
    this.on(player, EVENTS.AUTO_MODE_SELECTED, this.exitSourceOptions);

  }

  handleLoadedMetadata() {

    let player = this.player_;

    if (player.tech_.hls && player.tech_.hls.playlists && player.tech_.hls.playlists.master && player.tech_.hls.playlists.master.playlists) {
      let sortedPlaylists = player.tech_.hls.playlists.master.playlists.slice();
      if (sortedPlaylists && sortedPlaylists[0].attributes) {
        this.createMenuByPlaylist(sortedPlaylists);
      }
    }
    this.setSelectedLabel();
  }

  clearRenditionMenuItems() {
    videojs.emptyEl(this.renditionsOptionsContainer);
  }

  getRenditionSettingsByHeight(height) {
    let rendition = null;

    defaults.renditions.forEach(function(value){
      if(value.resolution === height) {
        rendition = value;
        return value;
      }
    });

    return rendition;
  }

  createMenuByPlaylist(playlists) {
    this.clearRenditionMenuItems();

    this.renditionsOptionsContainer.appendChild(this.renditionsMenuItemAuto);

    for (var i = 0; i < playlists.length; i++) {
      let rendition = playlists[i];
      let rendSettings = this.getRenditionSettingsByHeight(rendition.attributes.RESOLUTION.height);
      let isHighFramerate = (rendition.attributes.BANDWIDTH > rendSettings.low);

      this.renditionsOptionsContainer.appendChild(super.createEl('li', {
        onclick: this.handleRenditionItemClick,
        className: this.buildCSSClass() + defaults.classNames.menuPanels.item,
        innerHTML: '<label data-bandwidth="' + rendition.attributes.BANDWIDTH + '" data-framerate="' + ((isHighFramerate) ? 60 : 30) + '" data-resolution="' + rendition.attributes.RESOLUTION.height + '" class="vjs-toggle-menu-labels vjs-rendition-item">' + rendition.attributes.RESOLUTION.height + ((isHighFramerate) ? 'p 60fps' : 'p') + '</label>'
      }));
    }
  }

  handleRenditionItemClick(e) {
    player.trigger(EVENTS.RENDITION_SELECTED, (typeof e.target.dataset.bandwidth === 'undefined') ? e.target.querySelector('.vjs-rendition-item').dataset : e.target.dataset);
    player.trigger(EVENTS.STREAM_SWITCH_START);
    player.trigger(EVENTS.STREAM_SWITCH_END);

    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    settingsMenu.classList.add('vjs-settings-menu-toggle');
    settingsMenu.classList.remove('vjs-panel-fadeout');
    settingsMenu.classList.remove('vjs-panel-fadein');
    renditionsMenu.classList.add('vjs-renditions-menu-toggle');
    renditionsMenu.classList.remove('vjs-menu-upto-renditions');
    renditionsMenu.classList.add('vjs-no-delay');
  }

  handleAutoClick() {
    player.trigger(EVENTS.AUTO_MODE_SELECTED, true);

    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    settingsMenu.classList.add('vjs-settings-menu-toggle');
    settingsMenu.classList.remove('vjs-panel-fadeout');
    settingsMenu.classList.remove('vjs-panel-fadein');
    renditionsMenu.classList.add('vjs-renditions-menu-toggle');
    renditionsMenu.classList.remove('vjs-menu-upto-renditions');
    renditionsMenu.classList.add('vjs-no-delay');
  }

  createEl() {

    let el_ = super.createEl('ul', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.renditions.ul
    });

    this.renditionsMenuTop = super.createEl('li', {
      onclick: el_.exitSourceOptions,
      className: this.buildCSSClass() + defaults.classNames.menuPanels.renditions.top,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.quality + '</label>'
    });
    this.sourceOptionsExit = super.createEl('div', {className: this.buildCSSClass() + defaults.classNames.menuPanels.source.exitIcon});

    this.renditionsOptionsContainer = super.createEl('div', {className: this.buildCSSClass() + defaults.classNames.menuPanels.renditions.container});
    this.renditionsMenuItemAuto = super.createEl('li', {
      onclick: this.handleAutoClick,
      className: this.buildCSSClass() + defaults.classNames.menuPanels.item,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.auto + '</label>'
    });

    this.renditionsMenuItemSource = super.createEl('li', {className: this.buildCSSClass() + defaults.classNames.menuPanels.source.item});
    this.renditionsMenuItemSourceTag = super.createEl('div', {className: this.buildCSSClass() + defaults.classNames.menuPanels.source.tag});
    this.renditionsMenuItemSourceLabel = super.createEl('div', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.source.label,
      innerHTML: '<label class="vjs-toggle-menu-labels">' + defaults.labels.source + '</label>'
    });

    this.renditionsMenuTop.appendChild(this.sourceOptionsExit);
    this.renditionsMenuItemSource.appendChild(this.renditionsMenuItemSourceTag);
    this.renditionsMenuItemSource.appendChild(this.renditionsMenuItemSourceLabel);

    el_.appendChild(this.renditionsMenuTop);
    el_.appendChild(this.renditionsOptionsContainer);

    return el_;
  }

  exitSourceOptions() {
    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');
    let settingsMenuTop = player.el_.querySelector('.vjs-settings-menu-top');

    settingsMenuTop.classList.toggle('vjs-settings-menu-top-toggle');
    settingsMenu.classList.remove('vjs-panel-fadeout');
    settingsMenu.classList.add('vjs-panel-fadein');
    renditionsMenu.classList.toggle('vjs-renditions-menu-toggle');
    renditionsMenu.classList.toggle('vjs-menu-upto-renditions');
    this.toggleMenuIcons();
    this.toggleMenuLabels();
  }

  setSelectedLabel() {
    let settingsMenuRendition = player.el_.querySelector('.vjs-settings-menu-item-label-source');
    let settingsMenuRenditionLabel = settingsMenuRendition.getElementsByTagName('label')[0].innerHTML;
    let menuLabels = this.el().getElementsByTagName('label');
    let k;
    for (k = 0; k < menuLabels.length; k++) {
      if(menuLabels[k].innerHTML === settingsMenuRenditionLabel) {
        menuLabels[k].style.fontWeight = '700';
      } else {
        menuLabels[k].style.fontWeight = '300';
      }
    }
  }

  toggleMenuIcons() {
    let settingsMenuIcons = this.el().parentNode.getElementsByClassName('vjs-menu-icon');
    let i;
    for (i = 0; i < settingsMenuIcons.length; i++) {
      settingsMenuIcons[i].classList.remove('vjs-toggle-menu-labels');
      settingsMenuIcons[i].style.offsetWidth = settingsMenuIcons[i].offsetWidth;
      settingsMenuIcons[i].classList.add('vjs-toggle-menu-labels');
    }
    return false;
  }

  toggleMenuLabels() {
    let menuLabels = this.el().parentNode.getElementsByTagName('label');
    let k;
    for (k = 0; k < menuLabels.length; k++) {
      menuLabels[k].classList.remove('vjs-toggle-menu-labels');
      menuLabels[k].style.offsetWidth = menuLabels[k].offsetWidth;
      menuLabels[k].classList.add('vjs-toggle-menu-labels');
    }
    this.setSelectedLabel();
    return false;
  }

}

videojs.registerComponent('AzubuSettingsMenu', AzubuSettingsMenu);

export default class AzubuMenuPanels extends Component {
  constructor(player, options) {
    super(player, options);

    player.settingsMenu = new AzubuSettingsMenu(player, options);
    player.renditionsMenu = new AzubuRenditionsMenu(player, options);
    this.addChild(player.settingsMenu);
    this.addChild(player.renditionsMenu);

    this.on(player, 'pause', this.handleMenuPanelsPause);
    this.on(player, 'userinactive', this.handleMenuPanelInactive);
  }

  createEl() {
    return super.createEl('div', {
      className: this.buildCSSClass() + defaults.classNames.menuPanels.container
    });
  }

  handleMenuPanelInactive() {
    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    if (settingsMenu.classList.contains('vjs-settings-menu-toggle')) {
      //
    } else {
      settingsMenu.classList.add('vjs-settings-menu-toggle');
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
      renditionsMenu.classList.remove('vjs-menu-upto-renditions');
      renditionsMenu.classList.add('vjs-no-delay');
    }

    if (renditionsMenu.classList.contains('vjs-renditions-menu-toggle')) {
      //
    } else {
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
      renditionsMenu.classList.add('vjs-renditions-menu-toggle');
      renditionsMenu.classList.remove('vjs-menu-upto-renditions');
    }
  }

  handleMenuPanelsPause() {
    let playerPaused = player.paused();
    let playerEnded = player.ended();
    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');

    if ((playerPaused || playerEnded) && settingsMenu.classList.contains('vjs-settings-menu-toggle')) {
      //
    } else {
      settingsMenu.classList.add('vjs-settings-menu-toggle');
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
      renditionsMenu.classList.remove('vjs-menu-upto-renditions');
      renditionsMenu.classList.add('vjs-no-delay');
    }

    if ((playerPaused || playerEnded) && renditionsMenu.classList.contains('vjs-renditions-menu-toggle')) {
      //
    } else {
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
      renditionsMenu.classList.add('vjs-renditions-menu-toggle');
      renditionsMenu.classList.remove('vjs-menu-upto-renditions');
    }
  }
}

videojs.registerComponent('AzubuMenuPanels', AzubuMenuPanels);
