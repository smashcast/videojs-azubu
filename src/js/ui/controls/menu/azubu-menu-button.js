import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import AzubuMenuPanels from './azubu-menu-panel';
import AzubuRenditionNotification from './azubu-rendition-notification';

let Button = videojs.getComponent('Button');

const defaults = {
  className: 'vjs-settings-control '
};

export default class AzubuMenuButton extends Button {
  constructor(player, options) {
    super(player, options);

    this.renditionNotification = new AzubuRenditionNotification(player, options);
    this.menuPanels = new AzubuMenuPanels(player, options);

    player.addChild(this.renditionNotification);
    player.controlBar.addChild(this.menuPanels);

    player.on(EVENTS.STREAM_SWITCH_START, this.handleStreamSwitchStart);
  }

  createEl(){
    return super.createEl('button', {
      className: defaults.className + ' ' + this.buildCSSClass(),
      id: 'menu-button'
    });
  }

  // Manage menu open / close behavior from settings (gear/cog) icon
  handleClick(){
    super.handleClick();
    let settingsMenu = player.el_.querySelector('.vjs-settings-menu');
    let renditionsMenu = player.el_.querySelector('.vjs-renditions-menu');
    if (settingsMenu.classList.contains('vjs-settings-menu-toggle')
      && renditionsMenu.classList.contains('vjs-renditions-menu-toggle')) {
      settingsMenu.classList.remove('vjs-settings-menu-toggle');
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
      renditionsMenu.classList.add('vjs-menu-upto-renditions');
      renditionsMenu.classList.remove('vjs-no-delay');
    }
    else {
      settingsMenu.classList.add('vjs-settings-menu-toggle');
      renditionsMenu.classList.remove('vjs-menu-upto-renditions');
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
      renditionsMenu.classList.add('vjs-no-delay');
    }
    if (renditionsMenu.classList.contains('vjs-renditions-menu-toggle')) {
      //
    }
    else {
      renditionsMenu.classList.add('vjs-renditions-menu-toggle');
      renditionsMenu.classList.add('vjs-no-delay');
      settingsMenu.classList.remove('vjs-panel-fadeout');
      settingsMenu.classList.remove('vjs-panel-fadein');
    }
  }


  // Rotates menu gear/cog icon when switching renditions
  handleStreamSwitchStart() {
    let menuButton = player.el_.querySelector('.vjs-settings-control');

    if (menuButton.classList.contains('vjs-icon-spin')) {
      menuButton.classList.remove('vjs-icon-spin');
      menuButton.style.offsetWidth = menuButton.offsetWidth;
      menuButton.classList.add('vjs-icon-spin');
    }
    else {
      menuButton.classList.add('vjs-icon-spin');
    }
  }

}

videojs.registerComponent('AzubuMenuButton', AzubuMenuButton);
