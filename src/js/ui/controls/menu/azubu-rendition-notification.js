import videojs from 'video.js';
import * as EVENTS from '../../../util/event';

let Component = videojs.getComponent('Component');

const defaults = {
  className: 'vjs-rendition-notification vjs-rendition-notification-toggle vjs-hidden',
  message: 'Rendition changing to'
};

export default class AzubuRenditionNotification extends Component {
  constructor(player,options) {
    super(player, options);

    this.on(player, EVENTS.STREAM_SWITCH_END, this.setRenditionNotification);
    this.on(player, EVENTS.RENDITION_SELECTED, this.showRenditionNotification);
  }

  createEl(){
    return super.createEl('div', {
      className: this.buildCSSClass() + defaults.className,
      innerHTML: defaults.message
    });
  }

  showRenditionNotification() {
    let renditionNotification = player.el_.querySelector('.vjs-rendition-notification');

    if (renditionNotification.classList.contains('vjs-hidden')) {
      renditionNotification.classList.remove('vjs-hidden');
    }
    else {
      renditionNotification.classList.remove('vjs-rendition-notification-toggle');
      renditionNotification.style.offsetWidth = renditionNotification.offsetWidth;
      renditionNotification.classList.add('vjs-rendition-notification-toggle');
    }
  }

  setRenditionNotification() {
    let sourceRendition = player.el_.querySelector('.vjs-settings-menu-item-label-source');
    let sourceRenditionValue = sourceRendition.querySelector('.vjs-toggle-menu-labels').innerHTML;

    this.el().innerHTML = defaults.message + ' ' + sourceRenditionValue;
  }
}

videojs.registerComponent('AzubuRenditionNotification', AzubuRenditionNotification);
