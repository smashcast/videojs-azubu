import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import AzubuInfoPanel from './info-panel';

let Button = videojs.getComponent('Button');

const defaults = {
  className: 'vjs-infodock-control '
};

export default class AzubuInfoButton extends Button {
  constructor(player, options) {
    super(player, options);

    this.infoPanel = new AzubuInfoPanel(player, options);
    this.infoPanel.hide();

    player.addChild(this.infoPanel);

    this.on(player,[EVENTS.AD_START, EVENTS.LOADED_METADATA], this.handleLoadedMetadata);
    this.hide();
  }

  createEl(){
    return super.createEl('button', {
      className: defaults.className + this.buildCSSClass()
    });
  }

  handleLoadedMetadata(e){
    if(player.mediainfo) {
      this.infoPanel.show();
      this.show();
    } else {
      this.infoPanel.hide();
      this.hide();
    }
  }

  handleClick(){
    let infoInfoPanelContent = player.el_.querySelector('.vjs-infodock-content');
    infoInfoPanelContent.classList.toggle('vjs-panel-fadeout');
  }
}

videojs.registerComponent('AzubuInfoButton', AzubuInfoButton);
