import videojs from 'video.js';
import * as EVENTS from '../../../util/event';
import * as UTIL from '../../../util/util';
import * as API from '../../../communication/api';

let Component = videojs.getComponent('Component');

const defaults = {
  className: 'vjs-stream-details',
  user: {
    name: 'Faker',
    avatarUrl: 'http://img.azubu.tv/images/userprofile/0/f/0f2c213acbb4d9da421b3c07f02ebc5aa0677ef4-260x260.jpeg',
    gameName: 'League of Legends',
    category: '/games/'
  }
};

export default class AzubuInfoPanel extends Component {
  constructor(player, options) {
    super(player, options);

    this.on(player, EVENTS.LOAD_START, this.handleLoadedMetadata);
    this.on(player, EVENTS.BROADCASTER_DATA_CHANGE, this.handleBroadcasterDataChange);
    this.on(this.broadcasterScreenName, 'click', this.handleScreenNameClick);
    this.on(this.gameTitle, 'click', this.handleGameTitleClick);
  }

  createEl() {
    let el_ = super.createEl('div', {
      className: this.buildCSSClass() + defaults.className
    });

    this.infodockContent = super.createEl('div', {className: 'vjs-infodock-content', innerHTML: ''});
    this.broadcasterImageContainer = super.createEl('div', {
      className: 'vjs-broadcaster-image-container',
      innerHTML: ''
    });
    this.broadcasterImageMask = super.createEl('div', {className: 'vjs-broadcaster-image', innerHTML: ''});
    this.broadcasterImage = super.createEl('img');

    this.nameGameContainer = super.createEl('div', {className: 'vjs-name-game-container'});
    this.broadcasterScreenName = super.createEl('a', {href: '#', className: 'vjs-broadcaster-screenname', innerHTML: 'Loading'});
    this.gameNameLabelContainer = super.createEl('div', {className: 'vjs-game-name-label-container'});
    this.gameLabel = super.createEl('div', {className: 'vjs-game-label', innerHTML: 'playing'});
    this.gameTitle = super.createEl('a', {href: '#', className: 'vjs-game-title', innerHTML: 'Loading'});
    this.streamTitle = super.createEl('div', {className: 'vjs-stream-title', innerHTML: 'Loading'});

    this.broadcasterImageMask.appendChild(this.broadcasterImage);
    this.broadcasterImageContainer.appendChild(this.broadcasterImageMask);
    this.nameGameContainer.appendChild(this.broadcasterScreenName);
    this.nameGameContainer.appendChild(this.gameNameLabelContainer);
    this.gameNameLabelContainer.appendChild(this.gameLabel);
    this.gameNameLabelContainer.appendChild(this.gameTitle);

    this.infodockContent.appendChild(this.broadcasterImageContainer);
    this.infodockContent.appendChild(this.nameGameContainer);
    this.infodockContent.appendChild(this.streamTitle);

    el_.appendChild(this.infodockContent);

    return el_;
  }

  handleLoadedMetadata() {
    if (player.mediainfo) {
      this.updateTitle(player.mediainfo.name);
      this.updateGameName(UTIL.getGameNameFromTags(player.mediainfo.tags));
      this.updateScreenName(UTIL.getChannelNameFromTags(player.mediainfo.tags));
      this.updateGameNameUrl(UTIL.getGameNameForUrlFromTags(player.mediainfo.tags));

      if(UTIL.getChannelNameFromTags(player.mediainfo.tags) && UTIL.getChannelNameFromTags(player.mediainfo.tags) !== 'undefined') {
        API.getChannelInfoByChannelName(UTIL.getChannelNameFromTags(player.mediainfo.tags), function (data, status) {
          player.trigger(EVENTS.BROADCASTER_DATA_CHANGE, {data: data, status: status});
        });
      }

    }
  }

  handleBroadcasterDataChange(event, response) {
    if (response.data && response.data.user) {
      this.updateScreenName(response.data.user.display_name);
      this.updateScreenNameUrl(UTIL.getChannelPageUrlByTags(player.mediainfo.tags));
      this.updateImage(response.data.user.profile.url_photo_large);
    }
    if (response.data && response.data.category) {
      this.updateGameName(response.data.category.title);
      this.updateGameNameUrl(UTIL.getGameNameForUrlFromTags(player.mediainfo.tags));
    }
  }

  handleScreenNameClick() {
    window.open(UTIL.getChannelPageUrlByTags(player.mediainfo.tags), '_top');
  }

  handleGameTitleClick() {
    window.open(UTIL.getGameNameForUrlFromTags(player.mediainfo.tags), '_top');
  }

  updateTitle(name) {
    this.streamTitle.innerHTML = name;
  }

  updateImage(url) {
    this.broadcasterImage.src = url;
  }

  updateScreenName(name) {
    this.broadcasterScreenName.innerHTML = name;
  }

  updateScreenNameUrl(url) {
    this.broadcasterScreenName.href = url;
  }

  updateGameName(name) {
    this.gameTitle.innerHTML = name;
  }

  updateGameNameUrl(url) {
    this.gameTitle.href = url;
  }

}

videojs.registerComponent('AzubuInfoPanel', AzubuInfoPanel);
