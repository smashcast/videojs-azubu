import * as EVENTS from '../util/event';

const ReconnectEvents = [1, 2, -2];

const ReconnectStatus = {
  attempts: 0,
  maxAttempts: 3
};

let handleReconnecting = (event, src) => {
  console.log('Player Reconnecting');
  console.log(src);
  ReconnectStatus.attempts++;
  event.target.player.one(EVENTS.LOAD_START, () => {
    event.target.player.trigger(EVENTS.RECONNECTED);
  });

  player.src(src);
};

let handleReconnected = () => {
  console.log('Player Reconnected');
  ReconnectStatus.attempts = 0;
};

let handleError = (event) => {
  if(ReconnectEvents.indexOf(event.target.player.error().code) !== -1) {
    if(ReconnectStatus.attempts < ReconnectStatus.maxAttempts) {
      event.preventDefault();
      event.stopPropagation();
      event.target.player.trigger(EVENTS.RECONNECTING, {
        src: event.target.player.currentSrc(),
        type: event.target.player.currentType()
      });
    } else {
      console.log('reconnect attempt max');
    }
  }
};

export const init = (player) => {
  if(player) {
    player.on(EVENTS.RECONNECTING, handleReconnecting);
    player.on(EVENTS.RECONNECTED, handleReconnected);
    player.on(EVENTS.ERROR, handleError);
  }
};
