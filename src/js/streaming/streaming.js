import videojs from 'video.js';

const DESIRED_SEGMENTS = 1;
const SEGMENT_LENGTH = 10;

export const init = (player, options) => {
  try{
    //videojs.Hls.Playlist.UNSAFE_LIVE_SEGMENTS = options.UNSAFE_LIVE_SEGMENTS || DESIRED_SEGMENTS;
    videojs.Hls.GOAL_BUFFER_LENGTH = 10;
    videojs.Hls.xhr.beforeRequest = function(options) {

      /*
       * In progress - trying to fix the initial segment issue at brightcove for them.

       if(options.responseType && options.responseType === 'arraybuffer' && player.hls.stats.mediaRequests === 0) {
       let correctSegment = player.hls.playlists.media().segments[player.hls.playlists.media().segments.length-videojs.Hls.Playlist.UNSAFE_LIVE_SEGMENTS];
       let correctIndex = player.hls.playlists.media().segments.indexOf(correctSegment);

       console.warn('hello tom, wrong segment', options.uri, player.hls.playlists.media().mediaSequence);
       console.warn('correct segment', correctSegment, correctIndex);

       player.hls.playlists.media().mediaSequence += correctIndex-1;

       console.log(player.hls.playlists.media().mediaSequence);
       //options.uri = '';

       options.uri = player.hls.playlists.media().segments[player.hls.playlists.media().segments.length-videojs.Hls.Playlist.UNSAFE_LIVE_SEGMENTS-1].resolvedUri;
       }
       */

      return options;
    };
  } catch (err) {
    console.warn('Unable to setup HLS:', err.message);
  }
};
