import videojs from 'video.js';
import * as ADAPTIVE from './adaptive/selectPlaylist';
import * as MANUAL from './manual/selectPlaylist';

export const init = (player) => {
  if(videojs.Hls && !videojs.Hls.supportsNativeHls) {
    // Initialize Manual Selection Logic
    MANUAL.init(player);
    // Initial Adaptive Selection Logic
    ADAPTIVE.init(player);
  }
};

