import videojs from 'video.js';
import * as UTIL from '../../../util/util';
import * as EVENTS from '../../../util/event';

const DEFAULT_AUTO_MODE = true;

export const init = (player) => {
  player.on(EVENTS.AUTO_MODE_SELECTED, handleAutoModeSelected);
  player.on(EVENTS.RENDITION_SELECTED, handleRenditionSelected);

  player.one(EVENTS.LOADED_METADATA, handleLoadedMetadata);

  setDefaultAutoMode();

  if (UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE) === 'true') {
    player.trigger(EVENTS.AUTO_MODE_SELECTED, true);
  }
};

const setDefaultAutoMode = () => {
  let previousAutoMode = UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE);

  if (!previousAutoMode || previousAutoMode === 'undefined') {
    UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE, DEFAULT_AUTO_MODE);
  }
};

const handleLoadedMetadata = () => {
  if(player.duration()===Infinity &&
    player.tech_.hls &&
    player.tech_.hls.playlists &&
    player.tech_.hls.playlists.master) {

    if(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE) !== 'true'){
      player.trigger(EVENTS.AUTO_MODE_SELECTED, true);
    }
  }
};

const handleAutoModeSelected = (event, data) => {
  UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE, data);

  if (UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE) === 'true') selectPlaylist();
};

const handleRenditionSelected = () => {
  UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.AUTO_MODE, false);
};

export const selectPlaylist = () => {
  /**
   * Chooses the appropriate media playlist based on the current
   * bandwidth estimate and the player size.
   * @return the highest bitrate playlist less than the currently detected
   * bandwidth, accounting for some amount of bandwidth variance
   */
  videojs.HlsHandler.prototype.selectPlaylist = function () {
    //console.log('AUTO', this.tech_.hls.segmentXhrTime);

    let
      effectiveBitrate,
      sortedPlaylists = this.playlists.master.playlists.slice(),
      bandwidthPlaylists = [],
      now = +new Date(),
      i,
      variant,
      bandwidthBestVariant,
      resolutionPlusOne,
      resolutionBestVariant,
      width,
      height;

    sortedPlaylists.sort(videojs.Hls.comparePlaylistBandwidth);

    // filter out any playlists that have been excluded due to
    // incompatible configurations or playback errors
    sortedPlaylists = sortedPlaylists.filter(function (variant) {
      if (variant.excludeUntil !== undefined) {
        return now >= variant.excludeUntil;
      }
      return true;
    });

    // filter out any variant that has greater effective bitrate
    // than the current estimated bandwidth
    i = sortedPlaylists.length;
    while (i--) {
      variant = sortedPlaylists[i];

      // ignore playlists without bandwidth information
      if (!variant.attributes || !variant.attributes.BANDWIDTH) {
        continue;
      }

      effectiveBitrate = variant.attributes.BANDWIDTH * 1.1;

      if (effectiveBitrate < this.bandwidth) {
        bandwidthPlaylists.push(variant);

        // since the playlists are sorted in ascending order by
        // bandwidth, the first viable variant is the best
        if (!bandwidthBestVariant) {
          bandwidthBestVariant = variant;
        }
      }
    }

    i = bandwidthPlaylists.length;

    // sort variants by resolution
    bandwidthPlaylists.sort(videojs.Hls.comparePlaylistResolution);

    // forget our old variant from above, or we might choose that in high-bandwidth scenarios
    // (this could be the lowest bitrate rendition as  we go through all of them above)
    variant = null;

    width = parseInt(getComputedStyle(this.tech_.el()).width, 10);
    height = parseInt(getComputedStyle(this.tech_.el()).height, 10);

    // iterate through the bandwidth-filtered playlists and find
    // best rendition by player dimension
    while (i--) {
      variant = bandwidthPlaylists[i];

      // ignore playlists without resolution information
      if (!variant.attributes || !variant.attributes.RESOLUTION || !variant.attributes.RESOLUTION.width || !variant.attributes.RESOLUTION.height) {
        continue;
      }

      // since the playlists are sorted, the first variant that has
      // dimensions less than or equal to the player size is the best

      if (variant.attributes.RESOLUTION.width === width &&
        variant.attributes.RESOLUTION.height === height) {
        // if we have the exact resolution as the player use it
        resolutionPlusOne = null;
        resolutionBestVariant = variant;
        break;
      } else if (variant.attributes.RESOLUTION.width < width &&
        variant.attributes.RESOLUTION.height < height) {
        // if both dimensions are less than the player use the
        // previous (next-largest) variant
        break;
      } else if (!resolutionPlusOne ||
        (variant.attributes.RESOLUTION.width < resolutionPlusOne.attributes.RESOLUTION.width &&
        variant.attributes.RESOLUTION.height < resolutionPlusOne.attributes.RESOLUTION.height)) {
        // If we still haven't found a good match keep a
        // reference to the previous variant for the next loop
        // iteration

        // By only saving variants if they are smaller than the
        // previously saved variant, we ensure that we also pick
        // the highest bandwidth variant that is just-larger-than
        // the video player
        resolutionPlusOne = variant;
      }
    }

    // fallback chain of variants
    return resolutionPlusOne || resolutionBestVariant || bandwidthBestVariant || sortedPlaylists[0];
  };
}
