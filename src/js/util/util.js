import * as ENV from './environment';

const TAG_KEYS = {
  LIVE: 'live_',
  VOD: 'svod_',
  TEAM: 'team_',
  CATEGORY: 'category_'
};

export const LOCAL_STORAGE_KEYS = {
  USER_NAME: 'azubu.user.name',
  USER_ID: 'azubu.user.id',
  SUBSCRIPTIONS: 'azubu.user.subscriptions',
  RESTRICTIONS: 'azubu.player.restrictions',
  VOLUME: 'azubu.player.volume',
  SELECTED_BANDWIDTH: 'azubu.player.bandwidth',
  AUTO_MODE: 'azubu.player.autoMode',
  OVERLAY_CHANNELS: 'azubu.overlay.channels'
};

export const getGameNameFromTags = (tags) => {
  if(!tags || tags === undefined) {
    return '';
  } else {
    for (var i=0; i <tags.length; i++) {
      if(tags[i]){
        if(tags[i].indexOf(TAG_KEYS.CATEGORY) === 0) {
          return tags[i].substr(TAG_KEYS.CATEGORY.length, tags[i].length).replace('-', ' ');
        }
      }
    }
  }
};

export const getGameNameForUrlFromTags = (tags) => {
  if(!tags || tags === undefined) {
    return '';
  } else {
    for (var i=0; i <tags.length; i++) {
      if(tags[i]){
        if(tags[i].indexOf(TAG_KEYS.CATEGORY) === 0) {
          return ENV.location.protocol + '//' + (ENV.isAzubuDomain() ? ENV.location.hostname : 'www.azubu.tv') + '/games/' + tags[i].substr(TAG_KEYS.CATEGORY.length);
        }
      }
    }
  }
};

export const getChannelNameFromTags = (tags) => {
  if(!tags || tags === undefined) {
    return '';
  } else {
    for (var i=0; i <tags.length; i++) {
      if(tags[i]){
        if(tags[i].indexOf(TAG_KEYS.LIVE) === 0 || tags[i].indexOf(TAG_KEYS.VOD) === 0 || tags[i].indexOf(TAG_KEYS.TEAM) === 0) {
          return tags[i].substr(5, tags[i].length);
        }
      }
    }
  }
};

export const loadScriptWithCallback = (url, callback=null, errorCallback=null) => {
  try{
    var d = document, t = 'script', o = d.createElement(t), s = d.getElementsByTagName(t)[0];
    o.src = url;
    if(callback) {
      o.addEventListener('load', function(e) {
        callback(e);
      }, false);
    }
    if(errorCallback) {
      o.addEventListener('error', (e) => {
        errorCallback(e);
      });
    }
    s.parentNode.insertBefore(o, s);
  } catch (err) {
    console.warn('Cannot load script', url);
    console.warn(err.message);
    if(errorCallback){
      errorCallback();
    }
  }
};

export const setLocalStorageItem = (key, value) => {
  window.localStorage.setItem(key, value);

  return getLocalStorageItem(key);
};

export const getLocalStorageItem = (key) => {
  return window.localStorage.getItem(key);
};

export const getUserName = () => {
  return getLocalStorageItem(LOCAL_STORAGE_KEYS.USER_NAME) || 'Guest';
};

export const getUserID = () => {
  // TODO: Find User ID, put in LocalStoarge, refer/return here.
  return getLocalStorageItem(LOCAL_STORAGE_KEYS.USER_ID) || -1;
};

export const getChannelPageUrlByTags = (tags) => {
  return ENV.location.protocol + '//' + (ENV.isAzubuDomain() ? ENV.location.hostname : 'www.azubu.tv') + '/' + getChannelNameFromTags(tags)
};
