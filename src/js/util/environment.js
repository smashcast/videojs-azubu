import videojs from 'video.js';

videojs.IS_EDGE = (/Edge/i).test(window.navigator.userAgent);

export const HOSTS = {
  WEB: 'www.azubu.tv',
  API: 'api.azubu.tv',
  OVERLAY: {
    WEB: 'www.azubu.tv',
    S3: 's3-us-west-1.amazonaws.com'
  },
  EMBED: 'embed.azubu.tv',
  UOL: 'azubu.uol.com.br',
  STAGING: 'www.qa2.azubu.it',
  LOCAL: 'localhost'
};

const AZUBU_DOMAINS = [HOSTS.WEB, HOSTS.EMBED, HOSTS.STAGING];

const referrerLocation = document.createElement('a');
      referrerLocation.href = (window.top !== window.self) ? document.referrer : window.self.location.href;

export const location = referrerLocation;

export const isIFrame = () => {
  return (window.top !== window.self);
};

export const isUOL = () => {
  return (location.hostname.indexOf(HOSTS.UOL) !== -1);
};

export const isAzubuEmbed = () => {
  return isIFrame() && isAzubuDomain();
};

export const isAzubuDomain = () => {
  return (AZUBU_DOMAINS.indexOf(location.hostname) !== -1);
};

export const isExternalEmbed = () => {
  return isIFrame() && !isAzubuDomain();
};

export const isAzubuHomepage = () => {
  return isAzubuDomain() && (location.pathname === '/');
};

export const isAzubuChannelpage = () => {
  return (!isAzubuHomepage() && isAzubuDomain());
};

export const isAdvertisementWhitelistedUrl = () => {
  return isAzubuHomepage();
};

export const status = () => {
  let output = {
    location: {
      hostname: location.hostname,
      path: location.pathname,
      protocol: location.protocol,
      search: location.search,
      href: location.href
    },
    iframe: isIFrame(),
    azubuEmbed: isAzubuEmbed(),
    azubuDomain: isAzubuDomain(),
    azubuHomepage: isAzubuHomepage(),
    azubuChannelpage: isAzubuChannelpage(),
    externalEmbed: isExternalEmbed(),
    whitelisted: isAdvertisementWhitelistedUrl(),
    isUOL: isUOL()
  };
  return output;
};


