// Basic Media Events
/**
 * Abort
 * Fires when the loading of an audio/video is aborted
 * @type {string}
 */
export const ABORT = 'abort';

/**
 * Can Play
 * Fires when the browser can start playing the audio/video
 * @type {string}
 */
export const CAN_PLAY = 'canplay';

/**
 * Can Play Through
 * Fires when the browser can play through the audio/video without stopping for buffering
 * @type {string}
 */
export const CAN_PLAY_THROUGH = 'canplaythrough';

/**
 * Duration Change
 * Fires when the duration of the audio/video is changed
 * @type {string}
 */
export const DURATION_CHANGE = 'durationchange';

/**
 * Emptied
 * Fires when the current playlist is empty
 * @type {string}
 */
export const EMPTIED = 'emptied';

/**
 * Ended
 * Fires when the current playlist is ended
 * @type {string}
 */
export const ENDED = 'ended';

/**
 * Error
 * Fires when an error occurred during the loading of an audio/video
 * @type {string}
 */
export const ERROR = 'error'; //

/**
 * Loaded Data
 * Fires when the browser has loaded the current frame of the audio/video
 * @type {string}
 */
export const LOADED_DATA = 'loadeddata'; //

/**
 * Loaded Metadata
 * Fires when the browser has loaded meta data for the audio/video
 * @type {string}
 */
export const LOADED_METADATA = 'loadedmetadata'; //

/**
 * Load Start
 * Fires when the browser starts looking for the audio/video
 * @type {string}
 */
export const LOAD_START = 'loadstart'; //

/**
 * Pause
 * Fires when the audio/video has been paused
 * @type {string}
 */
export const PAUSE = 'pause'; //

/**
 * Play
 * Fires when the audio/video has been started or is no longer paused
 * @type {string}
 */
export const PLAY = 'play'; //

/**
 * Playing
 * Fires when the audio/video is playing after having been paused or stopped for buffering
 * @type {string}
 */
export const PLAYING = 'playing'; //

/**
 * Progress
 * Fires when the browser is downloading the audio/video
 * @type {string}
 */
export const PROGRESS = 'progress'; //

/**
 * Rate Change
 * Fires when the playing speed of the audio/video is changed
 * @type {string}
 */
export const RATE_CHANGE = 'ratechange'; //

/**
 * Seeked
 * Fires when the user is finished moving/skipping to a new position in the audio/video
 * @type {string}
 */
export const SEEKED = 'seeked'; //

/**
 * Seeking
 * Fires when the user starts moving/skipping to a new position in the audio/video
 * @type {string}
 */
export const SEEKING = 'seeking'; //

/**
 * Stalled
 * Fires when the browser is trying to get media data, but data is not available
 * @type {string}
 */
export const STALLED = 'stalled'; //

/**
 * Suspend
 * Fires when the browser is intentionally not getting media data
 * @type {string}
 */
export const SUSPEND = 'suspend'; //

/**
 * Time Update
 * Fires when the current playback position has changed
 * @type {string}
 */
export const TIME_UPDATE = 'timeupdate'; //

/**
 * Volume Change
 * Fires when the volume has been changed
 * @type {string}
 */
export const VOLUME_CHANGE = 'volumechange'; //

/**
 * Waiting
 * Fires when the video stops because it needs to buffer the next frame
 * @type {string}
 */
export const WAITING = 'waiting'; //

// Custom / VJS

/**
 * First Play [VJS]
 * Fires when the player plays a piece of media for the first time
 * @type {string}
 */
export const FIRST_PLAY = 'firstplay';

/**
 * Inactive [VJS]
 * Fires when the player user becomes inactive
 * @type {string}
 */
export const INACTIVE = 'userinactive';

/**
 * Active [VJS]
 * Fires when the player user becomes active
 * @type {string}
 */
export const ACTIVE = 'useractive';

/**
 * Ready [VJS]
 * Fires when the player is ready
 * @type {string}
 */
export const READY = 'ready';

/**
 * Reconnecting [Custom]
 * Fires when the player timeout error happens on a live media
 * @type {string}
 */
export const RECONNECTING = 'reconnecting';

/**
 * Reconnected [Custom]
 * Fires when the player recovers from a reconnecting attempt
 * @type {string}
 */
export const RECONNECTED = 'reconnected';

/**
 * Broadcaster Data Changed [Custom]
 * Fires when the player receives new broadcaster data from the AZUBU api
 * @type {string}
 */
export const BROADCASTER_DATA_CHANGE = 'broadcasterdatachange';

/**
 * User Data Changed [Custom]
 * Fires when the player receives new user data from the AZUBU api
 * @type {string}
 */
export const USER_DATA_CHANGE = 'userdatachange';

/**
 * Subscription Data Changed [Custom]
 * Fires when the player receives new subscription data from teh AZUBU api
 * @type {string}
 */
export const SUBSCRIPTION_DATA_CHANGE = 'subscriptiondatachange';

/**
 * Rendition Selected [Custom]
 * Fires when the user selects a new rendition
 * @type {string}
 */
export const RENDITION_SELECTED = 'renditionSelected';

/**
 * Auto Mode Selected [Custom]
 * Fires when the user selects auto mode
 * @type {string}
 */
export const AUTO_MODE_SELECTED = 'autoModeSelected';

// Ads Events
export const CUE_CHANGE = 'cuechange';
export const TRACK_ADDED = 'addtrack';
export const AD_START = 'adstart';
export const AD_END = 'adend';
export const AD_ERROR = 'aderror';
export const AD_TIMEOUT = 'adtimeout';
export const ADS_READY = 'adsready';

// Rendition Change Events
export const STREAM_SWITCH_START = 'streamswitchstart';
export const STREAM_SWITCH_END = 'streamswitchend';

// Share Screen Events
export const SHARE_OPEN = 'shareopen';
export const SHARE_CLOSE = 'shareclose';
