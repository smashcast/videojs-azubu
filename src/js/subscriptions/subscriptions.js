import * as API from '../communication/api';
import * as EVENTS from '../util/event';
import * as UTIL from '../util/util';

export const init = (player, options) => {

  let getChannelRestrictionsCallback = (data, status) => {
    if (status === 200 && data) {
      UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.RESTRICTIONS, JSON.stringify(data));
    }
  };

  let getUserNameCallback = (data, status) => {
    if (data.username && status === 200) {
      UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.USER_NAME, data.username);

      if(data.id) {
        UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.USER_ID, data.id);
      }

      player.trigger(EVENTS.USER_DATA_CHANGE, data);
      API.getUserSubscriptions(data.username, getUserSubscriptionsCallback);
    }
  };

  let getUserSubscriptionsCallback = (data, status) => {
    if (status === 200 && data) {

      if (Array.isArray(data)) {
        data.forEach(function (value, index) {
          data[index] = data[index].toLowerCase();
        });
      }

      UTIL.setLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.SUBSCRIPTIONS, JSON.stringify(data));
      player.trigger(EVENTS.SUBSCRIPTION_DATA_CHANGE, JSON.parse(UTIL.getLocalStorageItem(UTIL.LOCAL_STORAGE_KEYS.SUBSCRIPTIONS)));
    }
  };

  API.getChannelRestrictions(getChannelRestrictionsCallback);
  API.getUserName(getUserNameCallback);
};
