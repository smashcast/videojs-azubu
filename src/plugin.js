import videojs from 'video.js';

//import 'dashjs/dist/dash.all.debug';
//import 'videojs-contrib-dash/dist/videojs-dash';

import * as EVENTS from './js/util/event';
import * as ENV from './js/util/environment';
import * as SUBSCRIPTIONS from './js/subscriptions/subscriptions';
import * as RENDITIONS from './js/streaming/renditions/renditions';
import * as ANALYTICS from './js/analytics/analytics';
import * as ADS from './js/ads/ads';
import * as POST from './js/communication/postmessage';
import * as INTERFACE from './js/ui/ui';
import * as STREAMING from './js/streaming/streaming';
import * as RECONNECT from './js/reconnecting/reconnecting';

// Default options for the plugin.
const defaults = {
  NAMESPACE: 'azubu',
  VERSION: '9.9.9'
};

const handleFirstPlaying = (event) => {
  if(event.target.player.duration() === Infinity) {
    console.warn('LIVE SEEK?');
    event.target.player.currentTime(player.seekable().end(0)-5);
    event.target.player.off(EVENTS.PLAYING, handleFirstPlaying);
  }
};

/**
 * A video.js plugin.
 *
 * In the plugin function, the value of `this` is a video.js `Player`
 * instance. You cannot rely on the player being in a "ready" state here,
 * depending on how the plugin is invoked. This may or may not be important
 * to you; if not, remove the wait for "ready"!
 *
 * @function azubu
 * @param    {Object} [options={}]
 *           An object of options left to the plugin author to define.
 */
const azubu = function(options) {
  console.log('Status', ENV.status());

  let settings = videojs.mergeOptions(defaults, options);
  let player = this;

  // Init Ads
  if(ENV.status().isUOL) ADS.init(player, settings);

  // Init Streaming Rules
  STREAMING.init(player, settings);

  // Init Analytics
  ANALYTICS.init(player, settings);

  // Init Subscriptions
  SUBSCRIPTIONS.init(player, settings);

  // Init Rendition Selection Logic
  RENDITIONS.init(player, settings);

  // Post Message API
  POST.init(player, settings);

  // Init User Interface
  INTERFACE.init(player, settings);

  // Reconnect Settings
  // RECONNECT.init(player, settings);

  player.el().focus();

  videojs.IS_EDGE = (/Edge/i).test(window.navigator.userAgent);

  console.log('Everest:', settings.VERSION, player.controls(), player.autoplay());

  if(player.options_['data-setup']) {
    let setup = JSON.parse(player.options_['data-setup']);
    player.controls(setup.controls);
    player.autoplay(setup.autoplay);
  }

  //player.one(EVENTS.PLAYING, handleFirstPlaying);

};

// Register the plugin with video.js.
videojs.plugin(defaults.NAMESPACE, azubu);

export default azubu;
