/**
 * mux-embed
 * @version 1.0.0
 * @copyright 2016 [object Object]
 * @license
 */
!function e(t, r, n) {
  function i(a, l) {
    if (!r[a]) {
      if (!t[a]) {
        var s = "function" == typeof require && require;
        if (!l && s)return s(a, !0);
        if (o)return o(a, !0);
        throw new Error("Cannot find module '" + a + "'")
      }
      var c = r[a] = {exports: {}};
      t[a][0].call(c.exports, function (e) {
        var r = t[a][1][e];
        return i(r ? r : e)
      }, c, c.exports, e, t, r, n)
    }
    return r[a].exports
  }

  for (var o = "function" == typeof require && require, a = 0; a < n.length; a++)i(n[a]);
  return i
}({
  1: [function (e, t, r) {
    "use strict";
    function n(e, t) {
      if (e && !s()) {
        var r = i() || {}, n = {};
        if (!t || "object" != typeof t)return x.error("[mux] A data object was expected in sendEvent but was not provided");
        if (isNaN(parseInt(t.viewer_time, 10)))return x.error("[mux] A timestamp did not exist in the data object");
        "pageloadstart" === e && (A.page_load_start = t.viewer_time), h(n, A), h(n, t), h(n, r), n.event = e, n.user_id && (n.viewer_user_id = n.user_id, delete n.user_id), n = v(n), x.debug("Sending Event: ", e, n), g.track("https://img.litix.io/a.gif", e, n)
      }
    }

    function i(e) {
      var t, r = (new Date).getTime();
      try {
        t = b.parse(y.get("muxData") || "")
      } catch (n) {
      }
      return t = t || {}, function () {
        var e = {uid: "mux_viewer_id", sid: "session_id", sexp: "session_expires", sstart: "session_start"};
        for (var r in e)t[r] && (t[e[r]] || (t[e[r]] = t[r]), delete t[r]);
        t.timestamp && delete t.timestamp
      }(), t.mux_viewer_id = t.mux_viewer_id || l(), (!t.session_expires || t.session_expires < r) && (t.session_id = l(), t.session_start = r), t.session_expires = r + 15e5, e && h(t, e), y.set("muxData", b.stringify(t), {expires: 7300}), t
    }

    function o(e) {
      a("property_key", e)
    }

    function a(e, t, r) {
      "debug" === e ? t ? x.setLevel("debug") : x.setLevel("warn") : A[e] = t
    }

    function l() {
      return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (e) {
        var t = 16 * Math.random() | 0, r = "x" === e ? t : 3 & t | 8;
        return r.toString(16)
      })
    }

    function s() {
      var e = p.navigator, t = p.doNotTrack || e.doNotTrack || e.msDoNotTrack;
      return "1" === t
    }

    function c(e, t) {
      e = Array.prototype.slice.apply(e), t = t || (new Date).getTime();
      var r, n = e[e.length - 1];
      return "object" != typeof n ? (e.push({viewer_time: t}), e) : (n.viewer_time || (r = h({}, n), r.viewer_time = t, e[e.length - 1] = r), e)
    }

    var u = "1.0.0", f = "0.2", p = e("global/window"), d = e("platform"), b = e("qs"), y = e("./vendor/js-cookie.js"), v = e("./minify-keys.js"), g = e("./utils/image-tracker.js"), m = e("lodash.isarray"), h = e("lodash.assign"), x = e("loglevel"), w = p.MUX_VAR_NAME || "mux", S = p[w];
    if (S && S.loaded)return void x.warn("[mux] Embed loaded more than once.");
    var O = {send: n, set: a, create: o}, j = p[w] = function (e) {
      var t = arguments, r = (new Date).getTime();
      "string" == typeof e ? O.hasOwnProperty(e) && p.setTimeout(function () {
        t = Array.prototype.splice.call(t, 1), t = c(t, r), O[e].apply(null, t)
      }, 0) : "function" == typeof e ? p.setTimeout(function () {
        e.call(null, j)
      }, 0) : x.warn("[mux] `" + e + "` is invalid.")
    };
    j.loaded = (new Date).getTime(), j.VERSION = u, j.API_VERSION = f;
    var A = {
      mux_api_version: f,
      mux_embed_version: u,
      page_url: p.location.href,
      viewer_application_name: d.name,
      viewer_application_version: d.version,
      viewer_application_engine: d.layout,
      viewer_device_name: d.product,
      viewer_device_category: "",
      viewer_device_manufacturer: d.manufacturer,
      viewer_os_family: d.os.family,
      viewer_os_architecture: d.os.architecture,
      viewer_os_version: d.os.version
    };
    !function () {
      var e, t, r = S && S.q;
      if (m(r))for (var n = 0; n < r.length; n++)m(r[n]) && (e = r[n][0], t = r[n][1], e.length && (e = c(e, t), j.apply(null, e)))
    }(), t.exports = j
  }, {
    "./minify-keys.js": 2,
    "./utils/image-tracker.js": 3,
    "./vendor/js-cookie.js": 4,
    "global/window": 5,
    "lodash.assign": 6,
    "lodash.isarray": 8,
    loglevel: 10,
    platform: 11,
    qs: 12
  }], 2: [function (e, t, r) {
    function n(e) {
      var t = {};
      for (var r in e)e.hasOwnProperty(r) && (t[e[r]] = r);
      return t
    }

    var i = e("loglevel"), o = {
      a: "property",
      d: "ad",
      e: "event",
      m: "mux",
      p: "player",
      s: "session",
      t: "timestamp",
      u: "viewer",
      v: "video",
      w: "page",
      x: "view"
    }, a = n(o), l = {
      ap: "api",
      al: "application",
      ar: "architecture",
      au: "autoplay",
      bu: "buffering",
      co: "count",
      cd: "code",
      cg: "category",
      cn: "config",
      ct: "content",
      cu: "current",
      du: "duration",
      dv: "device",
      ec: "encoding",
      en: "end",
      eg: "engine",
      em: "embed",
      er: "error",
      ev: "events",
      ex: "expires",
      fi: "first",
      fm: "family",
      fr: "frame",
      fs: "fullscreen",
      ht: "height",
      id: "id",
      "in": "instance",
      ip: "ip",
      is: "is",
      ke: "key",
      la: "language",
      lo: "load",
      me: "message",
      mn: "manufacturer",
      mx: "mux",
      nm: "name",
      os: "os",
      pa: "paused",
      pb: "playback",
      pd: "producer",
      pi: "plugin",
      po: "poster",
      pr: "preload",
      ra: "rate",
      ro: "ratio",
      se: "session",
      sm: "stream",
      sr: "series",
      st: "start",
      so: "source",
      sw: "software",
      ti: "time",
      to: "to",
      tt: "title",
      ty: "type",
      ur: "url",
      us: "user",
      va: "variant",
      vi: "video",
      ve: "version",
      vw: "view",
      vr: "viewer",
      wd: "width",
      wa: "watch"
    }, s = n(l);
    t.exports = function (e) {
      var t = {};
      for (var r in e)if (e.hasOwnProperty(r)) {
        var n = r.split("_"), o = n[0], l = a[o];
        l || (i.warn("[mux] Data key word `" + n[0] + "` not expected in " + r), l = o + "_"), n.splice(1).forEach(function (e) {
          s[e] ? l += s[e] : (i.warn("[mux] Data key word `" + e + "` not expected " + r), l += "_" + e + "_")
        }), t[l] = e[r]
      }
      return t
    }
  }, {loglevel: 10}], 3: [function (e, t, r) {
    var n = e("qs"), i = {};
    i.track = function (e, t, r) {
      var i = new Image;
      return i.src = e + "?e=" + t + "&" + n.stringify(r), i
    }, t.exports = i
  }, {qs: 12}], 4: [function (e, t, r) {
    !function (e) {
      t.exports = e()
    }(function () {
      function e() {
        for (var e = 0, t = {}; e < arguments.length; e++) {
          var r = arguments[e];
          for (var n in r)t[n] = r[n]
        }
        return t
      }

      function t(r) {
        function n(t, i, o) {
          var a;
          if (arguments.length > 1) {
            if (o = e({path: "/"}, n.defaults, o), "number" == typeof o.expires) {
              var l = new Date;
              l.setMilliseconds(l.getMilliseconds() + 864e5 * o.expires), o.expires = l
            }
            try {
              a = JSON.stringify(i), /^[\{\[]/.test(a) && (i = a)
            } catch (s) {
            }
            return i = r.write ? r.write(i, t) : encodeURIComponent(String(i)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent), t = encodeURIComponent(String(t)), t = t.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent), t = t.replace(/[\(\)]/g, escape), document.cookie = [t, "=", i, o.expires && "; expires=" + o.expires.toUTCString(), o.path && "; path=" + o.path, o.domain && "; domain=" + o.domain, o.secure ? "; secure" : ""].join("")
          }
          t || (a = {});
          for (var c = document.cookie ? document.cookie.split("; ") : [], u = /(%[0-9A-Z]{2})+/g, f = 0; f < c.length; f++) {
            var p = c[f].split("="), d = p[0].replace(u, decodeURIComponent), b = p.slice(1).join("=");
            '"' === b.charAt(0) && (b = b.slice(1, -1));
            try {
              if (b = r.read ? r.read(b, d) : r(b, d) || b.replace(u, decodeURIComponent), this.json)try {
                b = JSON.parse(b)
              } catch (s) {
              }
              if (t === d) {
                a = b;
                break
              }
              t || (a[d] = b)
            } catch (s) {
            }
          }
          return a
        }

        return n.get = n.set = n, n.getJSON = function () {
          return n.apply({json: !0}, [].slice.call(arguments))
        }, n.defaults = {}, n.remove = function (t, r) {
          n(t, "", e(r, {expires: -1}))
        }, n.withConverter = t, n
      }

      return t(function () {
      })
    })
  }, {}], 5: [function (e, t, r) {
    (function (e) {
      "undefined" != typeof window ? t.exports = window : "undefined" != typeof e ? t.exports = e : "undefined" != typeof self ? t.exports = self : t.exports = {}
    }).call(this, "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
  }, {}], 6: [function (e, t, r) {
    function n(e, t) {
      return e = "number" == typeof e || x.test(e) ? +e : -1, t = null == t ? g : t, e > -1 && e % 1 == 0 && t > e
    }

    function i(e, t, r) {
      var n = e[t];
      (!u(n, r) || u(n, w[t]) && !S.call(e, t) || void 0 === r && !(t in e)) && (e[t] = r)
    }

    function o(e) {
      return function (t) {
        return null == t ? void 0 : t[e]
      }
    }

    function a(e, t, r) {
      return l(e, t, r)
    }

    function l(e, t, r, n) {
      r || (r = {});
      for (var o = -1, a = t.length; ++o < a;) {
        var l = t[o], s = n ? n(r[l], e[l], l, r, e) : e[l];
        i(r, l, s)
      }
      return r
    }

    function s(e) {
      return v(function (t, r) {
        var n = -1, i = r.length, o = i > 1 ? r[i - 1] : void 0, a = i > 2 ? r[2] : void 0;
        for (o = "function" == typeof o ? (i--, o) : void 0, a && c(r[0], r[1], a) && (o = 3 > i ? void 0 : o, i = 1), t = Object(t); ++n < i;) {
          var l = r[n];
          l && e(t, l, n, o)
        }
        return t
      })
    }

    function c(e, t, r) {
      if (!b(r))return !1;
      var i = typeof t;
      return ("number" == i ? f(r) && n(t, r.length) : "string" == i && t in r) ? u(r[t], e) : !1
    }

    function u(e, t) {
      return e === t || e !== e && t !== t
    }

    function f(e) {
      return null != e && !("function" == typeof e && p(e)) && d(j(e))
    }

    function p(e) {
      var t = b(e) ? O.call(e) : "";
      return t == m || t == h
    }

    function d(e) {
      return "number" == typeof e && e > -1 && e % 1 == 0 && g >= e
    }

    function b(e) {
      var t = typeof e;
      return !!e && ("object" == t || "function" == t)
    }

    var y = e("lodash.keys"), v = e("lodash.rest"), g = 9007199254740991, m = "[object Function]", h = "[object GeneratorFunction]", x = /^(?:0|[1-9]\d*)$/, w = Object.prototype, S = w.hasOwnProperty, O = w.toString, j = o("length"), A = s(function (e, t) {
      a(t, y(t), e)
    });
    t.exports = A
  }, {"lodash.keys": 7, "lodash.rest": 9}], 7: [function (e, t, r) {
    function n(e, t) {
      for (var r = -1, n = Array(e); ++r < e;)n[r] = t(r);
      return n
    }

    function i(e, t) {
      return e = "number" == typeof e || j.test(e) ? +e : -1, t = null == t ? h : t, e > -1 && e % 1 == 0 && t > e
    }

    function o(e, t) {
      return k.call(e, t) || "object" == typeof e && t in e && null === E(e)
    }

    function a(e) {
      return M(Object(e))
    }

    function l(e) {
      return function (t) {
        return null == t ? void 0 : t[e]
      }
    }

    function s(e) {
      var t = e ? e.length : void 0;
      return b(t) && (I(e) || g(e) || u(e)) ? n(t, String) : null
    }

    function c(e) {
      var t = e && e.constructor, r = "function" == typeof t && t.prototype || A;
      return e === r
    }

    function u(e) {
      return p(e) && k.call(e, "callee") && (!_.call(e, "callee") || P.call(e) == x)
    }

    function f(e) {
      return null != e && !("function" == typeof e && d(e)) && b(C(e))
    }

    function p(e) {
      return v(e) && f(e)
    }

    function d(e) {
      var t = y(e) ? P.call(e) : "";
      return t == w || t == S
    }

    function b(e) {
      return "number" == typeof e && e > -1 && e % 1 == 0 && h >= e
    }

    function y(e) {
      var t = typeof e;
      return !!e && ("object" == t || "function" == t)
    }

    function v(e) {
      return !!e && "object" == typeof e
    }

    function g(e) {
      return "string" == typeof e || !I(e) && v(e) && P.call(e) == O
    }

    function m(e) {
      var t = c(e);
      if (!t && !f(e))return a(e);
      var r = s(e), n = !!r, l = r || [], u = l.length;
      for (var p in e)!o(e, p) || n && ("length" == p || i(p, u)) || t && "constructor" == p || l.push(p);
      return l
    }

    var h = 9007199254740991, x = "[object Arguments]", w = "[object Function]", S = "[object GeneratorFunction]", O = "[object String]", j = /^(?:0|[1-9]\d*)$/, A = Object.prototype, k = A.hasOwnProperty, P = A.toString, E = Object.getPrototypeOf, _ = A.propertyIsEnumerable, M = Object.keys, C = l("length"), I = Array.isArray;
    t.exports = m
  }, {}], 8: [function (e, t, r) {
    var n = Array.isArray;
    t.exports = n
  }, {}], 9: [function (e, t, r) {
    function n(e, t, r) {
      var n = r.length;
      switch (n) {
        case 0:
          return e.call(t);
        case 1:
          return e.call(t, r[0]);
        case 2:
          return e.call(t, r[0], r[1]);
        case 3:
          return e.call(t, r[0], r[1], r[2])
      }
      return e.apply(t, r)
    }

    function i(e, t) {
      if ("function" != typeof e)throw new TypeError(c);
      return t = S(void 0 === t ? e.length - 1 : l(t), 0), function () {
        for (var r = arguments, i = -1, o = S(r.length - t, 0), a = Array(o); ++i < o;)a[i] = r[t + i];
        switch (t) {
          case 0:
            return e.call(this, a);
          case 1:
            return e.call(this, r[0], a);
          case 2:
            return e.call(this, r[0], r[1], a)
        }
        var l = Array(t + 1);
        for (i = -1; ++i < t;)l[i] = r[i];
        return l[t] = a, n(e, this, l)
      }
    }

    function o(e) {
      var t = a(e) ? w.call(e) : "";
      return t == d || t == b
    }

    function a(e) {
      var t = typeof e;
      return !!e && ("object" == t || "function" == t)
    }

    function l(e) {
      if (!e)return 0 === e ? e : 0;
      if (e = s(e), e === u || e === -u) {
        var t = 0 > e ? -1 : 1;
        return t * f
      }
      var r = e % 1;
      return e === e ? r ? e - r : e : 0
    }

    function s(e) {
      if (a(e)) {
        var t = o(e.valueOf) ? e.valueOf() : e;
        e = a(t) ? t + "" : t
      }
      if ("string" != typeof e)return 0 === e ? e : +e;
      e = e.replace(y, "");
      var r = g.test(e);
      return r || m.test(e) ? h(e.slice(2), r ? 2 : 8) : v.test(e) ? p : +e
    }

    var c = "Expected a function", u = 1 / 0, f = 1.7976931348623157e308, p = NaN, d = "[object Function]", b = "[object GeneratorFunction]", y = /^\s+|\s+$/g, v = /^[-+]0x[0-9a-f]+$/i, g = /^0b[01]+$/i, m = /^0o[0-7]+$/i, h = parseInt, x = Object.prototype, w = x.toString, S = Math.max;
    t.exports = i
  }, {}], 10: [function (e, t, r) {
    !function (r, n) {
      "use strict";
      "object" == typeof t && t.exports && "function" == typeof e ? t.exports = n() : "function" == typeof define && "object" == typeof define.amd ? define(n) : r.log = n()
    }(this, function () {
      "use strict";
      function e(e) {
        return typeof console === l ? !1 : void 0 !== console[e] ? t(console, e) : void 0 !== console.log ? t(console, "log") : a
      }

      function t(e, t) {
        var r = e[t];
        if ("function" == typeof r.bind)return r.bind(e);
        try {
          return Function.prototype.bind.call(r, e)
        } catch (n) {
          return function () {
            return Function.prototype.apply.apply(r, [e, arguments])
          }
        }
      }

      function r(e, t, r) {
        return function () {
          typeof console !== l && (n.call(this, t, r), this[e].apply(this, arguments))
        }
      }

      function n(e, t) {
        for (var r = 0; r < s.length; r++) {
          var n = s[r];
          this[n] = e > r ? a : this.methodFactory(n, e, t)
        }
      }

      function i(t, n, i) {
        return e(t) || r.apply(this, arguments)
      }

      function o(e, t, r) {
        function o(e) {
          var t = (s[e] || "silent").toUpperCase();
          try {
            return void(window.localStorage[f] = t)
          } catch (r) {
          }
          try {
            window.document.cookie = encodeURIComponent(f) + "=" + t + ";"
          } catch (r) {
          }
        }

        function a() {
          var e;
          try {
            e = window.localStorage[f]
          } catch (t) {
          }
          if (typeof e === l)try {
            var r = window.document.cookie, n = r.indexOf(encodeURIComponent(f) + "=");
            n && (e = /^([^;]+)/.exec(r.slice(n))[1])
          } catch (t) {
          }
          return void 0 === u.levels[e] && (e = void 0), e
        }

        var c, u = this, f = "loglevel";
        e && (f += ":" + e), u.levels = {
          TRACE: 0,
          DEBUG: 1,
          INFO: 2,
          WARN: 3,
          ERROR: 4,
          SILENT: 5
        }, u.methodFactory = r || i, u.getLevel = function () {
          return c
        }, u.setLevel = function (t, r) {
          if ("string" == typeof t && void 0 !== u.levels[t.toUpperCase()] && (t = u.levels[t.toUpperCase()]), !("number" == typeof t && t >= 0 && t <= u.levels.SILENT))throw"log.setLevel() called with invalid level: " + t;
          return c = t, r !== !1 && o(t), n.call(u, t, e), typeof console === l && t < u.levels.SILENT ? "No console available for logging" : void 0
        }, u.setDefaultLevel = function (e) {
          a() || u.setLevel(e, !1)
        }, u.enableAll = function (e) {
          u.setLevel(u.levels.TRACE, e)
        }, u.disableAll = function (e) {
          u.setLevel(u.levels.SILENT, e)
        };
        var p = a();
        null == p && (p = null == t ? "WARN" : t), u.setLevel(p, !1)
      }

      var a = function () {
      }, l = "undefined", s = ["trace", "debug", "info", "warn", "error"], c = new o, u = {};
      c.getLogger = function (e) {
        if ("string" != typeof e || "" === e)throw new TypeError("You must supply a name when creating a logger.");
        var t = u[e];
        return t || (t = u[e] = new o(e, c.getLevel(), c.methodFactory)), t
      };
      var f = typeof window !== l ? window.log : void 0;
      return c.noConflict = function () {
        return typeof window !== l && window.log === c && (window.log = f), c
      }, c
    })
  }, {}], 11: [function (e, t, r) {
    (function (e) {
      (function () {
        "use strict";
        function n(e) {
          return e = String(e), e.charAt(0).toUpperCase() + e.slice(1)
        }

        function i(e, t, r) {
          var n = {
            6.4: "10",
            6.3: "8.1",
            6.2: "8",
            6.1: "Server 2008 R2 / 7",
            "6.0": "Server 2008 / Vista",
            5.2: "Server 2003 / XP 64-bit",
            5.1: "XP",
            5.01: "2000 SP1",
            "5.0": "2000",
            "4.0": "NT",
            "4.90": "ME"
          };
          return t && r && /^Win/i.test(e) && (n = n[/[\d.]+$/.exec(e)]) && (e = "Windows " + n), e = String(e), t && r && (e = e.replace(RegExp(t, "i"), r)), e = a(e.replace(/ ce$/i, " CE").replace(/\bhpw/i, "web").replace(/\bMacintosh\b/, "Mac OS").replace(/_PowerPC\b/i, " OS").replace(/\b(OS X) [^ \d]+/i, "$1").replace(/\bMac (OS X)\b/, "$1").replace(/\/(\d)/, " $1").replace(/_/g, ".").replace(/(?: BePC|[ .]*fc[ \d.]+)$/i, "").replace(/\bx86\.64\b/gi, "x86_64").replace(/\b(Windows Phone) OS\b/, "$1").split(" on ")[0])
        }

        function o(e, t) {
          var r = -1, n = e ? e.length : 0;
          if ("number" == typeof n && n > -1 && x >= n)for (; ++r < n;)t(e[r], r, e); else l(e, t)
        }

        function a(e) {
          return e = p(e), /^(?:webOS|i(?:OS|P))/.test(e) ? e : n(e)
        }

        function l(e, t) {
          for (var r in e)j.call(e, r) && t(e[r], r, e)
        }

        function s(e) {
          return null == e ? n(e) : A.call(e).slice(8, -1)
        }

        function c(e, t) {
          var r = null != e ? typeof e[t] : "number";
          return !/^(?:boolean|number|string|undefined)$/.test(r) && ("object" == r ? !!e[t] : !0)
        }

        function u(e) {
          return String(e).replace(/([ -])(?!$)/g, "$1?")
        }

        function f(e, t) {
          var r = null;
          return o(e, function (n, i) {
            r = t(r, n, i, e)
          }), r
        }

        function p(e) {
          return String(e).replace(/^ +| +$/g, "")
        }

        function d(e) {
          function t(t) {
            return f(t, function (t, r) {
              return t || RegExp("\\b" + (r.pattern || u(r)) + "\\b", "i").exec(e) && (r.label || r)
            })
          }

          function r(t) {
            return f(t, function (t, r, n) {
              return t || (r[z] || r[/^[a-z]+(?: +[a-z]+\b)*/i.exec(z)] || RegExp("\\b" + u(n) + "(?:\\b|\\w*\\d)", "i").exec(e)) && n
            })
          }

          function n(t) {
            return f(t, function (t, r) {
              return t || RegExp("\\b" + (r.pattern || u(r)) + "\\b", "i").exec(e) && (r.label || r)
            })
          }

          function o(t) {
            return f(t, function (t, r) {
              var n = r.pattern || u(r);
              return !t && (t = RegExp("\\b" + n + "(?:/[\\d.]+|[ \\w.]*)", "i").exec(e)) && (t = i(t, n, r.label || r)), t
            })
          }

          function b(t) {
            return f(t, function (t, r) {
              var n = r.pattern || u(r);
              return !t && (t = RegExp("\\b" + n + " *\\d+[.\\w_]*", "i").exec(e) || RegExp("\\b" + n + "(?:; *(?:[a-z]+[_-])?[a-z]+\\d+|[^ ();-]*)", "i").exec(e)) && ((t = String(r.label && !RegExp(n, "i").test(r.label) ? r.label : t).split("/"))[1] && !/[\d.]+/.test(t[0]) && (t[0] += " " + t[1]), r = r.label || r, t = a(t[0].replace(RegExp(n, "i"), r).replace(RegExp("; *(?:" + r + "[_-])?", "i"), " ").replace(RegExp("(" + r + ")[-_.]?(\\w)", "i"), "$1 $2"))), t
            })
          }

          function g(t) {
            return f(t, function (t, r) {
              return t || (RegExp(r + "(?:-[\\d.]+/|(?: for [\\w-]+)?[ /-])([\\d.]+[^ ();/_-]*)", "i").exec(e) || 0)[1] || null
            })
          }

          function m() {
            return this.description || ""
          }

          var h = y, x = e && "object" == typeof e && "String" != s(e);
          x && (h = e, e = null);
          var O = h.navigator || {}, j = O.userAgent || "";
          e || (e = j);
          var k, P, E = x || S == v, _ = x ? !!O.likeChrome : /\bChrome\b/.test(e) && !/internal|\n/i.test(A.toString()), M = "Object", C = x ? M : "ScriptBridgingProxyObject", I = x ? M : "Environment", R = x && h.java ? "JavaPackage" : s(h.java), N = x ? M : "RuntimeObject", T = /\bJava/.test(R) && h.java, B = T && s(h.environment) == I, F = T ? "a" : "α", W = T ? "b" : "β", L = h.document || {}, $ = h.operamini || h.opera, D = w.test(D = x && $ ? $["[[Class]]"] : s($)) ? D : $ = null, G = e, U = [], X = null, K = e == j, H = K && $ && "function" == typeof $.version && $.version(), V = t(["Trident", {
            label: "WebKit",
            pattern: "AppleWebKit"
          }, "iCab", "Presto", "NetFront", "Tasman", "KHTML", "Gecko"]), q = n(["Adobe AIR", "Arora", "Avant Browser", "Breach", "Camino", "Epiphany", "Fennec", "Flock", "Galeon", "GreenBrowser", "iCab", "Iceweasel", {
            label: "SRWare Iron",
            pattern: "Iron"
          }, "K-Meleon", "Konqueror", "Lunascape", "Maxthon", "Midori", "Nook Browser", "PhantomJS", "Raven", "Rekonq", "RockMelt", "SeaMonkey", {
            label: "Silk",
            pattern: "(?:Cloud9|Silk-Accelerated)"
          }, "Sleipnir", "SlimBrowser", "Sunrise", "Swiftfox", "WebPositive", "Opera Mini", {
            label: "Opera Mini",
            pattern: "OPiOS"
          }, "Opera", {label: "Opera", pattern: "OPR"}, "Chrome", {
            label: "Chrome Mobile",
            pattern: "(?:CriOS|CrMo)"
          }, {label: "Firefox", pattern: "(?:Firefox|Minefield)"}, {label: "IE", pattern: "IEMobile"}, {
            label: "IE",
            pattern: "MSIE"
          }, "Safari"]), z = b([{label: "BlackBerry", pattern: "BB10"}, "BlackBerry", {
            label: "Galaxy S",
            pattern: "GT-I9000"
          }, {label: "Galaxy S2", pattern: "GT-I9100"}, {label: "Galaxy S3", pattern: "GT-I9300"}, {
            label: "Galaxy S4",
            pattern: "GT-I9500"
          }, "Google TV", "Lumia", "iPad", "iPod", "iPhone", "Kindle", {
            label: "Kindle Fire",
            pattern: "(?:Cloud9|Silk-Accelerated)"
          }, "Nook", "PlayBook", "PlayStation 4", "PlayStation 3", "PlayStation Vita", "TouchPad", "Transformer", {
            label: "Wii U",
            pattern: "WiiU"
          }, "Wii", "Xbox One", {label: "Xbox 360", pattern: "Xbox"}, "Xoom"]), J = r({
            Apple: {
              iPad: 1,
              iPhone: 1,
              iPod: 1
            },
            Amazon: {Kindle: 1, "Kindle Fire": 1},
            Asus: {Transformer: 1},
            "Barnes & Noble": {Nook: 1},
            BlackBerry: {PlayBook: 1},
            Google: {"Google TV": 1},
            HP: {TouchPad: 1},
            HTC: {},
            LG: {},
            Microsoft: {Xbox: 1, "Xbox One": 1},
            Motorola: {Xoom: 1},
            Nintendo: {"Wii U": 1, Wii: 1},
            Nokia: {Lumia: 1},
            Samsung: {"Galaxy S": 1, "Galaxy S2": 1, "Galaxy S3": 1, "Galaxy S4": 1},
            Sony: {"PlayStation 4": 1, "PlayStation 3": 1, "PlayStation Vita": 1}
          }), Z = o(["Windows Phone ", "Android", "CentOS", "Debian", "Fedora", "FreeBSD", "Gentoo", "Haiku", "Kubuntu", "Linux Mint", "Red Hat", "SuSE", "Ubuntu", "Xubuntu", "Cygwin", "Symbian OS", "hpwOS", "webOS ", "webOS", "Tablet OS", "Linux", "Mac OS X", "Macintosh", "Mac", "Windows 98;", "Windows "]);
          if (V && (V = [V]), J && !z && (z = b([J])), (k = /\bGoogle TV\b/.exec(z)) && (z = k[0]), /\bSimulator\b/i.test(e) && (z = (z ? z + " " : "") + "Simulator"), "Opera Mini" == q && /\bOPiOS\b/.test(e) && U.push("running in Turbo/Uncompressed mode"), /^iP/.test(z) ? (q || (q = "Safari"), Z = "iOS" + ((k = / OS ([\d_]+)/i.exec(e)) ? " " + k[1].replace(/_/g, ".") : "")) : "Konqueror" != q || /buntu/i.test(Z) ? J && "Google" != J && (/Chrome/.test(q) && !/\bMobile Safari\b/i.test(e) || /\bVita\b/.test(z)) ? (q = "Android Browser", Z = /\bAndroid\b/.test(Z) ? Z : "Android") : (!q || (k = !/\bMinefield\b|\(Android;/i.test(e) && /\b(?:Firefox|Safari)\b/.exec(q))) && (q && !z && /[\/,]|^[^(]+?\)/.test(e.slice(e.indexOf(k + "/") + 8)) && (q = null), (k = z || J || Z) && (z || J || /\b(?:Android|Symbian OS|Tablet OS|webOS)\b/.test(Z)) && (q = /[a-z]+(?: Hat)?/i.exec(/\bAndroid\b/.test(Z) ? Z : k) + " Browser")) : Z = "Kubuntu", (k = /\((Mobile|Tablet).*?Firefox\b/i.exec(e)) && k[1] && (Z = "Firefox OS", z || (z = k[1])), H || (H = g(["(?:Cloud9|CriOS|CrMo|IEMobile|Iron|Opera ?Mini|OPiOS|OPR|Raven|Silk(?!/[\\d.]+$))", "Version", u(q), "(?:Firefox|Minefield|NetFront)"])), "iCab" == V && parseFloat(H) > 3 ? V = ["WebKit"] : "Trident" != V && (k = /\bOpera\b/.test(q) && (/\bOPR\b/.test(e) ? "Blink" : "Presto") || /\b(?:Midori|Nook|Safari)\b/i.test(e) && "WebKit" || !V && /\bMSIE\b/i.test(e) && ("Mac OS" == Z ? "Tasman" : "Trident")) ? V = [k] : /\bPlayStation\b(?! Vita\b)/i.test(q) && "WebKit" == V && (V = ["NetFront"]), "IE" == q && (k = (/; *(?:XBLWP|ZuneWP)(\d+)/i.exec(e) || 0)[1]) ? (q += " Mobile", Z = "Windows Phone " + (/\+$/.test(k) ? k : k + ".x"), U.unshift("desktop mode")) : /\bWPDesktop\b/i.test(e) ? (q = "IE Mobile", Z = "Windows Phone 8+", U.unshift("desktop mode"), H || (H = (/\brv:([\d.]+)/.exec(e) || 0)[1])) : "IE" != q && "Trident" == V && (k = /\brv:([\d.]+)/.exec(e)) ? (/\bWPDesktop\b/i.test(e) || (q && U.push("identifying as " + q + (H ? " " + H : "")), q = "IE"), H = k[1]) : "Chrome" != q && "IE" == q || !(k = /\bEdge\/([\d.]+)/.exec(e)) || (q = "Microsoft Edge", H = k[1], V = ["Trident"]), K) {
            if (c(h, "global"))if (T && (k = T.lang.System, G = k.getProperty("os.arch"), Z = Z || k.getProperty("os.name") + " " + k.getProperty("os.version")), E && c(h, "system") && (k = [h.system])[0]) {
              Z || (Z = k[0].os || null);
              try {
                k[1] = h.require("ringo/engine").version, H = k[1].join("."), q = "RingoJS"
              } catch (Y) {
                k[0].global.system == h.system && (q = "Narwhal")
              }
            } else"object" == typeof h.process && (k = h.process) ? (q = "Node.js", G = k.arch, Z = k.platform, H = /[\d.]+/.exec(k.version)[0]) : B && (q = "Rhino"); else s(k = h.runtime) == C ? (q = "Adobe AIR", Z = k.flash.system.Capabilities.os) : s(k = h.phantom) == N ? (q = "PhantomJS", H = (k = k.version || null) && k.major + "." + k.minor + "." + k.patch) : "number" == typeof L.documentMode && (k = /\bTrident\/(\d+)/i.exec(e)) && (H = [H, L.documentMode], (k = +k[1] + 4) != H[1] && (U.push("IE " + H[1] + " mode"), V && (V[1] = ""), H[1] = k), H = "IE" == q ? String(H[1].toFixed(1)) : H[0]);
            Z = Z && a(Z)
          }
          H && (k = /(?:[ab]|dp|pre|[ab]\d+pre)(?:\d+\+?)?$/i.exec(H) || /(?:alpha|beta)(?: ?\d)?/i.exec(e + ";" + (K && O.appMinorVersion)) || /\bMinefield\b/i.test(e) && "a") && (X = /b/i.test(k) ? "beta" : "alpha", H = H.replace(RegExp(k + "\\+?$"), "") + ("beta" == X ? W : F) + (/\d+\+?/.exec(k) || "")), "Fennec" == q || "Firefox" == q && /\b(?:Android|Firefox OS)\b/.test(Z) ? q = "Firefox Mobile" : "Maxthon" == q && H ? H = H.replace(/\.[\d.]+/, ".x") : "Silk" == q ? (/\bMobi/i.test(e) || (Z = "Android", U.unshift("desktop mode")), /Accelerated *= *true/i.test(e) && U.unshift("accelerated")) : /\bXbox\b/i.test(z) ? (Z = null, "Xbox 360" == z && /\bIEMobile\b/.test(e) && U.unshift("mobile mode")) : !/^(?:Chrome|IE|Opera)$/.test(q) && (!q || z || /Browser|Mobi/.test(q)) || "Windows CE" != Z && !/Mobi/i.test(e) ? "IE" == q && K && null === h.external ? U.unshift("platform preview") : (/\bBlackBerry\b/.test(z) || /\bBB10\b/.test(e)) && (k = (RegExp(z.replace(/ +/g, " *") + "/([.\\d]+)", "i").exec(e) || 0)[1] || H) ? (k = [k, /BB10/.test(e)], Z = (k[1] ? (z = null, J = "BlackBerry") : "Device Software") + " " + k[0], H = null) : this != l && "Wii" != z && (K && $ || /Opera/.test(q) && /\b(?:MSIE|Firefox)\b/i.test(e) || "Firefox" == q && /\bOS X (?:\d+\.){2,}/.test(Z) || "IE" == q && (Z && !/^Win/.test(Z) && H > 5.5 || /\bWindows XP\b/.test(Z) && H > 8 || 8 == H && !/\bTrident\b/.test(e))) && !w.test(k = d.call(l, e.replace(w, "") + ";")) && k.name && (k = "ing as " + k.name + ((k = k.version) ? " " + k : ""), w.test(q) ? (/\bIE\b/.test(k) && "Mac OS" == Z && (Z = null), k = "identify" + k) : (k = "mask" + k, q = D ? a(D.replace(/([a-z])([A-Z])/g, "$1 $2")) : "Opera", /\bIE\b/.test(k) && (Z = null), K || (H = null)), V = ["Presto"], U.push(k)) : q += " Mobile", (k = (/\bAppleWebKit\/([\d.]+\+?)/i.exec(e) || 0)[1]) && (k = [parseFloat(k.replace(/\.(\d)$/, ".0$1")), k], "Safari" == q && "+" == k[1].slice(-1) ? (q = "WebKit Nightly", X = "alpha", H = k[1].slice(0, -1)) : (H == k[1] || H == (k[2] = (/\bSafari\/([\d.]+\+?)/i.exec(e) || 0)[1])) && (H = null), k[1] = (/\bChrome\/([\d.]+)/i.exec(e) || 0)[1], 537.36 == k[0] && 537.36 == k[2] && parseFloat(k[1]) >= 28 && "IE" != q && "Microsoft Edge" != q && (V = ["Blink"]), K && (_ || k[1]) ? (V && (V[1] = "like Chrome"), k = k[1] || (k = k[0], 530 > k ? 1 : 532 > k ? 2 : 532.05 > k ? 3 : 533 > k ? 4 : 534.03 > k ? 5 : 534.07 > k ? 6 : 534.1 > k ? 7 : 534.13 > k ? 8 : 534.16 > k ? 9 : 534.24 > k ? 10 : 534.3 > k ? 11 : 535.01 > k ? 12 : 535.02 > k ? "13+" : 535.07 > k ? 15 : 535.11 > k ? 16 : 535.19 > k ? 17 : 536.05 > k ? 18 : 536.1 > k ? 19 : 537.01 > k ? 20 : 537.11 > k ? "21+" : 537.13 > k ? 23 : 537.18 > k ? 24 : 537.24 > k ? 25 : 537.36 > k ? 26 : "Blink" != V ? "27" : "28")) : (V && (V[1] = "like Safari"), k = k[0], k = 400 > k ? 1 : 500 > k ? 2 : 526 > k ? 3 : 533 > k ? 4 : 534 > k ? "4+" : 535 > k ? 5 : 537 > k ? 6 : 538 > k ? 7 : 601 > k ? 8 : "8"), V && (V[1] += " " + (k += "number" == typeof k ? ".x" : /[.+]/.test(k) ? "" : "+")), "Safari" == q && (!H || parseInt(H) > 45) && (H = k)), "Opera" == q && (k = /\bzbov|zvav$/.exec(Z)) ? (q += " ", U.unshift("desktop mode"), "zvav" == k ? (q += "Mini", H = null) : q += "Mobile", Z = Z.replace(RegExp(" *" + k + "$"), "")) : "Safari" == q && /\bChrome\b/.exec(V && V[1]) && (U.unshift("desktop mode"), q = "Chrome Mobile", H = null, /\bOS X\b/.test(Z) ? (J = "Apple", Z = "iOS 4.3+") : Z = null), H && 0 == H.indexOf(k = /[\d.]+$/.exec(Z)) && e.indexOf("/" + k + "-") > -1 && (Z = p(Z.replace(k, ""))), V && !/\b(?:Avant|Nook)\b/.test(q) && (/Browser|Lunascape|Maxthon/.test(q) || /^(?:Adobe|Arora|Breach|Midori|Opera|Phantom|Rekonq|Rock|Sleipnir|Web)/.test(q) && V[1]) && (k = V[V.length - 1]) && U.push(k), U.length && (U = ["(" + U.join("; ") + ")"]), J && z && z.indexOf(J) < 0 && U.push("on " + J), z && U.push((/^on /.test(U[U.length - 1]) ? "" : "on ") + z), Z && (k = / ([\d.+]+)$/.exec(Z), P = k && "/" == Z.charAt(Z.length - k[0].length - 1), Z = {
            architecture: 32,
            family: k && !P ? Z.replace(k[0], "") : Z,
            version: k ? k[1] : null,
            toString: function () {
              var e = this.version;
              return this.family + (e && !P ? " " + e : "") + (64 == this.architecture ? " 64-bit" : "")
            }
          }), (k = /\b(?:AMD|IA|Win|WOW|x86_|x)64\b/i.exec(G)) && !/\bi686\b/i.test(G) && (Z && (Z.architecture = 64, Z.family = Z.family.replace(RegExp(" *" + k), "")), q && (/\bWOW64\b/i.test(e) || K && /\w(?:86|32)$/.test(O.cpuClass || O.platform) && !/\bWin64; x64\b/i.test(e)) && U.unshift("32-bit")), e || (e = null);
          var Q = {};
          return Q.description = e, Q.layout = V && V[0], Q.manufacturer = J, Q.name = q, Q.prerelease = X, Q.product = z, Q.ua = e, Q.version = q && H, Q.os = Z || {
              architecture: null,
              family: null,
              version: null,
              toString: function () {
                return "null"
              }
            }, Q.parse = d, Q.toString = m, Q.version && U.unshift(H), Q.name && U.unshift(q), Z && q && (Z != String(Z).split(" ")[0] || Z != q.split(" ")[0] && !z) && U.push(z ? "(" + Z + ")" : "on " + Z), U.length && (Q.description = U.join(" ")), Q
        }

        var b = {
          "function": !0,
          object: !0
        }, y = b[typeof window] && window || this, v = y, g = b[typeof r] && r, m = b[typeof t] && t && !t.nodeType && t, h = g && m && "object" == typeof e && e;
        !h || h.global !== h && h.window !== h && h.self !== h || (y = h);
        var x = Math.pow(2, 53) - 1, w = /\bOpera/, S = this, O = Object.prototype, j = O.hasOwnProperty, A = O.toString;
        "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () {
          return d()
        }) : g && m ? l(d(), function (e, t) {
          g[t] = e
        }) : y.platform = d()
      }).call(this)
    }).call(this, "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
  }, {}], 12: [function (e, t, r) {
    "use strict";
    var n = e("./stringify"), i = e("./parse");
    t.exports = {stringify: n, parse: i}
  }, {"./parse": 13, "./stringify": 14}], 13: [function (e, t, r) {
    "use strict";
    var n = e("./utils"), i = {
      delimiter: "&",
      depth: 5,
      arrayLimit: 20,
      parameterLimit: 1e3,
      strictNullHandling: !1,
      plainObjects: !1,
      allowPrototypes: !1,
      allowDots: !1
    };
    i.parseValues = function (e, t) {
      for (var r = {}, i = e.split(t.delimiter, t.parameterLimit === 1 / 0 ? void 0 : t.parameterLimit), o = 0; o < i.length; ++o) {
        var a = i[o], l = -1 === a.indexOf("]=") ? a.indexOf("=") : a.indexOf("]=") + 1;
        if (-1 === l)r[n.decode(a)] = "", t.strictNullHandling && (r[n.decode(a)] = null); else {
          var s = n.decode(a.slice(0, l)), c = n.decode(a.slice(l + 1));
          Object.prototype.hasOwnProperty.call(r, s) ? r[s] = [].concat(r[s]).concat(c) : r[s] = c
        }
      }
      return r
    }, i.parseObject = function (e, t, r) {
      if (!e.length)return t;
      var n, o = e.shift();
      if ("[]" === o)n = [], n = n.concat(i.parseObject(e, t, r)); else {
        n = r.plainObjects ? Object.create(null) : {};
        var a = "[" === o[0] && "]" === o[o.length - 1] ? o.slice(1, o.length - 1) : o, l = parseInt(a, 10);
        !isNaN(l) && o !== a && String(l) === a && l >= 0 && r.parseArrays && l <= r.arrayLimit ? (n = [], n[l] = i.parseObject(e, t, r)) : n[a] = i.parseObject(e, t, r)
      }
      return n
    }, i.parseKeys = function (e, t, r) {
      if (e) {
        var n = r.allowDots ? e.replace(/\.([^\.\[]+)/g, "[$1]") : e, o = /^([^\[\]]*)/, a = /(\[[^\[\]]*\])/g, l = o.exec(n), s = [];
        if (l[1]) {
          if (!r.plainObjects && Object.prototype.hasOwnProperty(l[1]) && !r.allowPrototypes)return;
          s.push(l[1])
        }
        for (var c = 0; null !== (l = a.exec(n)) && c < r.depth;)c += 1, (r.plainObjects || !Object.prototype.hasOwnProperty(l[1].replace(/\[|\]/g, "")) || r.allowPrototypes) && s.push(l[1]);
        return l && s.push("[" + n.slice(l.index) + "]"), i.parseObject(s, t, r)
      }
    }, t.exports = function (e, t) {
      var r = t || {};
      if (r.delimiter = "string" == typeof r.delimiter || n.isRegExp(r.delimiter) ? r.delimiter : i.delimiter, r.depth = "number" == typeof r.depth ? r.depth : i.depth, r.arrayLimit = "number" == typeof r.arrayLimit ? r.arrayLimit : i.arrayLimit, r.parseArrays = r.parseArrays !== !1, r.allowDots = "boolean" == typeof r.allowDots ? r.allowDots : i.allowDots, r.plainObjects = "boolean" == typeof r.plainObjects ? r.plainObjects : i.plainObjects, r.allowPrototypes = "boolean" == typeof r.allowPrototypes ? r.allowPrototypes : i.allowPrototypes, r.parameterLimit = "number" == typeof r.parameterLimit ? r.parameterLimit : i.parameterLimit, r.strictNullHandling = "boolean" == typeof r.strictNullHandling ? r.strictNullHandling : i.strictNullHandling, "" === e || null === e || "undefined" == typeof e)return r.plainObjects ? Object.create(null) : {};
      for (var o = "string" == typeof e ? i.parseValues(e, r) : e, a = r.plainObjects ? Object.create(null) : {}, l = Object.keys(o), s = 0; s < l.length; ++s) {
        var c = l[s], u = i.parseKeys(c, o[c], r);
        a = n.merge(a, u, r)
      }
      return n.compact(a)
    }
  }, {"./utils": 15}], 14: [function (e, t, r) {
    "use strict";
    var n = e("./utils"), i = {
      delimiter: "&", arrayPrefixGenerators: {
        brackets: function (e) {
          return e + "[]"
        }, indices: function (e, t) {
          return e + "[" + t + "]"
        }, repeat: function (e) {
          return e
        }
      }, strictNullHandling: !1, skipNulls: !1, encode: !0
    };
    i.stringify = function (e, t, r, o, a, l, s, c, u) {
      var f = e;
      if ("function" == typeof s)f = s(t, f); else if (n.isBuffer(f))f = String(f); else if (f instanceof Date)f = f.toISOString(); else if (null === f) {
        if (o)return l ? n.encode(t) : t;
        f = ""
      }
      if ("string" == typeof f || "number" == typeof f || "boolean" == typeof f)return l ? [n.encode(t) + "=" + n.encode(f)] : [t + "=" + f];
      var p = [];
      if ("undefined" == typeof f)return p;
      var d;
      if (Array.isArray(s))d = s; else {
        var b = Object.keys(f);
        d = c ? b.sort(c) : b
      }
      for (var y = 0; y < d.length; ++y) {
        var v = d[y];
        a && null === f[v] || (p = Array.isArray(f) ? p.concat(i.stringify(f[v], r(t, v), r, o, a, l, s, c, u)) : p.concat(i.stringify(f[v], t + (u ? "." + v : "[" + v + "]"), r, o, a, l, s, c, u)))
      }
      return p
    }, t.exports = function (e, t) {
      var r, n, o = e, a = t || {}, l = "undefined" == typeof a.delimiter ? i.delimiter : a.delimiter, s = "boolean" == typeof a.strictNullHandling ? a.strictNullHandling : i.strictNullHandling, c = "boolean" == typeof a.skipNulls ? a.skipNulls : i.skipNulls, u = "boolean" == typeof a.encode ? a.encode : i.encode, f = "function" == typeof a.sort ? a.sort : null, p = "undefined" == typeof a.allowDots ? !1 : a.allowDots;
      "function" == typeof a.filter ? (n = a.filter, o = n("", o)) : Array.isArray(a.filter) && (r = n = a.filter);
      var d = [];
      if ("object" != typeof o || null === o)return "";
      var b;
      b = a.arrayFormat in i.arrayPrefixGenerators ? a.arrayFormat : "indices" in a ? a.indices ? "indices" : "repeat" : "indices";
      var y = i.arrayPrefixGenerators[b];
      r || (r = Object.keys(o)), f && r.sort(f);
      for (var v = 0; v < r.length; ++v) {
        var g = r[v];
        c && null === o[g] || (d = d.concat(i.stringify(o[g], g, y, s, c, u, n, f, p)))
      }
      return d.join(l)
    }
  }, {"./utils": 15}], 15: [function (e, t, r) {
    "use strict";
    var n = function () {
      for (var e = new Array(256), t = 0; 256 > t; ++t)e[t] = "%" + ((16 > t ? "0" : "") + t.toString(16)).toUpperCase();
      return e
    }();
    r.arrayToObject = function (e, t) {
      for (var r = t.plainObjects ? Object.create(null) : {}, n = 0; n < e.length; ++n)"undefined" != typeof e[n] && (r[n] = e[n]);
      return r
    }, r.merge = function (e, t, n) {
      if (!t)return e;
      if ("object" != typeof t) {
        if (Array.isArray(e))e.push(t); else {
          if ("object" != typeof e)return [e, t];
          e[t] = !0
        }
        return e
      }
      if ("object" != typeof e)return [e].concat(t);
      var i = e;
      return Array.isArray(e) && !Array.isArray(t) && (i = r.arrayToObject(e, n)), Object.keys(t).reduce(function (e, i) {
        var o = t[i];
        return Object.prototype.hasOwnProperty.call(e, i) ? e[i] = r.merge(e[i], o, n) : e[i] = o, e
      }, i)
    }, r.decode = function (e) {
      try {
        return decodeURIComponent(e.replace(/\+/g, " "))
      } catch (t) {
        return e
      }
    }, r.encode = function (e) {
      if (0 === e.length)return e;
      for (var t = "string" == typeof e ? e : String(e), r = "", i = 0; i < t.length; ++i) {
        var o = t.charCodeAt(i);
        45 === o || 46 === o || 95 === o || 126 === o || o >= 48 && 57 >= o || o >= 65 && 90 >= o || o >= 97 && 122 >= o ? r += t.charAt(i) : 128 > o ? r += n[o] : 2048 > o ? r += n[192 | o >> 6] + n[128 | 63 & o] : 55296 > o || o >= 57344 ? r += n[224 | o >> 12] + n[128 | o >> 6 & 63] + n[128 | 63 & o] : (i += 1, o = 65536 + ((1023 & o) << 10 | 1023 & t.charCodeAt(i)), r += n[240 | o >> 18] + n[128 | o >> 12 & 63] + n[128 | o >> 6 & 63] + n[128 | 63 & o])
      }
      return r
    }, r.compact = function (e, t) {
      if ("object" != typeof e || null === e)return e;
      var n = t || [], i = n.indexOf(e);
      if (-1 !== i)return n[i];
      if (n.push(e), Array.isArray(e)) {
        for (var o = [], a = 0; a < e.length; ++a)"undefined" != typeof e[a] && o.push(e[a]);
        return o
      }
      for (var l = Object.keys(e), s = 0; s < l.length; ++s) {
        var c = l[s];
        e[c] = r.compact(e[c], n)
      }
      return e
    }, r.isRegExp = function (e) {
      return "[object RegExp]" === Object.prototype.toString.call(e)
    }, r.isBuffer = function (e) {
      return null === e || "undefined" == typeof e ? !1 : !!(e.constructor && e.constructor.isBuffer && e.constructor.isBuffer(e))
    }
  }, {}]
}, {}, [1]);

/**
 * videojs-mux
 * @version 1.0.2
 * @copyright 2016 Matthew McClure
 * @license MIT
 */
!function (e) {
  if ("object" == typeof exports && "undefined" != typeof module)module.exports = e(); else if ("function" == typeof define && define.amd)define([], e); else {
    var t;
    t = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, t.videojsMux = e()
  }
}(function () {
  var e;
  return function t(e, n, r) {
    function i(a, u) {
      if (!n[a]) {
        if (!e[a]) {
          var l = "function" == typeof require && require;
          if (!u && l)return l(a, !0);
          if (o)return o(a, !0);
          var f = new Error("Cannot find module '" + a + "'");
          throw f.code = "MODULE_NOT_FOUND", f
        }
        var c = n[a] = {exports: {}};
        e[a][0].call(c.exports, function (t) {
          var n = e[a][1][t];
          return i(n ? n : t)
        }, c, c.exports, t, e, n, r)
      }
      return n[a].exports
    }

    for (var o = "function" == typeof require && require, a = 0; a < r.length; a++)i(r[a]);
    return i
  }({
    1: [function (e, t, n) {
      "use strict";
      function r(e, t) {
        return "function" == typeof e[t] ? e[t].call(e) : ""
      }

      Object.defineProperty(n, "__esModule", {value: !0}), n["default"] = r
    }, {}], 2: [function (e, t, n) {
      "use strict";
      function r(e) {
        return e && e.__esModule ? e : {"default": e}
      }

      function i(e, t) {
        return a["default"].defaultView && a["default"].defaultView.getComputedStyle ? a["default"].defaultView.getComputedStyle(e, null).getPropertyValue(t) : void 0
      }

      Object.defineProperty(n, "__esModule", {value: !0}), n["default"] = i;
      var o = e("global/document"), a = r(o)
    }, {"global/document": 5}], 3: [function (e, t, n) {
      "use strict";
      function r() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (e) {
          var t = 16 * Math.random() | 0, n = "x" === e ? t : 3 & t | 8;
          return n.toString(16)
        })
      }

      Object.defineProperty(n, "__esModule", {value: !0}), n["default"] = r
    }, {}], 4: [function (e, t, n) {
    }, {}], 5: [function (e, t, n) {
      (function (n) {
        var r = "undefined" != typeof n ? n : "undefined" != typeof window ? window : {}, i = e("min-document");
        if ("undefined" != typeof document)t.exports = document; else {
          var o = r["__GLOBAL_DOCUMENT_CACHE@4"];
          o || (o = r["__GLOBAL_DOCUMENT_CACHE@4"] = i), t.exports = o
        }
      }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"min-document": 4}], 6: [function (e, t, n) {
      (function (e) {
        "undefined" != typeof window ? t.exports = window : "undefined" != typeof e ? t.exports = e : "undefined" != typeof self ? t.exports = self : t.exports = {}
      }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {}], 7: [function (e, t, n) {
      function r(e, t) {
        return e = "number" == typeof e || b.test(e) ? +e : -1, t = null == t ? _ : t, e > -1 && e % 1 == 0 && t > e
      }

      function i(e, t, n) {
        var r = e[t];
        (!c(r, n) || c(r, m[t]) && !x.call(e, t) || void 0 === n && !(t in e)) && (e[t] = n)
      }

      function o(e) {
        return function (t) {
          return null == t ? void 0 : t[e]
        }
      }

      function a(e, t, n) {
        return u(e, t, n)
      }

      function u(e, t, n, r) {
        n || (n = {});
        for (var o = -1, a = t.length; ++o < a;) {
          var u = t[o], l = r ? r(n[u], e[u], u, n, e) : e[u];
          i(n, u, l)
        }
        return n
      }

      function l(e) {
        return y(function (t, n) {
          var r = -1, i = n.length, o = i > 1 ? n[i - 1] : void 0, a = i > 2 ? n[2] : void 0;
          for (o = "function" == typeof o ? (i--, o) : void 0, a && f(n[0], n[1], a) && (o = 3 > i ? void 0 : o, i = 1), t = Object(t); ++r < i;) {
            var u = n[r];
            u && e(t, u, r, o)
          }
          return t
        })
      }

      function f(e, t, n) {
        if (!v(n))return !1;
        var i = typeof t;
        return ("number" == i ? s(n) && r(t, n.length) : "string" == i && t in n) ? c(n[t], e) : !1
      }

      function c(e, t) {
        return e === t || e !== e && t !== t
      }

      function s(e) {
        return null != e && !("function" == typeof e && d(e)) && p(j(e))
      }

      function d(e) {
        var t = v(e) ? k.call(e) : "";
        return t == g || t == w
      }

      function p(e) {
        return "number" == typeof e && e > -1 && e % 1 == 0 && _ >= e
      }

      function v(e) {
        var t = typeof e;
        return !!e && ("object" == t || "function" == t)
      }

      var h = e("lodash.keys"), y = e("lodash.rest"), _ = 9007199254740991, g = "[object Function]", w = "[object GeneratorFunction]", b = /^(?:0|[1-9]\d*)$/, m = Object.prototype, x = m.hasOwnProperty, k = m.toString, j = o("length"), C = l(function (e, t) {
        a(t, h(t), e)
      });
      t.exports = C
    }, {"lodash.keys": 8, "lodash.rest": 9}], 8: [function (e, t, n) {
      function r(e, t) {
        for (var n = -1, r = Array(e); ++n < e;)r[n] = t(n);
        return r
      }

      function i(e, t) {
        return e = "number" == typeof e || j.test(e) ? +e : -1, t = null == t ? w : t, e > -1 && e % 1 == 0 && t > e
      }

      function o(e, t) {
        return E.call(e, t) || "object" == typeof e && t in e && null === I(e)
      }

      function a(e) {
        return T(Object(e))
      }

      function u(e) {
        return function (t) {
          return null == t ? void 0 : t[e]
        }
      }

      function l(e) {
        var t = e ? e.length : void 0;
        return v(t) && (F(e) || _(e) || c(e)) ? r(t, String) : null
      }

      function f(e) {
        var t = e && e.constructor, n = "function" == typeof t && t.prototype || C;
        return e === n
      }

      function c(e) {
        return d(e) && E.call(e, "callee") && (!S.call(e, "callee") || O.call(e) == b)
      }

      function s(e) {
        return null != e && !("function" == typeof e && p(e)) && v(L(e))
      }

      function d(e) {
        return y(e) && s(e)
      }

      function p(e) {
        var t = h(e) ? O.call(e) : "";
        return t == m || t == x
      }

      function v(e) {
        return "number" == typeof e && e > -1 && e % 1 == 0 && w >= e
      }

      function h(e) {
        var t = typeof e;
        return !!e && ("object" == t || "function" == t)
      }

      function y(e) {
        return !!e && "object" == typeof e
      }

      function _(e) {
        return "string" == typeof e || !F(e) && y(e) && O.call(e) == k
      }

      function g(e) {
        var t = f(e);
        if (!t && !s(e))return a(e);
        var n = l(e), r = !!n, u = n || [], c = u.length;
        for (var d in e)!o(e, d) || r && ("length" == d || i(d, c)) || t && "constructor" == d || u.push(d);
        return u
      }

      var w = 9007199254740991, b = "[object Arguments]", m = "[object Function]", x = "[object GeneratorFunction]", k = "[object String]", j = /^(?:0|[1-9]\d*)$/, C = Object.prototype, E = C.hasOwnProperty, O = C.toString, I = Object.getPrototypeOf, S = C.propertyIsEnumerable, T = Object.keys, L = u("length"), F = Array.isArray;
      t.exports = g
    }, {}], 9: [function (e, t, n) {
      function r(e, t, n) {
        var r = n.length;
        switch (r) {
          case 0:
            return e.call(t);
          case 1:
            return e.call(t, n[0]);
          case 2:
            return e.call(t, n[0], n[1]);
          case 3:
            return e.call(t, n[0], n[1], n[2])
        }
        return e.apply(t, n)
      }

      function i(e, t) {
        if ("function" != typeof e)throw new TypeError(f);
        return t = x(void 0 === t ? e.length - 1 : u(t), 0), function () {
          for (var n = arguments, i = -1, o = x(n.length - t, 0), a = Array(o); ++i < o;)a[i] = n[t + i];
          switch (t) {
            case 0:
              return e.call(this, a);
            case 1:
              return e.call(this, n[0], a);
            case 2:
              return e.call(this, n[0], n[1], a)
          }
          var u = Array(t + 1);
          for (i = -1; ++i < t;)u[i] = n[i];
          return u[t] = a, r(e, this, u)
        }
      }

      function o(e) {
        var t = a(e) ? m.call(e) : "";
        return t == p || t == v
      }

      function a(e) {
        var t = typeof e;
        return !!e && ("object" == t || "function" == t)
      }

      function u(e) {
        if (!e)return 0 === e ? e : 0;
        if (e = l(e), e === c || e === -c) {
          var t = 0 > e ? -1 : 1;
          return t * s
        }
        var n = e % 1;
        return e === e ? n ? e - n : e : 0
      }

      function l(e) {
        if (a(e)) {
          var t = o(e.valueOf) ? e.valueOf() : e;
          e = a(t) ? t + "" : t
        }
        if ("string" != typeof e)return 0 === e ? e : +e;
        e = e.replace(h, "");
        var n = _.test(e);
        return n || g.test(e) ? w(e.slice(2), n ? 2 : 8) : y.test(e) ? d : +e
      }

      var f = "Expected a function", c = 1 / 0, s = 1.7976931348623157e308, d = NaN, p = "[object Function]", v = "[object GeneratorFunction]", h = /^\s+|\s+$/g, y = /^[-+]0x[0-9a-f]+$/i, _ = /^0b[01]+$/i, g = /^0o[0-7]+$/i, w = parseInt, b = Object.prototype, m = b.toString, x = Math.max;
      t.exports = i
    }, {}], 10: [function (t, n, r) {
      !function (r, i) {
        "use strict";
        "object" == typeof n && n.exports && "function" == typeof t ? n.exports = i() : "function" == typeof e && "object" == typeof e.amd ? e(i) : r.log = i()
      }(this, function () {
        "use strict";
        function e(e) {
          return typeof console === u ? !1 : void 0 !== console[e] ? t(console, e) : void 0 !== console.log ? t(console, "log") : a
        }

        function t(e, t) {
          var n = e[t];
          if ("function" == typeof n.bind)return n.bind(e);
          try {
            return Function.prototype.bind.call(n, e)
          } catch (r) {
            return function () {
              return Function.prototype.apply.apply(n, [e, arguments])
            }
          }
        }

        function n(e, t, n) {
          return function () {
            typeof console !== u && (r.call(this, t, n), this[e].apply(this, arguments))
          }
        }

        function r(e, t) {
          for (var n = 0; n < l.length; n++) {
            var r = l[n];
            this[r] = e > n ? a : this.methodFactory(r, e, t)
          }
        }

        function i(t, r, i) {
          return e(t) || n.apply(this, arguments)
        }

        function o(e, t, n) {
          function o(e) {
            var t = (l[e] || "silent").toUpperCase();
            try {
              return void(window.localStorage[s] = t)
            } catch (n) {
            }
            try {
              window.document.cookie = encodeURIComponent(s) + "=" + t + ";"
            } catch (n) {
            }
          }

          function a() {
            var e;
            try {
              e = window.localStorage[s]
            } catch (t) {
            }
            if (typeof e === u)try {
              var n = window.document.cookie, r = n.indexOf(encodeURIComponent(s) + "=");
              r && (e = /^([^;]+)/.exec(n.slice(r))[1])
            } catch (t) {
            }
            return void 0 === c.levels[e] && (e = void 0), e
          }

          var f, c = this, s = "loglevel";
          e && (s += ":" + e), c.levels = {
            TRACE: 0,
            DEBUG: 1,
            INFO: 2,
            WARN: 3,
            ERROR: 4,
            SILENT: 5
          }, c.methodFactory = n || i, c.getLevel = function () {
            return f
          }, c.setLevel = function (t, n) {
            if ("string" == typeof t && void 0 !== c.levels[t.toUpperCase()] && (t = c.levels[t.toUpperCase()]), !("number" == typeof t && t >= 0 && t <= c.levels.SILENT))throw"log.setLevel() called with invalid level: " + t;
            return f = t, n !== !1 && o(t), r.call(c, t, e), typeof console === u && t < c.levels.SILENT ? "No console available for logging" : void 0
          }, c.setDefaultLevel = function (e) {
            a() || c.setLevel(e, !1)
          }, c.enableAll = function (e) {
            c.setLevel(c.levels.TRACE, e)
          }, c.disableAll = function (e) {
            c.setLevel(c.levels.SILENT, e)
          };
          var d = a();
          null == d && (d = null == t ? "WARN" : t), c.setLevel(d, !1)
        }

        var a = function () {
        }, u = "undefined", l = ["trace", "debug", "info", "warn", "error"], f = new o, c = {};
        f.getLogger = function (e) {
          if ("string" != typeof e || "" === e)throw new TypeError("You must supply a name when creating a logger.");
          var t = c[e];
          return t || (t = c[e] = new o(e, f.getLevel(), f.methodFactory)), t
        };
        var s = typeof window !== u ? window.log : void 0;
        return f.noConflict = function () {
          return typeof window !== u && window.log === f && (window.log = s), f
        }, f
      })
    }, {}], 11: [function (e, t, n) {
      (function (t) {
        "use strict";
        function r(e) {
          return e && e.__esModule ? e : {"default": e}
        }

        function i(e, t) {
          if (!(e instanceof t))throw new TypeError("Cannot call a class as a function")
        }

        Object.defineProperty(n, "__esModule", {value: !0});
        var o = function () {
          function e(e, t) {
            for (var n = 0; n < t.length; n++) {
              var r = t[n];
              r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
            }
          }

          return function (t, n, r) {
            return n && e(t.prototype, n), r && e(t, r), t
          }
        }(), a = "undefined" != typeof window ? window.videojs : "undefined" != typeof t ? t.videojs : null, u = r(a), l = e("loglevel"), f = r(l), c = e("global/window"), s = r(c), d = e("global/document"), p = r(d), v = e("lodash.assign"), h = r(v), y = e("./lib/get-computed-style.js"), _ = r(y), g = e("./lib/call-if-exists.js"), w = r(g), b = e("./lib/uuid.js"), m = r(b), x = ["ended", "error", "loadstart", "pause", "play", "playing", "ratechange", "stalled", "waiting"];
        s["default"][s["default"].MUX_VAR_NAME || "mux"] || f["default"].error("[videojs-mux] No core mux function found! Did you forget to install it?");
        var k = function () {
          function e(t, n) {
            var r = this;
            i(this, e);
            var o = {heart_beat_interval: 1e4, check_buffering_interval: 100, debug: !1};
            this.player = t, this.options = (0, h["default"])({}, o, n), f["default"].setLevel(this.options.debug ? "debug" : "warn"), (n.userId || n.user_id) && this.queue("set", "viewer_user_id", n.userId || n.user_id), this.player_data = (0, h["default"])({
              player_instance_id: this._uniqueShortCode(),
              player_load_start: Date.now(),
              player_software: "Video.js",
              player_software_version: u["default"].VERSION || "< 4.11",
              player_mux_plugin_version: "1.0.2",
              player_view_count: 1
            }, this.options.player_data), this._resetViewData(), this.setVideo(this.options.video_data), this.track("playerloadstart"), this.player.mux = function () {
              f["default"].error("[videojs-mux] The plugin was initialized more than once.")
            }, this.player.mux.setVideo = this.setVideo.bind(this), t.muxChangeVideo = this.setVideo.bind(this), t.ready(function () {
              r.track("playerready"), t.addClass("vjs-mux"), r._initEventListeners()
            })
          }

          return o(e, [{
            key: "queue", value: function () {
              var e = s["default"].MUX_VAR_NAME || "mux", t = s["default"][e] || function () {
                };
              t.apply(null, arguments)
            }
          }, {
            key: "track", value: function (e) {
              var t = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1];
              this._clearNextHeartbeat();
              var n = (0, h["default"])(this._baseEventData(), t);
              f["default"].info(e, n), this.queue("send", e, n), this.player.paused() || this._createPendingHeartbeat()
            }
          }, {
            key: "setVideo", value: function () {
              var e = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0], t = this.video_data, n = {};
              !function () {
                var t = {
                  id: "video_id",
                  title: "video_title",
                  series: "video_series",
                  variantId: "video_variant_id",
                  variant: "video_variant_name"
                };
                for (var n in t)e[n] && (e[t[n]] = e[n], delete e[n])
              }(), (0, h["default"])(n, e), n.video_id || (n.video_id = this._videoIdFromSrc(this.player.currentSrc())), this.video_data = n, f["default"].debug("[mux] Changing Video (old, new): ", t && t.video_id, n.video_id), t && (this._resetViewData(), this.player_data.player_view_count += 1)
            }
          }, {
            key: "_resetViewData", value: function () {
              this._watchedRanges = [], this._currentWatchRange = null, this.view_data = {
                view_id: (0, m["default"])(),
                view_buffering_count: 0,
                view_buffering_duration: 0,
                view_watch_time: 0
              }
            }
          }, {
            key: "_initEventListeners", value: function () {
              var e = this;
              this._initTimeToFirstFrameCheck(), this._initSeekListener(), this._initBufferingCheck(), this._trackWatchTime(), x.forEach(function (t) {
                e.player.on(t, function () {
                  e.track(t)
                })
              })
            }
          }, {
            key: "_initTimeToFirstFrameCheck", value: function () {
              var e = this;
              this.player.one("play", function () {
                e.view_data.view_start = Date.now(), e.player.one("playing", function () {
                  var t = Date.now() - e.view_data.view_start;
                  e.view_data.view_time_to_first_frame = t
                })
              })
            }
          }, {
            key: "_initSeekListener", value: function () {
              var e = !1;
              this.player.on("seeking", function () {
                e === !1 && (e = !0, this.track("seeking"))
              }.bind(this)), this.player.on("playing", function () {
                e = !1
              })
            }
          }, {
            key: "_initBufferingCheck", value: function () {
              this._bufferingCheck(this.player.currentTime())
            }
          }, {
            key: "_bufferingCheck", value: function (e, t) {
              var n = this, r = arguments.length <= 2 || void 0 === arguments[2] ? !1 : arguments[2], i = this.player.currentTime(), o = 1 / this.options.check_buffering_interval;
              if (!t && !r && e + o > i && i !== e && !this.player.paused())this.track("bufferingstart"), t = Date.now(), r = !0; else if (t && r && i > e + o && !this.player.paused()) {
                var a = Date.now() - t;
                a > 30 && (this.view_data.view_buffering_count += 1, this.view_data.view_buffering_duration += a, this.track("bufferingend")), t = null, r = !1
              }
              this.player.setTimeout(function () {
                n._bufferingCheck(i, t, r)
              }, this.options.check_buffering_interval)
            }
          }, {
            key: "_createPendingHeartbeat", value: function () {
              var e = this;
              return this._nextHeartbeatId = this.player.setTimeout(function () {
                e.track("hb")
              }, this.options.heart_beat_interval), this._nextHeartbeatId
            }
          }, {
            key: "_clearNextHeartbeat", value: function () {
              this._nextHeartbeatId && this.player.clearTimeout(this._nextHeartbeatId)
            }
          }, {
            key: "_trackWatchTime", value: function () {
              var e = this.player, t = function () {
                if (!e.paused()) {
                  if (e.seeking())return void(this._currentWatchRange = null);
                  var t = e.currentTime(), n = this._currentWatchRange;
                  if (!n || t < n[1] || t > n[1] + 1)this._currentWatchRange = [t, t], this._watchedRanges.push(this._currentWatchRange); else {
                    var r = t - n[1];
                    this._currentWatchRange[1] = t, this.view_data.view_watch_time += r
                  }
                }
              }.bind(this);
              this.player.on("playing", t), this.player.on("timeupdate", t), this.player.on("end", t), this.player.on("seeking", function () {
                this._currentWatchRange = null
              }.bind(this))
            }
          }, {
            key: "_uniqueShortCode", value: function () {
              return ("000000" + (Math.random() * Math.pow(36, 6) << 0).toString(36)).slice(-6)
            }
          }, {
            key: "_currentVideoId", value: function (e) {
              if ("undefined" == typeof e && null === this._currentVideoInfo.id) {
                var t = this._currentVideoInfo.id = this._videoIdFromSrc(e);
                return t
              }
              return "undefined" == typeof e ? this._currentVideoInfo.id : this._videoIdFromSrc(e)
            }
          }, {
            key: "_videoIdFromSrc", value: function (e) {
              var t = p["default"].createElement("a");
              t.href = e;
              var n = t.pathname.replace(/\.[^\/.]+$/, "");
              return s["default"].btoa(t.host + n).split("=")[0]
            }
          }, {
            key: "_baseEventData", value: function () {
              var e = this.player, t = {};
              return (0, h["default"])(t, this.player_data), (0, h["default"])(t, this.video_data), (0, h["default"])(t, this.view_data), (0, h["default"])(t, {
                player_paused: e.paused(),
                player_time: e.currentTime(),
                player_language: (0, w["default"])(e, "language"),
                player_is_fullscreen: e.isFullscreen(),
                player_source_url: e.currentSrc(),
                player_source_type: (0, w["default"])(e, "currentType"),
                player_source_duration: e.duration(),
                player_source_height: (0, w["default"])(e, "videoHeight"),
                player_source_width: (0, w["default"])(e, "videoWidth"),
                player_autoplay: e.autoplay(),
                player_preload: e.preload(),
                player_poster_url: e.poster(),
                player_width: (0, _["default"])(e.el(), "width"),
                player_height: (0, _["default"])(e.el(), "height")
              }), e.duration() === 1 / 0 ? t.player_source_stream_type = "live" : e.duration() > 0 && (t.player_source_stream_type = "on-demand"), e.error() && (t.player_error_code = e.error().code, t.player_error_message = e.error().message), t
            }
          }]), e
        }();
        u["default"].plugin("mux", function (e) {
          return u["default"].IS_IE8 ? !1 : new k(this, e)
        }), n["default"] = k
      }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {
      "./lib/call-if-exists.js": 1,
      "./lib/get-computed-style.js": 2,
      "./lib/uuid.js": 3,
      "global/document": 5,
      "global/window": 6,
      "lodash.assign": 7,
      loglevel: 10
    }]
  }, {}, [11])(11)
});
